﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreRotation : MonoBehaviour
{
    [SerializeField] private GameObject[] rings;
    [SerializeField] private float rotationSpeed = 10;
    [SerializeField] private MeshRenderer coreMaterial;

    [Header("Core Colors")]

    [SerializeField] private Color blueColor;
    [SerializeField] private Color redColor;
    [SerializeField] private Color yellowColor;
    [SerializeField] private Color whiteColor;

    public void StartRotation()
    {
        coreMaterial.material.SetColor("BorderColor", Color.black);

        StartCoroutine(RotationRings(rings[0]));
        StartCoroutine(RotationRings(rings[1]));
        StartCoroutine(RotationRings(rings[2]));

        Invoke("Blue", 1);
        Invoke("Red", 2);
        Invoke("Yellow", 3);
        Invoke("White", 4);
    }

    private IEnumerator RotationRings(GameObject ring)
    {
        while (ring.transform.localEulerAngles.y != 0)
        {
            ring.transform.localEulerAngles = Vector3.MoveTowards(ring.transform.localEulerAngles, Vector3.zero, rotationSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void Blue()
    {
        coreMaterial.material.SetColor("BorderColor", blueColor);
    }

    private void Red()
    {
        coreMaterial.material.SetColor("BorderColor", redColor);
    }

    private void Yellow()
    {
        coreMaterial.material.SetColor("BorderColor", yellowColor);
    }

    private void White()
    {
        coreMaterial.material.SetColor("BorderColor", whiteColor);
    }
}
