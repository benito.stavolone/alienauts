﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class MouseInput : MonoBehaviour
    {
        private void Update()
        {
            ChapterSelection();
        }

        private void ChapterSelection()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit, 1000))
                {
                    if (hit.collider.GetComponent<ChapterManager>())
                    {
                        hit.collider.GetComponent<ChapterManager>().TestPanel.gameObject.SetActive(true);
                        Debug.Log(hit.collider);
                    }
                }
            }

        }
    }
