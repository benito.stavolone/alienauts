﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LauncherManager : MonoBehaviour
{
    [Header("Loading Screen")]

    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private Slider _slider;
    [SerializeField] private float loadingDelay = 2;

    [Header("Panels Setting")]

    [SerializeField] private GameObject StartPanel;
    [SerializeField] private GameObject OptionPanel;
    [SerializeField] private GameObject AudioPanel;
    [SerializeField] private GameObject GraphicsPanel;
    [SerializeField] private GameObject ControllerPanel;
    [SerializeField] private GameObject SystemPanel;
   
    [SerializeField] private GameObject SystemButton;

    [Header("Audio")]

    [SerializeField] private AudioClip confirmButtonAudio;
    [SerializeField] private AudioClip backButtonAudio;

    [Header("Buttons")]

    [SerializeField] private TextMeshProUGUI continueButtonTxt;
    [SerializeField] private Button continueButton;
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button levelsButton;

    private void Start()
    {
        if (PlayerPrefs.GetInt("Start") == 1)
        {
            continueButtonTxt.color = new Color(1, 1, 1, 1);
            continueButton.interactable = true;
            continueButton.GetComponent<Image>().enabled = true;
            //EventSystem.current.firstSelectedGameObject = continueButton.gameObject;
        }
        else
        {
            continueButtonTxt.color = new Color(1, 1, 1, 0.3f);
            continueButton.interactable = false;
            continueButton.GetComponent<Image>().enabled = false;
            //EventSystem.current.firstSelectedGameObject = newGameButton.gameObject;
        }

    }

    public void Continue()
    {
        try
        {
            EventManager.ChangeVFXClip(confirmButtonAudio);

            Invoke("StartLoadingContinue", loadingDelay);

        }
        catch (Exception e)
        {

        }
    }

    public void OnClickNewGame()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        PlayerPrefs.DeleteAll();

        SceneManager.LoadScene("StoryScene");
        //StartCoroutine(LoadAsync("StoryScene"));
    }

    public void OnClickStartPanel()
    {
        EventManager.ChangeVFXClip(backButtonAudio);

        StartPanel.SetActive(true);
        OptionPanel.SetActive(false);
        AudioPanel.SetActive(false);
        GraphicsPanel.SetActive(false);
        ControllerPanel.SetActive(false);
        SystemPanel.SetActive(false);
    }

    public void OnClickOptionPanel()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        SystemPanel.SetActive(true);
        StartPanel.SetActive(false);
        OptionPanel.SetActive(true);
        AudioPanel.SetActive(false);
        GraphicsPanel.SetActive(true);
        ControllerPanel.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(SystemButton);
    }

    public void OnClickAudioPanel()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        StartPanel.SetActive(false);
        AudioPanel.SetActive(true);
        GraphicsPanel.SetActive(false);
        ControllerPanel.SetActive(false);
        SystemPanel.SetActive(false);
    }

    public void OnClickGraphicsPanel()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        StartPanel.SetActive(false);
        AudioPanel.SetActive(false);
        GraphicsPanel.SetActive(true);
        ControllerPanel.SetActive(false);
        SystemPanel.SetActive(false);
    }

    public void OnClickControlPanel()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        StartPanel.SetActive(false);
        AudioPanel.SetActive(false);
        GraphicsPanel.SetActive(false);
        ControllerPanel.SetActive(true);
        SystemPanel.SetActive(false);
    }

    public void OnClickSystemPanel()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        StartPanel.SetActive(false);
        AudioPanel.SetActive(false);
        GraphicsPanel.SetActive(false);
        ControllerPanel.SetActive(false);
        SystemPanel.SetActive(true);
    }

    public void FadeOutAudio()
    {
        StartCoroutine(AudioManager.instance.ClipController.FadeOut());
    }

    public void OncliCkPlanetSelection()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);

        PlayerPrefs.SetInt("LevelFinished", 0);

        Invoke("StartLoadingPlanet", loadingDelay);
    }

    public void OnClickCreditButton()
    {
        EventManager.ChangeVFXClip(confirmButtonAudio);
    }

    public void OnClickBackButton()
    {
        EventManager.ChangeVFXClip(backButtonAudio);
    }

    public void OnApplicationQuit()
    {
        Application.Quit();
    }

    private void StartLoadingPlanet()
    {
        StartCoroutine(LoadAsync("PlanetScene"));
    }

    private void StartLoadingContinue()
    {
        StartCoroutine(LoadAsync(PlayerPrefs.GetString("StringPlanetScene")));
    }

    IEnumerator LoadAsync(string sceneindex)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneindex);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            _slider.value = progress;
            yield return null;
        }
    }
}
