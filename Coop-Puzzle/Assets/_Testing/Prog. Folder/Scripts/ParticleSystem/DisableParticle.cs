﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableParticle : MonoBehaviour
{
    [SerializeField] private float lifetime = 0.5f;

    private void OnEnable()
    {
        Invoke("DisableSystem", lifetime);
    }

    private void DisableSystem()
    {
        PoolManager.instance.PutObjectInPool(this.gameObject);
    }
}

