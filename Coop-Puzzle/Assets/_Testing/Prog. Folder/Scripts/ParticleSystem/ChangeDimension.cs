﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDimension : MonoBehaviour
{
    private int _weightReference;

    private ParticleSystem _particleSystem;

    public int WeightReference
    {
        get { return _weightReference; }
        set 
        { 
            _weightReference = value;
            ChangeScale();
        }
    }

    [System.Obsolete]
    private void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }


    private void ChangeScale()
    {
        switch (_weightReference)
        {
            case 0:
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                _particleSystem.startSize = 0.5f;
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                break;

            case 1:
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                _particleSystem.startSize = 0.4f;
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                break;

            case 2:
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                _particleSystem.startSize = 0.3f;
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                break;

            case 3:
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                _particleSystem.startSize = 0.2f;
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                break;
        }
    }
}
