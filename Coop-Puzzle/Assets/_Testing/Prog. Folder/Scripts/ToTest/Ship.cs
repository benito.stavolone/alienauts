﻿using UnityEngine;

public class Ship : MonoBehaviour
{
  public float speed;
  [SerializeField] private Transform[] _waypoints;
  private int waypointsIndex;

  private void Start()
  {
    transform.position = _waypoints[waypointsIndex].transform.position;
  }

  private void Update()
  {
    Move();
  }
  private void Move()
  {
    transform.position = Vector3.MoveTowards(transform.position, _waypoints[waypointsIndex].transform.position, speed * Time.deltaTime);
    if (transform.position == _waypoints[waypointsIndex].transform.position)
    {
       waypointsIndex += 1;
    }

  }
  
}
