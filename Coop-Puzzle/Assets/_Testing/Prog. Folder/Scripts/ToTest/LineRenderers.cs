﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRenderers : MonoBehaviour
{
   public LineRenderer _lineRenderer;

   [SerializeField] private Transform[] waypoints;

   public GameObject Ship;

   private void Start()
   {
      _lineRenderer.SetPosition(0, waypoints[0].position);
      _lineRenderer.startWidth = 5f;
   }

   private void Update()
   {
      _lineRenderer.SetPosition(1,Ship.transform.position);
   }
}
