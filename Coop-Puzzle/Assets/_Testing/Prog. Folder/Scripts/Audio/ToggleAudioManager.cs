﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleAudioManager : MonoBehaviour 
{
    [SerializeField] private Toggle _toggle;
    
    public void ToggleAudio()
    {
        if (_toggle.isOn == false)
        {
            AudioListener.volume = 0; 
        }

        if (_toggle.isOn == true)
        {
            AudioListener.volume = 1; 
        }
    }
}