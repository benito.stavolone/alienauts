﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private AudioMixer _VFXMixer;

    private ClipController _clipController;

    public ClipController ClipController
    {
        get { return _clipController; }
    }

    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else 
            Destroy(gameObject);
    }

    public void Start()
    {
        _clipController = gameObject.GetComponentInChildren<ClipController>();

        // if (AudioListener.volume == 0)
        // {
        //     _toggle.isOn = false;
        // }
    }
    public void SetVolume(float volume)
    {
       _audioMixer.SetFloat("MusicVolume", volume );
      //  _audioMixer.SetFloat("MusicVolume", Mathf.Log10(volume) * 20);
    }
    public void SetVFXVolume(float volume)
    {
        _VFXMixer.SetFloat("VfxVolume", volume);
       // _VFXMixer.SetFloat("VfxVolume", Mathf.Log10(volume) * 20);
    }

    public float GetVolume()
    {
        _audioMixer.GetFloat("MusicVolume", out float volume);
        return volume;
    }
    public float GetVFX()
    {
        _audioMixer.GetFloat("VfxVolume", out float volume);
        return volume;
    }

   
}
