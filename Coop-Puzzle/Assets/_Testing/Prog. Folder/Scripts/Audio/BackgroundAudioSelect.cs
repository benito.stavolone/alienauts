﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioSelect : MonoBehaviour
{
    [Header("Audio")]

    [SerializeField] private AudioClip planetSelection;

    [SerializeField] private AudioClip ambientEffect;

    // Start is called before the first frame update
    void Start()
    {
        EventManager.ChangeBackGroundClip(planetSelection);

        if(ambientEffect)
            EventManager.ChangeAmbientClip(ambientEffect);
        else
            EventManager.ChangeAmbientClip(null);
    }
}
