﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipController : MonoBehaviour
{
    [SerializeField] private AudioSource background;
    [SerializeField] private AudioSource vfx;
    [SerializeField] private AudioSource ambient;

    // Start is called before the first frame update
    void Start()
    {
        EventManager.backgroundClip += AssignBackGround;
        EventManager.vfxClip += AssignVFX;
        EventManager.ambientClip += AssignAmbient;
    }


    private void AssignBackGround(AudioClip clip)
    {
        StartCoroutine(CheckBackgroundVolume());

        background.clip = clip;
        background.Play();
    }

    private void AssignAmbient(AudioClip clip)
    {
        ambient.clip = clip;
        ambient.Play();
    }

    private void AssignVFX(AudioClip clip)
    {
        vfx.clip = clip;
        vfx.Play();
    }

    private IEnumerator CheckBackgroundVolume()
    {
        if(background.volume != 1)
        {
            while(background.volume < 1)
            {
                background.volume += Time.deltaTime;
                yield return null;
            }
        }
    }

    public IEnumerator FadeOut()
    {
        while(background.volume > 0)
        {
            background.volume -= Time.deltaTime;
            yield return null;
        }
    }

    private void OnDisable()
    {
        EventManager.backgroundClip -= AssignBackGround;
        EventManager.vfxClip -= AssignVFX;
        EventManager.ambientClip -= AssignAmbient;
    }
}
