﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using Slider = UnityEngine.UI.Slider;

public class LoadingPanel : MonoBehaviour
{
    [SerializeField] private GameObject[] _tips;
    private void OnEnable()
    {
        RandomTips();
    }

    private void OnDisable()
    {
        for (var i = 0; i < _tips.Length; i++)
        {
            _tips[i].SetActive(false);
        }
    }

    private void RandomTips()
    {
        _tips[Random.Range(0,_tips.Length)].SetActive(true);
    }
  
    
}
