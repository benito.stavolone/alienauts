﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks.Triggers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryPannel : MonoBehaviour
{
    [SerializeField] private GameObject[] StoryPanel;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private GameObject _loadingPage;
    [SerializeField] private Slider _slider;
    private int PanelIndex;

    public async void SkipPage()
    {
        StoryPanel[PanelIndex].gameObject.GetComponentInChildren<Button>().interactable = false;
        PanelIndex++; 
        await Utility.DoFade(_canvasGroup, 0, 1, 2); 
        StoryPanel[PanelIndex].gameObject.SetActive(true); 
        await Utility.DoFade(_canvasGroup, 1, 0, 2);
    }

    public void LoadPlanet()
    {
        _loadingPage.gameObject.SetActive(true);

        PlayerPrefs.SetInt("Start", 1);

        StartCoroutine(LoadAsync("[00]Planet1_Genesys_Tutorial"));
    }

    IEnumerator LoadAsync(string sceneindex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneindex);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            _slider.value = progress;
            yield return null;
        }
    }
}
