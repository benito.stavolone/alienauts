﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SliderEnum
{
    VOLUME,
    VFX,
}

public class SliderController : MonoBehaviour
{
    private AudioManager _audioManager;
    public SliderEnum SliderEnum;

    public Slider _sliderVolume;
    public Slider _sliderVfx;

    private void Awake()
    {
        _audioManager = FindObjectOfType<AudioManager>();
        _sliderVolume.value = _audioManager.GetVolume();
        _sliderVfx.value = _audioManager.GetVFX();

        _sliderVolume.value = PlayerPrefs.GetFloat("MusicVolume");
        _sliderVfx.value = PlayerPrefs.GetFloat("VfxVolume");
    }

    private void Start()
    {
        _sliderVolume.onValueChanged.AddListener(delegate{ValueChangeVolume();});
        _sliderVfx.onValueChanged.AddListener(delegate{ValueChange();});
    }

    public void ValueChange()
    {
        //_audioManager.SetVolume(_sliderVolume.value);
        _audioManager.SetVFXVolume(_sliderVfx.value);

        PlayerPrefs.SetFloat("VfxVolume", _sliderVfx.value);
    } 

    public void ValueChangeVolume()
    {
        //_audioManager.SetVolume(_sliderVolume.value);
        _audioManager.SetVolume(_sliderVolume.value);

        PlayerPrefs.SetFloat("MusicVolume", _sliderVolume.value);
    }
    
}
