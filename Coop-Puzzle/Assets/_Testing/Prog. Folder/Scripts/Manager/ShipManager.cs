﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    private ShipState _shipState;
    
    [SerializeField] private GameObject _ship;
    [SerializeField] private GameObject _waypoint;

    private Vector3 offset;
    private void Update()
    {
        switch (_shipState)
        {
            case ShipState.IDLE:
                break;
            case ShipState.MOVE:
                break;
        }
    }
    
   

    private IEnumerator ZoomOut()
    {
        while (gameObject.transform.position != _waypoint.transform.position)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, _waypoint.transform.position, 5f);
            yield return null;
        }
        
    }

    private IEnumerator CameraFollow()
    {
        while (_shipState == ShipState.MOVE)
        {
            transform.position = _ship.transform.position;
            yield return null;
        }
    }
}

public enum ShipState
{
    IDLE,
    MOVE,
}
