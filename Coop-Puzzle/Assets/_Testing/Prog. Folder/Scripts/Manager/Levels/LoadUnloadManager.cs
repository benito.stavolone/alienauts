﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadUnloadManager : MonoBehaviour
{
    private LevelManager _levelManager;
    private ChangeLevelManager _changeLevelManager;

    private int _localIndex = 0;
    private bool _isEnd = false;

    void Start()
    {
        _levelManager = GetComponent<LevelManager>();
        _changeLevelManager = GetComponent<ChangeLevelManager>();
    }

    private void OnEnable()
    {
        EventManager.levelCompleted += OnLevelCompleted;
        EventManager.changeLevel += OnChangeLevel;
        EventManager.endLevel += OnEndLevel;
    }

    private void OnDisable()
    {
        EventManager.levelCompleted -= OnLevelCompleted;
        EventManager.changeLevel -= OnChangeLevel;
        EventManager.endLevel -= OnEndLevel;
    }

    private void OnLevelCompleted()
    {
        _localIndex++;

        if (_localIndex == 6)
            return;

        _levelManager.LevelList[_localIndex].SetActive(true);

        Invoke("DisableOldLevels", 5);

    }

    private void OnChangeLevel(int index)
    {
        Invoke("DisableOldLevels", 5);

        _localIndex = index;

        if(_localIndex != 5)
        {
            for (int i = _localIndex - 1; i < _localIndex + 2; i++)
            {
                _levelManager.LevelList[i].SetActive(true);
            }
        }
        else
        {
            _levelManager.LevelList[_localIndex -1].SetActive(true);
        }
    }

    private void DisableOldLevels()
    {
        if (!_isEnd)
        {
            for (int i = 0; i < _localIndex - 1; i++)
            {
                _levelManager.LevelList[i].SetActive(false);
            }
        }
    }

    private void OnEndLevel()
    {
        _isEnd = true;

        for (int i = 1; i < _levelManager.LevelList.Length; i++)
        {
            _levelManager.LevelList[i].SetActive(true);
        }
    }
}
