﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ChangeLevelManager : MonoBehaviour
{
    public bool shortCut = true;

    [SerializeField] PlayerController[] players;

    [SerializeField] LevelsData[] levelsData;

    private int _levelIndex;
    private int _actualLevelIndex;
    private bool _isFirstFrame;
    private LevelManager _levelManager;

    private void Start()
    {
        _levelManager = GetComponent<LevelManager>();
        _levelIndex = PlayerPrefs.GetInt("LevelsIndex");
        _actualLevelIndex = PlayerPrefs.GetInt("ActualLevelIndex");

        _isFirstFrame = true;

        EventManager.restartLevel += OnLevelRestart;
    }

    private void LateUpdate()
    {
        if (_isFirstFrame)
        {
            _isFirstFrame = false;

            if (_levelIndex != 0)
                MoveToLevels(_levelIndex);

        }

        if (shortCut)
        {
            /* Level 1 */

            if (Keyboard.current.digit1Key.wasPressedThisFrame)
                MoveToLevels(1);

            /* Level 2 */

            if (Keyboard.current.digit2Key.wasPressedThisFrame)
                MoveToLevels(2);

            /* Level 3 */

            if (Keyboard.current.digit3Key.wasPressedThisFrame)
                MoveToLevels(3);

            /* Level 4 */

            if (Keyboard.current.digit4Key.wasPressedThisFrame)
                MoveToLevels(4);

            /* Level 5 */

            if (Keyboard.current.digit5Key.wasPressedThisFrame)
                MoveToLevels(5);
        }
        
    }

    private void MoveToLevels(int keyCode)
    {
        for (int i = 0; i < levelsData[keyCode].arrivePlatform.Length; i++)
        {
            levelsData[keyCode].arrivePlatform[i].transform.position = levelsData[keyCode].arrivePlatform[i].Destination.position;
            levelsData[keyCode].arrivePlatform[i].transform.rotation = levelsData[keyCode].arrivePlatform[i].Destination.rotation;

            players[i].transform.position = levelsData[keyCode].arrivePlatform[i].Destination.position + new Vector3(0, 0.9f, 0);
            players[i].transform.rotation = levelsData[keyCode].arrivePlatform[i].Destination.rotation;

            Destroy(levelsData[keyCode].arrivePlatform[i]);
        }

        EventManager.ChangeLevel(keyCode);
    }

    private void OnLevelRestart()
    {
        PlayerPrefs.SetInt("LevelsIndex", _levelManager.LevelCounter);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnDisable()
    {
        EventManager.restartLevel -= OnLevelRestart;
    }
}

[System.Serializable]
public struct LevelsData
{
    public ArrivePlatform[] arrivePlatform;
}
