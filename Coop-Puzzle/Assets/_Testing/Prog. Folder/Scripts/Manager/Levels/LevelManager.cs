﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CSVWriterCustom))]
public class LevelManager : MonoBehaviour
{
    public enum LevelState
    {
        Transition,
        GamePlay
    }

    public LevelState _levelState = LevelState.GamePlay;

    [SerializeField] private GameObject[] levelsList;

    public GameObject[] LevelList
    {
        get { return levelsList; }
    }

    [SerializeField] private CanvasGroup _canvasGroup;

    private CSVWriterCustom _CSVWriter;

    /* How Many Character are on ArrivePlatform */
    private int _levelIndex = 0; 

    /* Current Level Number */
    private int _levelCounter = 0;

    public int LevelCounter
    {
        get { return _levelCounter; }
    }

    /* Planet Index */
    private int _planetIndex;

    private bool _changeLevel = false;

    private void Start()
    {
        _planetIndex = PlayerPrefs.GetInt("PlanetIndex");
        _CSVWriter = GetComponent<CSVWriterCustom>();

        EventManager.playerArrived += IncrementLevelIndex;
        EventManager.playerLeft += DecrementLevelIndex;
        EventManager.levelStarted += TurnToGamePlayState;
        EventManager.changeLevel += SetLevelCounterIndex;

        CheckArrivePlatform();
    }

    private void IncrementLevelIndex()
    {
        _levelIndex++;

        if (_levelIndex == 3)
        {
            Invoke("LaunchEvent", 0.2f);
        }

    }

    private void LaunchEvent()
    {
        _CSVWriter.UpdateLevel(_levelCounter);

        if(!_changeLevel)
            EventManager.OnLevelCompleted();

        _levelState = LevelState.Transition;

        _levelCounter++;

        CheckArrivePlatform();
        CheckBonusLevel();

        _levelIndex = 0;

        if(_levelCounter == 6)
        {
            GameManager.currentState = GameManager.GameState.EndAnimation;
            EventManager.Endlevel();
            _levelCounter--;
            Invoke("LevelFinished", 6);
        }
    }

    private void DecrementLevelIndex()
    {
        _levelIndex--;
    }

    private async void CheckArrivePlatform()
    {
        await Task.Yield();

        if (_levelCounter < levelsList.Length)
        {
            ArrivePlatform[] arrivePlatform = levelsList[_levelCounter].GetComponentsInChildren<ArrivePlatform>();

            foreach (ArrivePlatform p in arrivePlatform)
            {
                p.enabled = true;
            }
        }
    }

    private void CheckBonusLevel()
    {
        if (_levelCounter >= 3 && !levelsList[levelsList.Length - 1].activeInHierarchy)
        {
            levelsList[0].SetActive(false);
            levelsList[levelsList.Length - 1].SetActive(true);
        }
    }

    private void TurnToGamePlayState()
    {
        _levelState = LevelState.GamePlay;
    }

    private async void LevelFinished()
    {
        if (_planetIndex == GameManager.PlanetID)
            _planetIndex++;

        PlayerPrefs.SetInt("PlanetIndex", _planetIndex);
        PlayerPrefs.SetInt("LevelFinished", 1);

        await Utility.DoFade(_canvasGroup, 0, 1, 1);

        SceneManager.LoadScene("PlanetScene");
    }

    private void SetLevelCounterIndex(int index)
    {
        _changeLevel = true;

        _levelCounter = index;

        CheckArrivePlatform();
        CheckBonusLevel();

        Invoke("ResetChangeLevel", 1);
    }

    private void ResetChangeLevel()
    {
        _changeLevel = false;
    }

    private void OnDisable()
    {
        EventManager.playerArrived -= IncrementLevelIndex;
        EventManager.playerLeft -= DecrementLevelIndex;
        EventManager.levelStarted -= TurnToGamePlayState;
        EventManager.changeLevel -= SetLevelCounterIndex;

        if(PlayerPrefs.GetInt(GameManager.PlanetName) < _levelCounter)
            PlayerPrefs.SetInt(GameManager.PlanetName, _levelCounter);

        PlayerPrefs.SetInt("ActualPlanetIndex", GameManager.PlanetID);
        PlayerPrefs.SetString("StringPlanetScene", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetInt("LevelsIndex", _levelCounter);
    }
}

