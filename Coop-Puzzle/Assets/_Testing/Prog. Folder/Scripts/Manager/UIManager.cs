﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject PausePanel;
    [SerializeField] private GameObject SettingsPanel;
    [SerializeField] private CanvasGroup _canvasGroup;

    [Header("Audio")]

    [SerializeField] private AudioClip confirmButtonSound;
    [SerializeField] private AudioClip backButtonSound;
    [SerializeField] private AudioClip undoButtonSound;

    private bool _canUndo = true;
    private bool _localCanUndo = true;
    private bool _isInPause = false;
    

    private void Start()
    {
        EventManager.enableUndo += UpdateCanUndo;
        _canvasGroup.gameObject.GetComponent<Image>().raycastTarget = false;
    }

    public void Undo()
    {
        if (_canUndo && _localCanUndo && _isInPause == false)
        {
            EventManager.ChangeVFXClip(undoButtonSound);
            EventManager.Undo();
            _localCanUndo = false;
            Invoke("ResetUndo", 0.5f);
        }
    }

    private void Update()
    {
        if (Gamepad.current.rightTrigger.IsPressed() || Input.GetMouseButton(1))
            Undo();

        if (Gamepad.current.startButton.IsPressed())
            Pause();
    }

    private void ResetUndo()
    {
        _localCanUndo = true;
    }

    private void UpdateCanUndo(bool value)
    {
        _canUndo = value;
    }

    public void Pause()
    {
        if(GameManager.currentState == GameManager.GameState.GamePlay)
        {
            _isInPause = true;
            EventManager.ChangeVFXClip(confirmButtonSound);
            EventManager.Pause(true);
            PausePanel.SetActive(true);
        }
    }

    public void ClosePause(bool reallyDisable)
    {
        if (reallyDisable)
        {
            Invoke("DisablePause", 0.25f);
        }

        PausePanel.SetActive(false);
    }

    private void DisablePause()
    {
        _isInPause = false;
        EventManager.ChangeVFXClip(confirmButtonSound);
        EventManager.Pause(false);
    }

    public void SettingPause()
    {
        EventManager.ChangeVFXClip(confirmButtonSound);
        SettingsPanel.SetActive(true);
       // PausePanel.SetActive(false);
    }

    public void CloseSettings()
    {
        EventManager.ChangeVFXClip(backButtonSound);
        SettingsPanel.SetActive(false);
    }

    public void RestartPuzzle()
    {
        ClosePause(false);
        EventManager.ChangeVFXClip(confirmButtonSound);
        EventManager.RestartLevel();
    }

    public async void PlanetSelection()
    {
        EventManager.ChangeVFXClip(confirmButtonSound);

        _canvasGroup.gameObject.GetComponent<Image>().raycastTarget = true; 
        ClosePause(false);
        await Utility.DoFade(_canvasGroup, 0, 1, 1);
        PlayerPrefs.SetInt("LevelFinished", 0);
        SceneManager.LoadScene("PlanetScene");
    }

    private void OnDisable()
    {
        EventManager.enableUndo -= UpdateCanUndo;

        EventManager.Pause(false);
    }

    public void OnClickPlanetSelection()
    {
        SceneManager.LoadScene(1);
    }
}
