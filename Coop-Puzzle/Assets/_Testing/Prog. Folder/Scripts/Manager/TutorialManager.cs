﻿using System;
using System.Collections;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private GameObject tutorialComponent;

    [SerializeField] private TutorialTile[] _tutorialTile;

    GamePadCursor gamePadCursor;



    [Header("Part I")]

    [SerializeField] private TextMeshProUGUI _tutorialTextUp;
    [SerializeField] private TextMeshProUGUI _tutorialTextDown;

    [SerializeField] private Image ArrowLeft;
    [SerializeField] private Image ArrowRight;
    [SerializeField] private Image ArrowDown;

    [Header("Part II")]
    
    [SerializeField] private TextMeshProUGUI _tutorialTextJumpUp1;
    [SerializeField] private TextMeshProUGUI _tutorialTextJumpUp2;
    [SerializeField] private TextMeshProUGUI _tutorialTextJumpUp3;

    [SerializeField] private MeshRenderer jumpFeedBack2;
    [SerializeField] private MeshRenderer jumpFeedBack3;

    private GameManager gameManager;
    private LevelManager levelManager;
    private bool isActive;
    private int index;

    public float FadeAnimation;

    private void Start()
    {
        gamePadCursor = FindObjectOfType<GamePadCursor>();

        EventManager.levelCompleted += DisableFirstTutorial;
        EventManager.EnterTutorial += IncrementIndex ;
        EventManager.ExitTutorial += DecrementIndex;
        EventManager.changeLevel += DisableAllTutorials;
        
        isActive = false;

        gameManager = GetComponent<GameManager>();
        levelManager = GetComponent<LevelManager>();

        StartCoroutine("CheckGameManagerState");
    }

    private void OnDisable()
    {
        EventManager.EnterTutorial -= IncrementIndex;
        EventManager.ExitTutorial -= DecrementIndex;
        EventManager.changeLevel -= DisableAllTutorials;
        EventManager.levelCompleted -= DisableFirstTutorial;
    }

    private void Update()
    {
        if (((Gamepad.current.aButton.IsPressed() || Input.GetMouseButtonDown(0))) && GameManager.currentState == GameManager.GameState.GamePlay && !isActive)
        {
            RaycastHit hit;
            Ray ray;

            if (gamePadCursor.playerInput.currentControlScheme == gamePadCursor.gamepadScheme)
                ray = Camera.main.ScreenPointToRay(gamePadCursor.cursorTransform.transform.position);
            else
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.gameObject.GetComponent<PlayerController>() && hit.collider.gameObject.GetComponent<PlayerController>().Weight != 2)
                {
                    isActive = true;
                    StartCoroutine(FadeText( 0, 1, _tutorialTextDown));
                    StartCoroutine(FadeText( 1, 0, _tutorialTextUp));

                    StartCoroutine(FadeImage( 0, 1, ArrowDown));
                    StartCoroutine(FadeImage( 1, 0, ArrowLeft));
                    StartCoroutine(FadeImage( 1, 0, ArrowRight));
                }
            }
        }
    }

    public IEnumerator CheckGameManagerState()
    {
        while (gameManager._currentStartTransition != GameManager.StartLevelTransition.CharacterFade)
        {
            yield return null;
        }

        ActivateTutorial();
    }

    private void ActivateTutorial()
    {
        StartCoroutine(FadeImage( 0, 1, ArrowLeft)); 
        StartCoroutine(FadeImage( 0, 1, ArrowRight));
        StartCoroutine(FadeText( 0, 1, _tutorialTextUp));
    }

    public IEnumerator FadeText(float start, float end, TextMeshProUGUI Text)
    {
        var counter = 0f;
        while (counter < FadeAnimation)
        {
            counter += Time.deltaTime;
            Text.color = new Color(Text.color.r, Text.color.g, Text.color.b, Mathf.Lerp(start, end, counter / FadeAnimation)); //Mathf.Lerp(start, end, counter / fadeDuration);
            yield return null;
        }
    }

    public IEnumerator FadeImage( float start, float end, Image image)
    {
        var counter = 0f;
        while (counter < FadeAnimation)
        {
            counter += Time.deltaTime;
            image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(start, end, counter / FadeAnimation)); //Mathf.Lerp(start, end, counter / fadeDuration);
            yield return null;
        }
    }

    private void DisableFirstTutorial()
    {
        StartCoroutine(FadeText(1, 0, _tutorialTextDown));
        StartCoroutine(FadeImage(1, 0, ArrowDown));

        EventManager.levelCompleted -= DisableFirstTutorial;
    }

    private void DisableAllTutorials(int index)
    {
        if (index > 3)
            tutorialComponent.SetActive(false);
    }

    private void IncrementIndex()
    {
        index++;

        if(index < 3)
            _tutorialTile[index].enabled = true;

        switch (index)
        {
            case 0:
                    

                break;

            case 1:
                    if (_tutorialTextJumpUp2.color.a == 0)
                        StartCoroutine(FadeText(0, 1, _tutorialTextJumpUp2));

                    if (_tutorialTextJumpUp1.color.a == 1)
                        StartCoroutine(FadeText(1, 0, _tutorialTextJumpUp1));

                    jumpFeedBack2.material.color = Color.red;
                break;

            case 2:
                    if (_tutorialTextJumpUp3.color.a == 0)
                        StartCoroutine(FadeText(0, 1, _tutorialTextJumpUp3));

                    if (_tutorialTextJumpUp1.color.a == 1)
                        StartCoroutine(FadeText(1, 0, _tutorialTextJumpUp1));

                    if (_tutorialTextJumpUp2.color.a == 1)
                        StartCoroutine(FadeText(1, 0, _tutorialTextJumpUp2));

                    jumpFeedBack3.material.color = Color.yellow;
                break;
        }
    }

    private void DecrementIndex()
    {
        index--;
    }
}
