﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    public int planetID;
    public string planetName;

    public static int PlanetID;
    public static string PlanetName;

    private CSVWriterCustom _CSVWriter;

    [Header("StartLevelAnimation")]

    [SerializeField] private bool permitAnimation = false;

    [SerializeField] private PlayerController[] characters;

    [SerializeField] private GameObject[] rays;
    [SerializeField] private GameObject[] ps_incomingRays;

    [SerializeField] private float timeBeforeActiveRay = 1;
    [SerializeField] private float raySpeed = 2;
    [SerializeField] private float timeBeforeActiveCharacters = 1;
    private float timeActivePlayer = 1;

    private float _currentTimeRay, _currentTimeCharacters, _currentTimeActive;


    public PlayerController[] Characters
    {
        get { return characters; }
    }

    [Header("EndLevelAnimation")]

    [SerializeField] private CoreRotation coreRotation;

    public enum StartLevelTransition
    {
        Empty,
        RayFade,
        CharacterFade,
        GamePlay
    }

    public StartLevelTransition _currentStartTransition = StartLevelTransition.GamePlay;

    public enum GameState
    {
        Animation,
        GamePlay,
        Pause,
        EndAnimation
    }

    public static GameState currentState = GameState.Animation;

    public enum EndLevelTransition
    {
        None,
        Prepare,
        Rotation,
        NucleusCreation
    }

    public EndLevelTransition _currentEndTransition = EndLevelTransition.None;

    private void Awake()
    {
        PlanetID = planetID;
        PlanetName = planetName;

        _CSVWriter = GetComponent<CSVWriterCustom>();

        _CSVWriter.UpdateChapter(planetID);

    }

    private void Start()
    {
        if (permitAnimation && PlayerPrefs.GetInt("LevelsIndex") == 0)
        {
            _currentTimeCharacters = timeBeforeActiveCharacters;
            _currentTimeRay = timeBeforeActiveRay;
            _currentTimeActive = timeActivePlayer;

            ChangeGameState(GameState.Animation);
            ChangeStartTransitionState(StartLevelTransition.Empty);
        }
        else
        {
            _currentTimeCharacters = timeBeforeActiveCharacters / 4;
            _currentTimeRay = timeBeforeActiveRay / 4;
            _currentTimeActive = timeActivePlayer / 4;

            if (PlayerPrefs.GetInt("LevelsIndex") < 3)
            {
                ChangeGameState(GameState.Animation);
                ChangeStartTransitionState(StartLevelTransition.Empty);
            }
            else
            {
                ChangeGameState(GameState.Animation);
                ChangeStartTransitionState(StartLevelTransition.RayFade);
            }

        }

        EventManager.changeLevel += DisableIncomingRays;
        EventManager.pause += OnClickPause;
        EventManager.endLevel += StartEndTransition;
    }

    #region<StartTransition>

    private void ChangeStartTransitionState(StartLevelTransition newState)
    {
        _currentStartTransition = newState;

        switch (_currentStartTransition)
        {
            case StartLevelTransition.Empty:
                EmptyState();
                break;

            case StartLevelTransition.RayFade:
                StartCoroutine(RayMovement(rays[0], new Vector3(0,0,0.2f)));
                StartCoroutine(RayMovement(rays[1], new Vector3(0, 0, 0.2f)));
                StartCoroutine(RayMovement(rays[2], new Vector3(0, 0, 0.2f)));

                Invoke("ChangeToEnableCharacters", _currentTimeCharacters);
                Invoke("ActivePlayer", 1);
                break;

            case StartLevelTransition.CharacterFade:

                StartCoroutine(RayMovement(rays[0], new Vector3(0, 0, 3)));
                StartCoroutine(RayMovement(rays[1], new Vector3(0, 0, 3)));
                StartCoroutine(RayMovement(rays[2], new Vector3(0, 0, 3)));

                Invoke("ChangeToGamePlay", 2);
                break;
        }
    }

    private void Update()
    {
        if (Keyboard.current.eKey.wasPressedThisFrame)
            EventManager.Endlevel();
    }

    private void EmptyState()
    {
        for(int i = 0; i < characters.Length; i++)
        {
            characters[i].gameObject.SetActive(false);
        }

        Invoke("ChangeToRay", _currentTimeRay);
    }

    private void ChangeToRay()
    {
        ChangeStartTransitionState(StartLevelTransition.RayFade);
    }

    private IEnumerator RayMovement(GameObject rayToMove, Vector3 destination)
    {
        while(rayToMove.transform.localPosition.z != destination.z)
        {
            rayToMove.transform.localPosition = Vector3.MoveTowards(rayToMove.transform.localPosition, destination, raySpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void ChangeToEnableCharacters()
    {
        ChangeStartTransitionState(StartLevelTransition.CharacterFade);
    }

    private void ActivePlayer()
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].gameObject.SetActive(true);
            characters[i].SetDissolveMaterial(1, true);
        }

        Invoke("SetNormalMaterial", 2);
    }

    private void SetNormalMaterial()
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetNormalMaterial();
        }
    }

    private void ChangeToGamePlay()
    {
        ChangeGameState(GameState.GamePlay);
        ChangeStartTransitionState(StartLevelTransition.GamePlay);
    }

    private void DisableIncomingRays(int index)
    {
        if(index > 0)
        {
            for (int i = 0; i < ps_incomingRays.Length; i++)
                ps_incomingRays[i].SetActive(false);
        }
    }

    #endregion

    #region<EndTransition>

    private void ChangeEndTransitionState(EndLevelTransition newState)
    {
        _currentEndTransition = newState;

        switch (_currentEndTransition)
        {
            case EndLevelTransition.Prepare:
                Invoke("ChangeToRotation", 1.5f);
                break;

            case EndLevelTransition.Rotation:
                coreRotation.StartRotation();
                break;
        }
    }

    private void StartEndTransition()
    {
        ChangeEndTransitionState(EndLevelTransition.Prepare);
    }

    private void ChangeToRotation()
    {
        ChangeEndTransitionState(EndLevelTransition.Rotation);
    }

    #endregion

    #region<GameState>

    public void ChangeGameState(GameState newState)
    {
        currentState = newState;
    }

    #endregion

    private void OnClickPause(bool value)
    {
        if (value)
            ChangeGameState(GameState.Pause);
        else
            ChangeGameState(GameState.GamePlay);
    }

    private void OnDisable()
    {
        EventManager.changeLevel -= DisableIncomingRays;
        EventManager.pause -= OnClickPause;
        EventManager.endLevel -= StartEndTransition;
    }
}
