﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPlanetManager : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private AudioClip backButtonSound;

    private async void Awake()
    {
        await Utility.DoFade(_canvasGroup, 1, 0, 1);
    }

    public void OnClickHome()
    {
        if(AnimationManager._shipState == ShipState.IDLE)
        {
            EventManager.ChangeVFXClip(backButtonSound);
            AudioManager.instance.ClipController.FadeOut();
            SceneManager.LoadScene(0);
        }
    }
}
