﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CSVWriterCustom))]
public class MovesCounterManager : MonoBehaviour
{
    private int _movesCounter = 0;
    private CSVWriterCustom _CSVWriter;

    void Start()
    {
        _CSVWriter = GetComponent<CSVWriterCustom>();

        EventManager.movesIncrement += OnCharacterNewMove;
        EventManager.levelCompleted += ResetMoves;
    }

    private void OnCharacterNewMove()
    {
        _movesCounter++;
    }

    private void ResetMoves()
    {
        _CSVWriter.UpdateMoves(_movesCounter);
        _movesCounter = 0;
    }

    private void OnDisable()
    {
        EventManager.movesIncrement -= OnCharacterNewMove;
        EventManager.levelCompleted -= ResetMoves;
    }
}
