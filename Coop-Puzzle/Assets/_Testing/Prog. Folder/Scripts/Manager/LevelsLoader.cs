﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelsLoader : MonoBehaviour
{
    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private Slider _slider;

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsync(sceneIndex));
    }

  IEnumerator LoadAsync(int sceneindex)
  {
      AsyncOperation operation = SceneManager.LoadSceneAsync(sceneindex);
      loadingScreen.SetActive(true);
      while (!operation.isDone)
      {
          float progress = Mathf.Clamp01(operation.progress / .9f);
          _slider.value = progress;
          yield return null; 
      }
  }
}
