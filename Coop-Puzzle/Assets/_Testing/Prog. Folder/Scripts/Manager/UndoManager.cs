﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoManager : MonoBehaviour
{
    #region<Undo Settings>

    protected List<Vector3> _lastPosition = new List<Vector3>();
    protected List<Quaternion> _lastRotation = new List<Quaternion>();
    protected List<GameObject> _lastPlayerMoves = new List<GameObject>();
    protected List<TileController> _lastTile = new List<TileController>();

    #endregion

    private void OnEnable()
    {
        EventManager.savePosition += UpdatePosition;
        EventManager.saveRotation += UpdateRotation;
        EventManager.savePlayer += UpdatePlayer;


        EventManager.undo += OnUndo;
        EventManager.levelCompleted += ClearLists;
    }

    #region<Events>

    private void UpdatePosition(Vector3 position, TileController tile)
    {
        _lastPosition.Add(position);
        _lastTile.Add(tile);
    }

    private void UpdateRotation(Quaternion rotation)
    {
        _lastRotation.Add(rotation);
    }

    private void UpdatePlayer(GameObject player)
    {
        _lastPlayerMoves.Add(player);
    }

    #endregion

    private void OnUndo()
    {
        if (_lastPosition.Count > 0)
        {
            Invoke("Method", 0.05f);
        }
    }

    private void Method()
    {
        /* Move Player at Last Position and Rotation */
        if(_lastTile[_lastTile.Count - 1])
        {
            _lastPlayerMoves[_lastPlayerMoves.Count - 1].transform.position = _lastTile[_lastTile.Count - 1].transform.position + new Vector3(0,0.9f,0);
            _lastPlayerMoves[_lastPlayerMoves.Count - 1].transform.rotation = _lastRotation[_lastRotation.Count - 1];
        }

        /* Remove the current poition from List */
        _lastPosition.RemoveAt(_lastPosition.Count - 1);
        _lastTile.RemoveAt(_lastTile.Count - 1);
        _lastRotation.RemoveAt(_lastRotation.Count - 1);

        if (_lastPlayerMoves[_lastPlayerMoves.Count - 1])
        {
            _lastPlayerMoves[_lastPlayerMoves.Count - 1].GetComponent<PlayerController>().ChangePlayerState(PathfindingController.PlayerState.Idle);
        }


        _lastPlayerMoves.RemoveAt(_lastPlayerMoves.Count - 1);


    }

    private void ClearLists()
    {
        _lastPosition.Clear();
        _lastRotation.Clear();
        _lastPlayerMoves.Clear();
        _lastTile.Clear();
    }

    private void OnDisable()
    {
        EventManager.savePosition -= UpdatePosition;
        EventManager.saveRotation -= UpdateRotation;
        EventManager.savePlayer -= UpdatePlayer;

        EventManager.undo -= OnUndo;
        EventManager.levelCompleted -= ClearLists;
    }

}
