﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class ChangeSceneTest : MonoBehaviour
{
    private int currentIndex;

    private void Start()
    {
        currentIndex = PlayerPrefs.GetInt("PlanetIndex");
    }

    // Update is called once per frame
    void Update()
    {
        if (Keyboard.current.spaceKey.wasPressedThisFrame)
        {
            if (currentIndex == GameManager.PlanetID)
            {
                currentIndex++;
            }

            PlayerPrefs.SetInt("PlanetIndex", currentIndex);
            PlayerPrefs.SetInt("LevelFinished", 1);
            SceneManager.LoadScene("PlanetScene");
        }
    }
}
