﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlanetsButtons : MonoBehaviour
{
    [SerializeField] private string _sceneName;

    [Header("Sprite")]

    [SerializeField] private Sprite completedSprite;
    [SerializeField] private Sprite toCompleteSprite;

    [Header("Loading Screen")]

    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private Slider _slider;
    [SerializeField] private float loadingDelay = 2;

    [SerializeField] private AudioClip confirmButtonSound;

    public string planetName;
    [SerializeField] private GameObject galaxyCanvas;
    [SerializeField] private GameObject _canvasReference;
    private List<GameObject> _buttonList = new List<GameObject>();
    private int _buttonIndex;

    private void Start()
    {
        GetButtonsChild();

        _buttonIndex = PlayerPrefs.GetInt(planetName);

        EnableButton();
    }

    private void GetButtonsChild()
    {
        for(int i = 0; i < _canvasReference.transform.childCount; i++)
        {
            if (_canvasReference.transform.GetChild(i).GetComponent<Button>())
            {
                _buttonList.Add(_canvasReference.transform.GetChild(i).gameObject);
            }             
        }
    }

    private void EnableButton()
    {
        for (int i = 0; i < _buttonIndex; i++)
        {
            _buttonList[i + 1].SetActive(true);
        }

        for (int i = -1; i < _buttonIndex; i++)
        {
            _buttonList[i + 1].GetComponent<Image>().sprite = completedSprite;
        }

        if (_buttonIndex == 0)
            _buttonList[0].GetComponent<Image>().sprite = toCompleteSprite;
        else
            _buttonList[_buttonIndex].GetComponent<Image>().sprite = toCompleteSprite;
    }

    public void OnClickLevelButton(int level)
    {
        FadeOutAudio();
        EventManager.ChangeVFXClip(confirmButtonSound);

        PlayerPrefs.SetInt("LevelsIndex", level);
        loadingScreen.SetActive(true);

        Invoke("StartLoadingPlanet", loadingDelay);
    }

    private void StartLoadingPlanet()
    {
        StartCoroutine(LoadAsync(_sceneName));
    }

    public void FadeOutAudio()
    {
        galaxyCanvas.SetActive(false);
        StartCoroutine(AudioManager.instance.ClipController.FadeOut());
    }

    IEnumerator LoadAsync(string sceneindex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneindex);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            _slider.value = progress;
            yield return null;
        }
    }
}
