﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{
    /* Collection of our Planets */
    [SerializeField] private PlanetData[] _waypoints;

    [SerializeField] private GameObject Ship;
    [SerializeField] private float _shipSpeed;
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private GameObject LineTarget;
    [SerializeField] private float TimeLapsed;

    private CancellationTokenSource _cancellationToken = new CancellationTokenSource();

    private TrailRenderer _shipTrailRenderer;
    private bool _isLooping = true;
    private int _planetIndex;
    private int _selectedPlanet;
    private int actualIndex;

    private AnimationPlanetManager _animationPlanetManager;

    public static ShipState _shipState;

    public ShipState ShipState
    {
        get { return _shipState; }
    }

    [System.Obsolete]
    private void Start()
    {
        _shipState = ShipState.IDLE;

        _shipTrailRenderer = Ship.GetComponent<TrailRenderer>();
        _shipTrailRenderer.enabled = false;

        _animationPlanetManager = GetComponent<AnimationPlanetManager>();

        _lineRenderer.SetWidth(2.5f, 2.5f);

        _planetIndex = PlayerPrefs.GetInt("PlanetIndex");
        _selectedPlanet = PlayerPrefs.GetInt("SelectedPlanetIndex");

        /* At Start our Ship have to be in Last Position of the Planet Selected */
        Ship.transform.position = _waypoints[_selectedPlanet].ShipWaypoint.position;

        /* At Start the Last Planet Selected is the Actual */
        _waypoints[_selectedPlanet].IsActual = true;

        /* So Switch the Index */
        actualIndex = _selectedPlanet;

        if(_planetIndex-1 == _selectedPlanet && PlayerPrefs.GetInt("LevelFinished") == 1)
        {
            /* If next Planet of our last selected is Lock we need to start Animation */
            if (_waypoints[_selectedPlanet+1].PlanetStatus == PlanetStatus.LOCK || _selectedPlanet == 0)
            {
                /* Starting Update */
                UpdateAsync().AttachExternalCancellation(_cancellationToken.Token);
                _cancellationToken.RegisterRaiseCancelOnDestroy(this);
            }
        }

        CreateLines();
    }
    
    private void CreateLines()
    {
        /*Create Lines*/

        for (int i = 0; i < PlayerPrefs.GetInt("PlanetIndex")-1; i++)
        {
            var line = Instantiate(_lineRenderer);
            line.SetPosition(0, _waypoints[i].transform.position);
            line.SetPosition(1, _waypoints[i+1].transform.position);
        }
    }

    public async UniTask UpdateAsync()
    {
        while (_isLooping)
        {
            await LineTransition();
            await ShipTransitionAsync();
        }
    }

    /* Method which will be create the Line Renderer Animation */
    public async UniTask LineTransition()
    {
        _shipState = ShipState.MOVE;

        /* Move the LineTarget at Last selected planet Position */
        LineTarget.transform.position = _waypoints[_selectedPlanet].transform.position;

        /* Create the Line */
        var line = Instantiate(_lineRenderer);

        /* First Point : last selected planet position */
        line.SetPosition(0, _waypoints[_selectedPlanet].transform.position);

        /* Second Point : planet at position Unlocked Planet */
        var Destination = _waypoints[_planetIndex];

        while (TimeLapsed / 3f < 1)
        {
            TimeLapsed += Time.deltaTime;
            LineTarget.transform.position = Vector3.Lerp(_waypoints[_selectedPlanet].transform.position, Destination.transform.position, TimeLapsed / 3f);
            line.SetPosition(1, LineTarget.transform.position);
            await UniTask.Yield();
        }
    }

    /* Method which will Move Our Starship at the end of The Level */
    public async UniTask ShipTransitionAsync()
    {
        while (!_cancellationToken.Token.IsCancellationRequested)
        {
            _shipTrailRenderer.enabled = true;
            _shipState = ShipState.MOVE;
            Ship.transform.position = Vector3.MoveTowards(Ship.transform.position, _waypoints[_planetIndex].ShipWaypoint.position, _shipSpeed * Time.deltaTime);

            /* When our Starship is Arrived at destination */
            if (Ship.transform.position == _waypoints[_planetIndex].ShipWaypoint.position)
            {
                _waypoints[_planetIndex].ChangePlanetStatus(PlanetStatus.UNLOCK); /* => we Unlock the Destination planet */

                _shipState = ShipState.IDLE;
                PlayerPrefs.SetInt("PlanetIndex", _planetIndex+1);

                CheckActualPlanet(_planetIndex); /* => disable ActualIndex at other Planet and check ActualIndex at destinationPlanet */

                _isLooping = false;
                TimeLapsed = 0f;
                _shipState = ShipState.IDLE; /* our Starship Is In idle now */
                break;
            }

            await UniTask.Yield();
        }
    } 

    private void CheckActualPlanet(int actualIndex)
    {
        for (int i = 0; i < _waypoints.Length; i++)
        {
            if (i == actualIndex)
            {
                _waypoints[i].IsActual = true;
                actualIndex = _selectedPlanet;
            }
            else
                _waypoints[i].IsActual = false;
        }


    }


    public IEnumerator MoveStarShip(PlanetData planetData)
    {
        _shipState = ShipState.MOVE;
        _shipTrailRenderer.enabled = true;


        if (actualIndex >= planetData.PlanetIndex) // moving back
        {
            int index = actualIndex;

            while (Ship.transform.position != _waypoints[planetData.PlanetIndex].ShipWaypoint.position)
            {
                while(Ship.transform.position != _waypoints[index].ShipWaypoint.position)
                {
                    Ship.transform.position = Vector3.MoveTowards(Ship.transform.position, _waypoints[index].ShipWaypoint.position, _shipSpeed * Time.deltaTime);
                    yield return null;
                }

                index--;

                yield return null;
            }

            _shipState = ShipState.IDLE;
            CheckActualPlanet(planetData.PlanetIndex);
            actualIndex = planetData.PlanetIndex;
            _animationPlanetManager.ZoomTransition(planetData);
        }

        if (actualIndex < planetData.PlanetIndex) // moving forward
        {
            int index = actualIndex;

            while (Ship.transform.position != _waypoints[planetData.PlanetIndex].ShipWaypoint.position)
            {
                while (Ship.transform.position != _waypoints[index].ShipWaypoint.position)
                {
                    Ship.transform.position = Vector3.MoveTowards(Ship.transform.position, _waypoints[index].ShipWaypoint.position, _shipSpeed * Time.deltaTime);
                    yield return null;
                }

                index++;

                yield return null;
            }

            _shipState = ShipState.IDLE;
            CheckActualPlanet(planetData.PlanetIndex);
            actualIndex = planetData.PlanetIndex;
            _animationPlanetManager.ZoomTransition(planetData);
        }

    }

    private void OnDestroy()
    {
        PlayerPrefs.SetString("StringPlanetScene", "PlanetScene");
    }
}
