﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSwitchVersion : MonoBehaviour
{
    [SerializeField] private Transform[] _waypoints;
    
    public float speed;
    public LineRenderer _lineRenderer;
    public GameObject Ship;
    public GameObject LineTarget;
    public float TimeLapsed;

    public int IndexTest;
    
    private int waypointsIndex;

    public AnimationStage AnimationStage;

    private void Update()
    {
        switch (AnimationStage)
        {
            case AnimationStage.LINE:
                LineTransition();
                break;
            case AnimationStage.SHIP:
                ShipTransitionAsync();
                break;
        }
    }
    
    public  void ShipTransitionAsync()
    {
        Ship.transform.position = Vector3.MoveTowards(Ship.transform.position, _waypoints[waypointsIndex].transform.position, speed * Time.deltaTime);
        if (Ship.transform.position == _waypoints[waypointsIndex].transform.position)
        {
            //waypointsIndex += 1;
            TimeLapsed = 0f;
        }
    }
    
    public void LineTransition()
    {
        LineTarget.transform.position = Ship.transform.position; 
        _lineRenderer.SetPosition(0, Ship.transform.position);
        
        var Destination = _waypoints[waypointsIndex];
        
        while (TimeLapsed / 3f < 1) 
        { 
            TimeLapsed += Time.deltaTime; 
            LineTarget.transform.position = Vector3.Lerp(Ship.transform.position, Destination.position,TimeLapsed / 3f); 
            _lineRenderer.SetPosition(1,LineTarget.transform.position); 
            
        }
            
    }
           
}
    


public enum AnimationStage
{
    LINE,
    SHIP,
}
