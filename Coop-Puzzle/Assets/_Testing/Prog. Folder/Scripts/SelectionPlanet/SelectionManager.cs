﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    private AnimationManager _animationManager;
    
    public float Duration;
    public GameObject Waypoint;
    private CanvasGroup CanvasGroup;
    
    private bool Faded = false;

    private void Start()
    {
        _animationManager = GetComponent<AnimationManager>();
        CanvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        //if(Input.GetMouseButtonDown(0))
        //    SelectedPlanet();
    }

    private async UniTask SelectedPlanet()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
        {
            PlanetData planetData = hit.collider.GetComponent<PlanetData>();

            if (planetData && planetData.PlanetStatus == PlanetStatus.UNLOCK)
            {
                if (planetData.IsActual)
                {
                    await ZoomIn();
                    await DoFade(CanvasGroup, CanvasGroup.alpha, Faded ? 1 : 0);
                    //ChangeScene(planetData);
                    //Zoom / Fade / Carica Selezione Livelli
                }
                else 
                {
                    //StartCoroutine(_animationManager.MoveStarShip(planetData.LoadSceneIndex));   
                }
            }
                
        }
    }

    //public void ChangeScene(PlanetData planetData)
    //{
    //    EventManagerLevel.ChangeScene();
    //    PlayerPrefs.SetInt("SelectedPlanetIndex", planetData.LoadSceneIndex);
    //    planetData.LoadLevel();
    //}

    #region ANIMATION

    public async UniTask DoFade(CanvasGroup canvasGroup, float start, float end)
    {
        var counter = 0f;
        while (counter < Duration)
        {
            counter += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, counter / Duration);
            await UniTask.Yield();
        }
    }
    
    private async UniTask ZoomIn()
    {
        while (Camera.main.transform.position != Waypoint.transform.position)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Waypoint.transform.position, 3f);
            await UniTask.Yield();
        }
    }


    #endregion
}

