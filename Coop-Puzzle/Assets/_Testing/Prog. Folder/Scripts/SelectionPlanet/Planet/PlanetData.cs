﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlanetData : MonoBehaviour
{
    [SerializeField] private string planetName;

    [SerializeField] private Vector3 lockZoomRotation;

    [SerializeField] private Material _material;
    [SerializeField] private Material _lockMaterial;

    [SerializeField] private int planetIndex;
    [SerializeField] private float fadeDuration = 2;

    [SerializeField] private Transform _cameraPosition;
    [SerializeField] private Transform _shipWaypoint;

    [SerializeField] private GameObject myCanvas;

    [SerializeField] private GameObject satellitesFatherObject;

    public int PlanetIndex
    {
        get { return planetIndex; }
    }

    public Transform ShipWaypoint
    {
        get { return _shipWaypoint; }
    }

    public GameObject MyCanvas
    {
        get { return myCanvas; }
    }

    public string PlanetName
    {
        get { return planetName; }
    }

    /* If the Planet has the Ship */
    private bool _isActual = false;

    public bool IsActual
    {
        get { return _isActual; }
        set { _isActual = value; }
    }

    public Transform CameraPosition => _cameraPosition;

    public PlanetStatus PlanetStatus;

    private void Awake()
    {
        /* If the Planet is Locked */
        if (PlanetStatus == PlanetStatus.LOCK)
        {
            /* If the Index of the Planet is minor than PlanetIndex */
            if (planetIndex < PlayerPrefs.GetInt("PlanetIndex"))
                ChangePlanetStatus(PlanetStatus.UNLOCK); /* Unlock It */
            else
                ChangePlanetStatus(PlanetStatus.LOCK); /* Lock it */
        }
        else
            /* If the planet is Unlock in the Inspector, call the UnlockMethod */
            ChangePlanetStatus(PlanetStatus.UNLOCK); 
    }

    public void ChangePlanetStatus(PlanetStatus newState)
    {
        PlanetStatus = newState;

        switch (PlanetStatus)
        {
            case PlanetStatus.LOCK:
                PlanetLock();
                break;
            case PlanetStatus.UNLOCK:
                PlanetUnlock();
                break;
        }
    }
    private void PlanetLock()
    {
        gameObject.GetComponent<Renderer>().material = _lockMaterial;

        /* If our Planet has multiple Mesh as Children */
        if (gameObject.transform.childCount > 0)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                /* Set Lock material to all Children */
                if(gameObject.transform.GetChild(i).GetComponent<NotDisableMe>() != null)
                {
                    gameObject.transform.GetChild(i).GetComponent<MeshRenderer>().material = _lockMaterial;
                    gameObject.transform.GetChild(i).GetComponent<NotDisableMe>().CheckChildren(_lockMaterial);
                }
                else
                    gameObject.transform.GetChild(i).gameObject.SetActive(false);           
            }
        }
        else
        {
            /* If the planet has no children we set lockMaterial to it and disable tweener */
            gameObject.GetComponent<Renderer>().material = _lockMaterial;
        }
        
    }

    private void PlanetUnlock()
    {
        gameObject.GetComponent<Renderer>().material = _material;

        if (satellitesFatherObject)
        {
            for(int i=0;i< satellitesFatherObject.transform.childCount; i++)
            {
                satellitesFatherObject.transform.GetChild(i).GetComponent<MeshRenderer>().material = satellitesFatherObject.GetComponent<AsteroidsBelt>().AsteroidMaterial;
            }
        }

        /* If our Planet has multiple Mesh as Children */
        if (gameObject.transform.childCount > 0)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                /* Set original material to all Children */
                //StartCoroutine(ChangeMaterial(_material, gameObject.transform.GetChild(i).GetComponent<Renderer>()));
                if (gameObject.transform.GetChild(i).GetComponent<NotDisableMe>() != null)
                {
                    gameObject.transform.GetChild(i).GetComponent<MeshRenderer>().material = gameObject.transform.GetChild(i).GetComponent<NotDisableMe>().OriginalMaterial;
                    gameObject.transform.GetChild(i).GetComponent<NotDisableMe>().CheckChildren(gameObject.transform.GetChild(i).GetComponent<NotDisableMe>().OriginalMaterial);
                }

                else
                    gameObject.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        else
        {
            /* If the planet has no children we set original Material to it and enable tweener */
            gameObject.GetComponent<Renderer>().material = _material;
        }
    }

    /* Method to change Material in a Faded way */
    private IEnumerator ChangeMaterial(Material EndingMaterial, Renderer Planet)
    {
        var counter = 0f;
        while (counter < fadeDuration)
        {
            counter += Time.deltaTime;
            Color.Lerp(_lockMaterial.color, EndingMaterial.color, counter / fadeDuration);
            yield return null;
        }
    }

    public void LockRotation()
    {
        transform.eulerAngles = lockZoomRotation;
    }
}
