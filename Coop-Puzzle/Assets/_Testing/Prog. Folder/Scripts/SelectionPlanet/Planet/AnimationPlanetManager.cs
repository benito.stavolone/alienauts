﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.InputSystem;

public class AnimationPlanetManager : MonoBehaviour
{
    [SerializeField] private float fadeDuration;
    [SerializeField] private float cameraSpeed;

    [SerializeField] private CanvasGroup CanvasGroup;
    [SerializeField] private GameObject Canvas;

    private GameObject CurrentCamera;
    private PlanetData CurrentPlanet;
    private AnimationManager _animationManager;
    private bool Faded = false;

    public Camera MainCamera;
    private bool isZoom = false;

    [SerializeField] private GameObject HomeButton;
    [SerializeField] private TextMeshProUGUI GalaxyTitle;

    [SerializeField] private AudioClip backButtonSound;

    [SerializeField] private float DelayTextType;
    
    private GamePadCursor gamePadCursor;

    private void Start()
    {
        gamePadCursor = FindObjectOfType<GamePadCursor>();
        CanvasGroup = GetComponent<CanvasGroup>();
        _animationManager = GetComponent<AnimationManager>();
    }

    private void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Gamepad.current.buttonSouth.IsPressed()) && _animationManager.ShipState == ShipState.IDLE)
            SelectedPlanet();
    }

    public async UniTask DoFade(CanvasGroup canvasGroup, float start, float end)
    {
        var counter = 0f;
        while (counter < fadeDuration)
        {
            counter += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, counter / fadeDuration);
            await UniTask.Yield();
        }
    }

    private void SelectedPlanet()
    {
        /* Check Mouse Ray only if we arent in Zoom Prospective */
        if (!isZoom)
        {
            Ray ray;

            if (gamePadCursor.playerInput.currentControlScheme == gamePadCursor.gamepadScheme)
                ray = Camera.main.ScreenPointToRay(gamePadCursor.cursorTransform.transform.position);
            else
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                PlanetData planetData = hit.collider.GetComponent<PlanetData>();

                if (planetData && planetData.PlanetStatus == PlanetStatus.UNLOCK)
                {
                    /* if the Planet Selected is The Actual (where the starship is) we can Zoom It */
                    if (planetData.IsActual)
                    {
                        ZoomTransition(planetData);                      
                    }
                    else
                        /* OtherWise we have to Mov the Starship at Selected Planet */
                        StartCoroutine(_animationManager.MoveStarShip(planetData));
                }
            }
        }
    }

    public async void ZoomTransition(PlanetData planetData)
    {
        isZoom = true;
        planetData.MyCanvas.SetActive(true);

        DoFade(CanvasGroup, 0, 1).Forget();

        HomeButton.SetActive(false);
        StartCoroutine(Utility.TypingtextCurutine(planetData.PlanetName, GalaxyTitle, DelayTextType));
        // GalaxyTitle.text = planetData.PlanetName;

        await FadeCenter(50.0f); 
        await ChangeCamera(planetData.CameraPosition.gameObject);
        await StopPlanet(planetData);
        await DoFade(CanvasGroup, 1, 0);

        PlayerPrefs.SetInt("SelectedPlanetIndex", planetData.PlanetIndex);
    }

    private async void ZoomOut(PlanetData planetData)
    {
        planetData.MyCanvas.SetActive(false);

        DoFade(CanvasGroup,0,1).Forget();
        Canvas.gameObject.SetActive(false);

        HomeButton.SetActive(true);
        StartCoroutine(Utility.TypingtextCurutine("Henize Galaxy", GalaxyTitle, DelayTextType));
       // GalaxyTitle.text = "Henize Galaxy";

        await PlayPlanet(planetData);
        await FadeCenter(55.0f);
        await Test();
        await DoFade(CanvasGroup, 1, 0);
    }


    public async UniTask StopPlanet(PlanetData planetData)
    { 
        planetData.GetComponent<Tween>().StopTween();
        planetData.LockRotation();

        CurrentPlanet = planetData;
    } 

    public async UniTask PlayPlanet(PlanetData planetData)
    { 
        planetData.GetComponent<Tween>().PlayTween();
    }

    public async UniTask FadeCenter(float EndValue)
    { 
        while (MainCamera.fieldOfView != EndValue)
        {
            MainCamera.fieldOfView = Mathf.MoveTowards(MainCamera.fieldOfView, EndValue,  2.5f*Time.deltaTime);
            await UniTask.Yield();
        }
    }

    public async UniTask ChangeCamera(GameObject NewCamera)
    {
        Camera.main.gameObject.SetActive(false);
        Canvas.SetActive(true);
        NewCamera.SetActive(true);
        CurrentCamera = NewCamera;
        isZoom = true;
        await UniTask.Yield();
    }

    public void OnClickBackCamera()
    {
        EventManager.ChangeVFXClip(backButtonSound);
        ZoomOut(CurrentPlanet);
    }
    
    public async UniTask Test()
    {
        MainCamera.gameObject.SetActive(true);
        CurrentCamera.SetActive(false);
        MainCamera.GetComponent<Camera>().fieldOfView = 55f;
        isZoom = false;

        await UniTask.Yield();
    }
}
