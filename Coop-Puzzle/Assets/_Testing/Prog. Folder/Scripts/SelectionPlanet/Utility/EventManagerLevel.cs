﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManagerLevel : MonoBehaviour
{
    public static Action changeScene;

    public static void ChangeScene()
    {
        changeScene?.Invoke();
    }
}
