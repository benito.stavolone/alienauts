﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotDisableMe : MonoBehaviour
{
    [SerializeField] private Material originalMaterial;

    public Material OriginalMaterial
    {
        get { return originalMaterial; }
    }

    public void CheckChildren(Material newMaterial)
    {
        if(transform.childCount > 0)
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<MeshRenderer>().material = newMaterial;
            }
        }
    }
}
