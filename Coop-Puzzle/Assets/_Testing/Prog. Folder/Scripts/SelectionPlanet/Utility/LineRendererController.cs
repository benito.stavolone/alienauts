﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererController : MonoBehaviour
{

    void OnEnable()
    {
        gameObject.SetActive(true);
        EventManagerLevel.changeScene += OnChangeScene;
    }

    private void OnChangeScene()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        EventManagerLevel.changeScene -= OnChangeScene;
    }
}
