﻿using UnityEngine;
public class SelectionLevel : MonoBehaviour
{
    [SerializeField] private PlanetStatus PlanetStatus;
  //  public GameObject Planet;
    private void Update()
    {
        switch (PlanetStatus)
        {
            case PlanetStatus.LOCK:
                LockPlanet();
                break;
            case PlanetStatus.UNLOCK:
                UnlockPlanet();
                break;
        }
    }

    private void LockPlanet()
    {
      // gameObject.GetComponent<ShaderGraphVfxAsset>(). = Color.black;
       Debug.Log(gameObject.GetComponent<Renderer>().sharedMaterial.color);
    }

    private void UnlockPlanet()
    {
        gameObject.GetComponent<Renderer>().material.color = Color.white;
        Debug.Log("Unlock");
    }
}

