﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private CPC_CameraPath cameraPath;

    public CameraWayPoint[] cameraData;
    private int _cameraIndex = 0;

    private void Awake()
    {
        cameraPath = GetComponent<CPC_CameraPath>();

        cameraPath.PausePath();
        cameraPath.PlayPath(26);
        cameraPath.PausePath();
    }

    private void Start()
    {
        EventManager.levelCompleted += Movement;
        EventManager.changeLevel += ChangeLevel;
    }

    private void Movement()
    {
        cameraPath.ResumePath();
    }

    private void Update()
    {
        if(GameManager.currentState != GameManager.GameState.EndAnimation)
        {
            if (cameraPath.IsPlaying())
            {
                if (transform.position == cameraData[_cameraIndex].waypoint.position)
                {
                    cameraPath.PausePath();
                    EventManager.OnLevelStarted();
                    _cameraIndex++;
                }
            }
        }
    }

    private void OnDisable()
    {
        EventManager.levelCompleted -= Movement;
        EventManager.changeLevel -= ChangeLevel;
    }

    private void ChangeLevel(int index)
    {
        transform.position = cameraData[index-1].waypoint.position;
        transform.rotation = cameraData[index-1].waypoint.rotation;
        _cameraIndex = index;

        cameraPath.SetCurrentWayPoint(index);
    }
}

[System.Serializable]
public struct CameraWayPoint
{
    public Transform waypoint;
}
