﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlayerSettings")]
public class PlayerScriptable : ScriptableObject
{
    [Header("Player Settings")]

    public float ClimbHeight = 1;

    public float ClimbVelocity = 2;

    [Min(10)]
    public float JumpVelocity = 2;

    [Range(0, 8)]
    public float MoveSpeed = 2;

    [Min(1)]
    public int Weight;

    public Color NotSelectColor;
}
