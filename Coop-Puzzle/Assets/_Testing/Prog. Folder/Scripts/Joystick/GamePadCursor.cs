﻿using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;

public class GamePadCursor : MonoBehaviour
{
    [SerializeField]
    public PlayerInput playerInput;

    [SerializeField]
    public RectTransform cursorTransform;

    [SerializeField]
    private float cursorSpeed = 1000;

    [SerializeField]
    private RectTransform canvasReactTransform;

    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    public Image cursorImage;

    private Mouse _virtualMouse;
    private bool _previousMouseState;
    private Camera _maincamera;
    private float _padding = 10f;
    public Mouse _currentMouse;

    private string previousControlScheme = "";
    public string gamepadScheme = "Gamepad";
    public string mouseScheme = "Keyboard&Mouse";

    private void OnEnable()
    {
        _maincamera = Camera.main;
        _currentMouse = Mouse.current;

        if (_virtualMouse == null)
        {
            _virtualMouse = (Mouse)InputSystem.AddDevice("VirtualMouse");
        }
        else
        {
            if (!_virtualMouse.added)
            {
                InputSystem.AddDevice(_virtualMouse);
            }
        }

        InputUser.PerformPairingWithDevice(_virtualMouse, playerInput.user);

        if(cursorTransform != null)
        {
            Vector2 position = cursorTransform.anchoredPosition;
            InputState.Change(_virtualMouse.position, position);
        }

        InputState.Change(_virtualMouse.position, new Vector2(Screen.width/2, Screen.height/2));

        InputSystem.onAfterUpdate += UpdateMotion;
        playerInput.onControlsChanged += OnControlsChanged;
    }

    private void OnDisable()
    {
        InputSystem.RemoveDevice(_virtualMouse);
        InputSystem.onAfterUpdate -= UpdateMotion;
        playerInput.onControlsChanged -= OnControlsChanged;
    }

    private void UpdateMotion()
    {
        if (_virtualMouse == null || Gamepad.current == null)
            return;

        Vector2 deltaValue = Gamepad.current.leftStick.ReadValue();
        deltaValue *= cursorSpeed * Time.deltaTime;

        Vector2 currentPosition = _virtualMouse.position.ReadValue();
        Vector2 newPosition = currentPosition + deltaValue;

        newPosition.x = Mathf.Clamp(newPosition.x, _padding, Screen.width -_padding);
        newPosition.y = Mathf.Clamp(newPosition.y, _padding, Screen.height -_padding);

        InputState.Change(_virtualMouse.position, newPosition);
        InputState.Change(_virtualMouse.delta, deltaValue);

        bool aButtonisPressed = Gamepad.current.aButton.IsPressed();
        if(_previousMouseState != Gamepad.current.aButton.isPressed)
        {
            _virtualMouse.CopyState<MouseState>(out var mouseState);
            mouseState.WithButton(MouseButton.Left, Gamepad.current.aButton.IsPressed());
            InputState.Change(_virtualMouse, mouseState);
            _previousMouseState = aButtonisPressed;
        }

        AnchorCursor(newPosition);
    }

    private void AnchorCursor(Vector2 position)
    {
        Vector2 anchoredPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasReactTransform, position, canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : _maincamera, out anchoredPosition);
        cursorTransform.anchoredPosition = anchoredPosition;
    }

    private void OnControlsChanged(PlayerInput input)
    {
        if(playerInput.currentControlScheme == mouseScheme && previousControlScheme != mouseScheme)
        {
            cursorTransform.gameObject.SetActive(false);
            Cursor.visible = true;
            _currentMouse.WarpCursorPosition(_virtualMouse.position.ReadValue());
            previousControlScheme = mouseScheme;
        }
        else
        {
            if(playerInput.currentControlScheme == gamepadScheme && previousControlScheme != gamepadScheme)
            {
                cursorTransform.gameObject.SetActive(true);
                Cursor.visible = false;
                InputState.Change(_virtualMouse.position, _currentMouse.position.ReadValue());
                AnchorCursor(_currentMouse.position.ReadValue());
                previousControlScheme = gamepadScheme;
            }
        }
    }
}
