﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class TileController : MonoBehaviour
{
    #region<TileStates>

    public enum TileStates
    {
        Default,
        Current,
        Target,
        Selectable
    }

    public TileStates _tileState = TileStates.Default;

    public TileStates TileState
    {
        get { return _tileState; }
    }

    public enum NotCheck
    {
        Null,
        Forward,
        BackWard,
        Left,
        Right
    }

    public NotCheck _notcheck = NotCheck.Null;

    #endregion

    #region<TileSettings>

    public bool isInPath = false;

    public bool _walkable;

    public bool Selectable = true;

    /* List that will Contains the Neighbors of the Tile */
    private List<TileController> _adjacencyList = new List<TileController>();

    public List<TileController> AdjacencyList
    {
        get { return _adjacencyList; }
    }

    private Color _myMaterial;
    private Transform _rendererTransform;

    #endregion

    #region<BFS-Settings>

    /* These Variables are Mandatory for the Implementation of the BFS Algorithm */
    private bool _isVisited = false;

    public bool IsVisited
    {
        get { return _isVisited; }
        set { _isVisited = value; }
    }

    [SerializeField] public TileController parentSelf;
    private TileController _parent;

    public TileController Parent
    {
        get { return _parent; }
        set { _parent = value; }
    }

    #endregion

    private bool _hasDirectionLine;

    public bool HasDirectionLine
    {
        get { return _hasDirectionLine; }
    }

    private void Awake()
    {
        InitializeTile();

        EventManager.levelCompleted += ResetValue;
    }

    private void Start()
    {
        if (gameObject.GetComponentInChildren<JumpInterface>())
            _hasDirectionLine = true;
        else
            _hasDirectionLine = false;
    }

    private void InitializeTile()
    {
        //if (transform.childCount > 0)
        //    _rendererTransform = transform.GetChild(0);
        //else
        //    _rendererTransform = transform;

        //_myMaterial = _rendererTransform.GetComponent<Renderer>().material.color;
    }

    public void ChangeTileState(TileStates newState)
    {
        _tileState = newState;
    }

    public void FindNeighboursTile(float jumpHeight)
    {
        ResetValue();

        CheckTile(transform.forward, jumpHeight);
        CheckTile(-transform.forward, jumpHeight);
        CheckTile(transform.right, jumpHeight);
        CheckTile(-transform.right, jumpHeight);
    }


    private void CheckTile(Vector3 direction, float jumpHeight)
    {
        Vector3 halfExtents = new Vector3(0.25f, (1 + jumpHeight) / 2, 0.25f);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction / 1.25f, halfExtents);

        foreach(Collider item in colliders)
        {
            /* Conditions For Jump */
            TileController tile = item.GetComponent<TileController>();

            if(tile!=null && tile._walkable)
            {
                RaycastHit hit;

                /* If there is No One Above the Tile to Jump to */
                if(!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1))
                {
                    if (_notcheck != NotCheck.Null)
                        CheckNotJumpZone(direction, tile);
                    else
                        AddTileInList(tile);
                }            

                if(GetComponentInParent<PlayerController>() == null)
                {
                    if (item.GetComponent<LadderController>())
                    {
                        if (item.GetComponent<LadderController>().GetTileController()._walkable)
                            _adjacencyList.Add(item.GetComponent<LadderController>().GetTileController());
                    }
                }
            }
        }
    }

    public void ResetValue()
    {
        _adjacencyList.Clear();

        ChangeTileState(TileStates.Default);

        _isVisited = false;

        _parent = null;
    }

    private void CheckNotJumpZone(Vector3 direction, TileController tile)
    {
        switch (_notcheck)
        {
            case NotCheck.Forward:
                if (direction == transform.forward)
                {
                    if (!tile.GetComponentInParent<PlayerController>())
                        AddTileInList(tile);
                }
                else
                    AddTileInList(tile);

                break;

            case NotCheck.Left:
                if (direction == -transform.right)
                {
                    if (!tile.GetComponentInParent<PlayerController>())
                        AddTileInList(tile);
                }
                else
                    AddTileInList(tile);

                break;

            case NotCheck.Right:
                if (direction == transform.right)
                {
                    if (!tile.GetComponentInParent<PlayerController>())
                        AddTileInList(tile);
                }
                else
                    AddTileInList(tile);

                break;

            case NotCheck.BackWard:
                if (direction == -transform.forward)
                {
                    if (!tile.GetComponentInParent<PlayerController>())
                        AddTileInList(tile);
                }
                else
                    AddTileInList(tile);

                break;
        }
    }

    private void AddTileInList(TileController tile)
    {
        if (!tile.GetComponent<LadderController>())
            _adjacencyList.Add(tile);
    }
}
