﻿using UnityEngine;

public class TutorialTile : CheckPlayer
{
    [SerializeField] private int PlayerWeight;

    private bool isOveR;
    protected override void PerformAction()
    {
        base.PerformAction();
        if (_playerController.Weight == PlayerWeight)
        {
            EventManager.EnterTileTutorial();
        }
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if(_playerController.Weight == PlayerWeight)
                EventManager.ExitTileTutorial();

            _playerController = null;
        }
    }
    
}
