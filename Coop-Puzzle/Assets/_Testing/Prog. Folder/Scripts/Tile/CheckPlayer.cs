﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayer : MonoBehaviour
{
    private LayerMask _player;
    protected bool _isVisible = true;
    protected bool isUndoMode = false;

    protected PlayerController _playerController;

    protected List<Vector3> tilePosition = new List<Vector3>();

    private void Awake()
    {
        _playerController = null;
        _player = LayerMask.GetMask("Player");
    }

    protected virtual void Start()
    {
        StartCoroutine(RayCastLauncher());
    }

    private void OnBecameVisible()
    {
        _isVisible = true;
        StartCoroutine(RayCastLauncher());
    }

    private void OnBecameInvisible()
    {
        _isVisible = false;
        StopAllCoroutines();
    }

    protected IEnumerator RayCastLauncher()
    {
        RaycastHit hit;
        while (_isVisible)
        {
            if (Physics.Raycast(transform.position, Vector3.up, out hit, 2, _player ))
            {
                if (_playerController == null) 
                { 
                    _playerController = hit.collider.GetComponent<PlayerController>();
                    if(!isUndoMode)
                        PerformAction(); 
                } 
            }
            else
                DesformAction();

            yield return null;
        }
    }
    
    protected virtual void PerformAction()
    {
        
    }

    protected virtual void DesformAction()
    {

    }
}
