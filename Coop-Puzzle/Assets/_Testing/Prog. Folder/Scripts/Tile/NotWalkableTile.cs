﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotWalkableTile : CheckPlayer
{
    private TileController _myTile;

    protected override void Start()
    {
        base.Start();

        _myTile = GetComponent<TileController>();
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        _myTile._walkable = false;
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            _myTile._walkable = true;
        }

        _playerController = null;
    }
}
