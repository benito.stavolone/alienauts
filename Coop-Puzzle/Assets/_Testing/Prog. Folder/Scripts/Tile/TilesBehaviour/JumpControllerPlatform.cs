﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpControllerPlatform : CheckPlayer
{
    protected override void PerformAction()
    {
        base.PerformAction();

        if (_playerController._playerState == PathfindingController.PlayerState.Idle)
        {
            if (_playerController.Weight != 1)
                Invoke("EnableTile", 0.25f);
        }
        else
            _playerController = null;       
    }

    private void EnableTile()
    {
        if(_playerController)
            _playerController.MyPersonalTile.GetComponent<BoxCollider>().enabled = true;
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (_playerController.Weight != 1)
                _playerController.MyPersonalTile.GetComponent<BoxCollider>().enabled = false;
        }

        _playerController = null;
    }
}
