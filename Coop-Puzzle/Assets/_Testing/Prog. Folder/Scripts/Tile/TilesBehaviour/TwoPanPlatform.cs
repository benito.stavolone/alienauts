﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPanPlatform : CheckPlayer
{
    private int _weightOnPlatform = 0;
    private Vector3 _initialPosition;

    private TileController _myTileController;

    private TwoPanController _twoPanController;

    private List<Vector3> _lastPosition = new List<Vector3>();
    private List<int> _lastWeight = new List<int>();

    private Vector3 _finalPositionReference;

    public TileController MyTileController
    {
        get { return _myTileController; }
    }

    public int WeightOnPlatform
    {
        get { return _weightOnPlatform; }
        set { _weightOnPlatform = value; }
    }

    public Vector3 InitialPosition
    {
        get { return _initialPosition; }
        set { _initialPosition = value; }
    }

    public PlayerController PlayerController
    {
        get { return _playerController; }
    }

    private void Start()
    {
        _initialPosition = transform.localPosition;

        _myTileController = GetComponent<TileController>();

        EventManager.saveTilePosition += SavePosition;
        EventManager.undo += Undo;
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        Invoke("Action", 0.3f);
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if(!isUndoMode)
                WeightOnPlatform = 0;

            _playerController.CanSave = true;

            Invoke("ReverseAction", 0.3f);

            _playerController.IsTimeToBlock = false;
        }

        _playerController = null;
    }

    private void Action()
    {
        if (_playerController)
        {
            WeightOnPlatform = _playerController.Weight;

            EventManager.OnPlayerScales(_twoPanController);
        }
    }

    private void ReverseAction()
    {
        EventManager.OnPlayerScales(_twoPanController);
    }

    public IEnumerator Movement(Vector3 finalPosition)
    {
        if (!isUndoMode)
        {
            _twoPanController.SavePosition();

            _twoPanController.IsAnimation = true;
            _finalPositionReference = finalPosition;

            while (transform.localPosition != finalPosition)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, finalPosition, 2 * Time.deltaTime);
                yield return null;
            }

            _twoPanController.IsAnimation = false;
        }   
    }

    public void GetTwoPanController(TwoPanController twoPanController)
    {
        _twoPanController = twoPanController;
    }

    private void SavePosition()
    {
        if (_twoPanController && _twoPanController.IsAnimation)
            _lastPosition.Add(_finalPositionReference);
        else
            _lastPosition.Add(transform.localPosition);

        _lastWeight.Add(_weightOnPlatform);
    }

    private void Undo()
    {
        if (_lastPosition.Count > 0)
        {
            isUndoMode = true;

            transform.localPosition = _lastPosition[_lastPosition.Count - 1];

            WeightOnPlatform = _lastWeight[_lastWeight.Count - 1];

            _lastPosition.RemoveAt(_lastPosition.Count - 1);

            _lastWeight.RemoveAt(_lastWeight.Count - 1);

            Invoke("ResetUndoMode", 0.3f);
        }      
    }

    private void ResetUndoMode()
    {
        isUndoMode = false;
    }

    private void OnDisable()
    {
        EventManager.saveTilePosition -= SavePosition;
        EventManager.undo -= Undo;
    }
}
