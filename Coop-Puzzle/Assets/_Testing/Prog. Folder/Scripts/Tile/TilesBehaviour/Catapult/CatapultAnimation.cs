﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatapultAnimation : MonoBehaviour
{
    [SerializeField] private float animationSpeed;

    private Vector3 _finalRotation;

    private bool _forward = true;
    private bool _active = false;

    void Update()
    {
        if (_active)
        {
            if (_forward)
            {
                transform.localEulerAngles = Vector3.MoveTowards(transform.localEulerAngles, _finalRotation, animationSpeed * Time.deltaTime);

                if (transform.localEulerAngles.z >= _finalRotation.z)
                {
                    _forward = false;
                }
            }
            else
            {
                transform.localEulerAngles = Vector3.MoveTowards(transform.localEulerAngles, Vector3.zero, animationSpeed * Time.deltaTime);

                if (transform.localEulerAngles ==  Vector3.zero)
                {
                    _forward = true;
                    _active = false;
                }
            }
        }
    }

    public void SetRotationProperty(Vector3 finalRotation, bool active)
    {
        _finalRotation = finalRotation;
        _active = active;
    }
}
