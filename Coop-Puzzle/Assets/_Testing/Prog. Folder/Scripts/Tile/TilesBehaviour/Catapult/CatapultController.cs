﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatapultController : CheckPlayer
{
    #region<CatapultSettings>

    [SerializeField] private AnimationCurve lerpCurve;
    [SerializeField] private Vector3 lerpOffset;
    [SerializeField] private float lerpTime = 3;

    private float _timer = 0;
    private float _curveLimit = 0.3f;

    [Header("Animation")]

    [SerializeField] private CatapultAnimation objectToAnimate;

    #endregion

    #region<CatapultDestinations>

    [Header("Players Destination")]

    [SerializeField] private Transform destinationWeight1;
    [SerializeField] private Transform destinationWeight2;
    [SerializeField] private Transform destinationWeight3;

    private Transform _destination;

    public Transform DestinationWeight1
    {
        get { return destinationWeight1; }
    }

    public Transform DestinationWeight2
    {
        get { return destinationWeight2; }
    }

    public Transform DestinationWeight3
    {
        get { return destinationWeight3; }
    }

    #endregion

    private PlayerController _selectedPlayer;
    private Coroutine _animationCoroutine;
    private bool _coroutineIsRunning = false;

    private TileController _myTileController;
    private bool _isInPath = false;

    public GameObject smokePrefab;

    protected override void Start()
    {
        base.Start();

        _myTileController = GetComponent<TileController>();

        EventManager.selectedPlayer += OnPlayerSelection;
    }

    private void Update()
    {
        if (_myTileController.isInPath && !_isInPath)
        {
            _isInPath = true;
            CheckPlayerWeight(_selectedPlayer);

            if(_destination)
                _destination.GetComponent<TileController>()._walkable = false;
        }
        else if(!_myTileController.isInPath)
        {
            _isInPath = false;
        }
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        if(_animationCoroutine != null)
            StopCoroutine(_animationCoroutine);

        CheckPlayerWeight(_playerController);

        Invoke("CatapultCoroutine", 0.25f);
    }

    private void CatapultCoroutine()
    {
        if (_playerController)
        {
            _playerController.isPushing = false;
            _playerController.IsTimeToBlock = true;

            if (smokePrefab)
                PoolManager.instance.ReuseObject<Component>(smokePrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);

            if(_destination)
                _destination.GetComponent<TileController>()._walkable = false;
            _coroutineIsRunning = true;
            EventManager.EnableUndo(false);

            _animationCoroutine = StartCoroutine(CatapultAnimation());

            objectToAnimate.SetRotationProperty(new Vector3(0, 0, 45), true);
        }
    }

    private IEnumerator CatapultAnimation()
    {
        while(_timer < lerpTime)
        {
            _timer += Time.deltaTime;

            float lerpRatio = _timer / lerpTime;

            Vector3 positionOffset = lerpCurve.Evaluate(lerpRatio) * lerpOffset;

            if (lerpRatio > _curveLimit)
                break;

            _playerController.transform.position = Vector3.Lerp(_playerController.transform.position, _destination.position, lerpRatio) + positionOffset;

            yield return null;
        }

        _timer = lerpTime;

        _playerController.IsTimeToBlock = false;
        _destination.GetComponent<TileController>()._walkable = true;
        _coroutineIsRunning = false;
        EventManager.EnableUndo(true);
        _playerController.isPushing = false;

        SetPlayerSettings();
    }

    private void CheckPlayerWeight(PlayerController player)
    {
        switch (player.Weight)
        {
            case 1:
                _destination = destinationWeight1;
                break;
            case 2:
                _destination = destinationWeight2;
                break;
            case 3:
                _destination = destinationWeight3;
                break;
        }
    }

    private void SetPlayerSettings()
    {
        _playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);

        _playerController.transform.localPosition = new Vector3(Mathf.RoundToInt(_playerController.transform.localPosition.x), 0.9f, Mathf.RoundToInt(_playerController.transform.localPosition.z));

        _destination.GetComponent<TileController>()._walkable = true;
        _playerController = null;
        _timer = 0;


    }

    protected override void DesformAction()
    {
        base.DesformAction();


        if(_playerController && !_coroutineIsRunning)
        {
            if(_destination)
                _destination.GetComponent<TileController>()._walkable = true;
        }

        if (!_coroutineIsRunning)
        {
            _playerController = null;

        }

    }

    public bool CheckCatapultDestination()
    {
        switch (_selectedPlayer.Weight)
        {
            case 1:

                if(destinationWeight1 == null)
                {
                    return false;
                }
                else
                {
                    if (destinationWeight1.GetComponent<CatapultController>() == null)
                    {
                        if (destinationWeight1.GetComponentInChildren<PlayerController>() == null && !destinationWeight1.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (destinationWeight1.GetComponent<CatapultController>().DestinationWeight1.GetComponentInChildren<PlayerController>() == null &&
                            !destinationWeight1.GetComponent<CatapultController>().DestinationWeight1.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                }

                

            case 2:

                if (destinationWeight2 == null)
                {
                    return false;
                }
                else
                {
                    if (destinationWeight2.GetComponent<CatapultController>() == null)
                    {
                        if (destinationWeight2.GetComponentInChildren<PlayerController>() == null && !destinationWeight2.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (destinationWeight2.GetComponent<CatapultController>().DestinationWeight2.GetComponentInChildren<PlayerController>() == null &&
                            !destinationWeight2.GetComponent<CatapultController>().DestinationWeight2.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                }           

            case 3:

                if (destinationWeight3 == null)
                {
                    return false;
                }
                else
                {
                    if (destinationWeight3.GetComponent<CatapultController>() == null)
                    {
                        if (destinationWeight3.GetComponentInChildren<PlayerController>() == null && !destinationWeight3.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (destinationWeight3.GetComponent<CatapultController>().DestinationWeight3.GetComponentInChildren<PlayerController>() == null &&
                            !destinationWeight3.GetComponent<CatapultController>().DestinationWeight3.GetComponent<TileController>().isInPath)
                            return true;
                        else
                            return false;
                    }
                }             
        }

        return false;
    }

    private void OnPlayerSelection(PlayerController player)
    {
        _selectedPlayer = player;
    }

    private void OnDisable()
    {
        EventManager.selectedPlayer -= OnPlayerSelection;
    }
}
