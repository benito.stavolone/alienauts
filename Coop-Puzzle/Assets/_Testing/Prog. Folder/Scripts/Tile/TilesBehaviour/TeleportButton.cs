﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportButton : CheckPlayer
{
    [SerializeField] private Transform destination;

    private PlayerController _selectedPlayer;
    private TileController _destinationTile;

    public bool active = true;

    protected override void Start()
    {
        base.Start();

        active = true;
        _destinationTile = destination.GetComponent<TileController>();
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        if (_playerController)
        {
            if(active)
                Invoke("Teleport", 0.25f);
        }

    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (!active)
            {
                active = true;
            }

            _playerController = null;
        }
    }

    private void Teleport()
    {
        if (_playerController)
        {
            _playerController.DisableParticle();

            /* Creating Clone */
            CreateClone();

            GameManager.currentState = GameManager.GameState.Animation;

            _playerController.gameObject.transform.position = new Vector3(destination.transform.position.x, destination.transform.position.y + 0.9f, destination.transform.position.z);
            _playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);
            _destinationTile.GetComponent<TeleportButton>().active = false;

            /* Materialized Player */
            MaterializedCharacter();

            _playerController.IsTimeToBlock = false;
            destination.GetComponent<TileController>()._walkable = true;
            _playerController = null;
        }
    }

    private void CreateClone()
    {
        GameObject playerClone = Instantiate(_playerController.gameObject, _playerController.transform.position, _playerController.transform.rotation);
        playerClone.GetComponent<PlayerController>().CanSave = false;
        playerClone.layer = 0;

        /*Dissolve Clone*/
        playerClone.GetComponent<PlayerController>().SetDissolveMaterial(-1, false);

        Destroy(playerClone, 1.5f);
    }

    private void MaterializedCharacter()
    {
        /*Materialized*/
        _playerController.SetDissolveMaterial(1, true);
        Invoke("EndTeleport", 1.5f);
    }

    private void EndTeleport()
    {
        GameManager.currentState = GameManager.GameState.GamePlay;
    }

    public bool CheckDestination()
    {
        if (destination.GetComponentInChildren<PlayerController>() == null || !_destinationTile.isInPath)
            return true;
        else
            return false;
    }
}
