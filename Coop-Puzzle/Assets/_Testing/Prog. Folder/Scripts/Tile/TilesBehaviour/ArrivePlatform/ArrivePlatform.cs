﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivePlatform : CheckPlayer
{
    [SerializeField] private int weightRequired;
    [SerializeField] private Transform destination;

    public Transform Destination
    {
        get { return destination; }
    }

    [SerializeField] private float delay = 1;

    [Header("Movement Settings")]

    [SerializeField] private float movementSpeed = 3;

    [SerializeField] private float rotationSpeed = 1;

    private void Start()
    {
        EventManager.levelCompleted += PlatformMovement;
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        if(_playerController.Weight == weightRequired)
        {
            EventManager.OnPlayerArrived();
        }
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (_playerController.Weight == weightRequired)
                EventManager.OnPlayerLeft();
        }

        _playerController = null;
    }

    private void PlatformMovement()
    {
        //_playerController.transform.SetParent(transform);

        StartCoroutine(Movement());
        StartCoroutine(Rotation());
    }

    private IEnumerator Movement()
    {
        yield return new WaitForSeconds(delay);
        if (destination == null)
            yield return null;

        while (transform.position != destination.position)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination.position, movementSpeed * Time.deltaTime);        
            yield return null;
        }

        Destroy(this);
        StopAllCoroutines();
        EventManager.levelCompleted -= PlatformMovement;
    }

    private IEnumerator Rotation()
    {
        yield return new WaitForSeconds(delay);
        if (destination == null)
            yield return null;

        while (transform.rotation != destination.rotation)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, destination.rotation, rotationSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void OnDisable()
    {
        EventManager.levelCompleted -= PlatformMovement;
    }
}
