﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NucleusPlatform : CheckPlayer
{
    [SerializeField] private int weightRequired;

    protected override void PerformAction()
    {
        base.PerformAction();

        if (_playerController.Weight == weightRequired)
        {
            EventManager.OnPlayerArrived();
        }
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (_playerController.Weight == weightRequired)
                EventManager.OnPlayerLeft();
        }

        _playerController = null;
    }
}
