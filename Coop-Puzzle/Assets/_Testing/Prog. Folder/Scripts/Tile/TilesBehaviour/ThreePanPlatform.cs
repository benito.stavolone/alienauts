﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreePanPlatform : CheckPlayer
{
    [SerializeField] private GameObject myJumpFeedback;
    [SerializeField] private GameObject mySparkParticle;

    private TileController _myTile;

    public GameObject MyJumpFeedback
    {
        get { return myJumpFeedback; }
    }

    public enum PanState
    {
        SideLeft,
        SideRight,
        Down
    }

    public PanState _panState;

    private ThreePanController _threePanController;

    private int _weightOnPlatform = 0;

    public int WeightOnPlatform
    {
        get { return _weightOnPlatform; }
        set { _weightOnPlatform = value; }
    }

    public PlayerController PlayerController
    {
        get { return _playerController; }
        set { _playerController = value; }
    }

    private void Start()
    {
        _threePanController = GetComponentInParent<ThreePanController>();

        _myTile = GetComponent<TileController>();
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        WeightOnPlatform = _playerController.Weight;
        //_playerController.CanSave = false;

        if (!isUndoMode)
        {
            if (_threePanController.CheckPansWeight())
                _playerController.BlockCharacter();
        }
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            _weightOnPlatform = 0;

            _playerController.CanSave = true;

            if (!isUndoMode)
            {
                _threePanController.CheckPansWeight();
                _playerController.IsTimeToBlock = false;
            }
        }

        _playerController = null;
    }

    private void Update()
    {
        if (_myTile.isInPath)
            _threePanController.BlockPlatform(false);
        else
            _threePanController.BlockPlatform(true);
    }

    public void ChangePanState(PanState newState)
    {
        _panState = newState;
    }

    public void ActiveUndoMode()
    {
        //isUndoMode = true;
    }

    #region<Movement>

    public IEnumerator Movement(Vector3 finalPosition)
    {
        BeforeMovement();

        while (transform.localPosition != finalPosition)
        {
            _isVisible = false;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, finalPosition, 2 * Time.deltaTime);
            yield return null;
        }

        transform.localPosition = Utility.Round(finalPosition, 0);

        AfterMovement();
    }

    private void BeforeMovement()
    {
        mySparkParticle.SetActive(true);
    }

    private void AfterMovement()
    {
        mySparkParticle.SetActive(false);

        _isVisible = true;
        StartCoroutine(RayCastLauncher());

        _threePanController.Animation = false;

        Invoke("EnableUndo", 0.5f);
    }

    private void EnableUndo()
    {
        EventManager.EnableUndo(true);
    }

    #endregion
}
