﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMovement : CheckPlayer
{
    public enum MovementType
    {
        Position,
        Rotation
    }

    public MovementType movementType = MovementType.Position;

    [SerializeField] private Vector3 from;
    [SerializeField] private Vector3 to;

    [SerializeField] private float movementSpeed = 2;

    [Header("Animation")]

    [SerializeField] private BlockTransform[] childToAnimate;
    [SerializeField] private float animationDelay = 1;

    private Vector3 _rotationDestination;

    [Header("Color")]

    [SerializeField] private MeshRenderer meshToColor;
    [SerializeField] private Color offColor;
    private Color _startColor;


    private Vector3 _direction;
    private bool _isObstacolated = false;

    private ButtonController _buttonController;
    private PlayerController _playerToMove;

    public PlayerController PlayerToMove
    {
        get { return _playerToMove; }
    }

    public ButtonController ButtonController
    {
        set { _buttonController = value; }
    }

    protected override void Start()
    {
        base.Start();

        CheckMyDirection();

        if (meshToColor)
        {
            _startColor = meshToColor.material.GetColor("ColorRef");
            meshToColor.material.SetColor("ColorRef", offColor);
        }
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        _playerToMove = _playerController;
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        _playerToMove = null;
        _playerController = null;
    }

    private void CheckMyDirection()
    {
        if(movementType == MovementType.Position)
        {
            if (from.x < to.x) //right direction
                _direction = transform.right;
            else //left direction
            {
                if (from.x > to.x)
                    _direction = -transform.right;
            }

            if (from.y < to.y) // up direction
                _direction = Vector3.zero;
            else // down direction
            {
                if (from.y > to.y)
                    _direction = -transform.up;
            }
                
            if (from.z < to.z) // forward direction
                _direction = transform.forward;
            else // backward direction 
            {
                if(from.z > to.z)
                    _direction = -transform.forward;
            }              
        }
    }

    public IEnumerator Move()
    {
        if (_playerController) /* During the Animation, if there is a Character on it*/
            _playerController.IsTimeToBlock = true; /* it cannot move */

        _isObstacolated = false;

        EventManager.EnableUndo(false);

        if(movementType == MovementType.Position) //Position
        {
            RaycastHit hit;

            while (transform.localPosition != to)
            {
                if (!Physics.Raycast(transform.position, _direction, out hit, 0.5f))
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, to, movementSpeed * Time.deltaTime);

                    if(childToAnimate.Length > 0)
                    {
                        if (transform.localPosition.y >= from.y)
                            BlockChild(true, animationDelay);
                        else
                            BlockChild(true, 0);
                    }

                }
                else
                {
                    _isObstacolated = true;
                    break;
                }

                yield return null;
            }

            if (!_isObstacolated)
                transform.localPosition = Utility.Round(to, 0);
            else
                transform.localPosition = Utility.Round(transform.localPosition,0);

            if (_playerController) /* At the end of the Animation, if there is a Character on it*/
                _playerController.IsTimeToBlock = false; /* it can move */

            _buttonController.ResetWalkableTile(true);
        }
        else //Rotation
        {
            _rotationDestination = to;

            while (transform.localRotation != Quaternion.Euler(to))
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation,Quaternion.Euler(to), movementSpeed * Time.deltaTime);
                yield return null;

                if (childToAnimate.Length > 0)
                    BlockChild(false, animationDelay);
            }

            transform.localEulerAngles = to;

            if (_playerController) /* At the end of the Animation, if there is a Character on it*/
                _playerController.IsTimeToBlock = false; /* it can move */

            _buttonController.ResetWalkableTiles(true);
        }

        EventManager.EnableUndo(true);
    }

    public IEnumerator RevertMove()
    {
        if (_playerController) /* During the Animation, if there is a Character on it*/
            _playerController.IsTimeToBlock = true; /* it cannot move */

        EventManager.EnableUndo(false);

        if (movementType == MovementType.Position)
        {
            RaycastHit hit;

            while (transform.localPosition != from)
            {
                if(_direction == Vector3.zero || _direction == -transform.up)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, from, movementSpeed * Time.deltaTime);

                    if (childToAnimate.Length > 0)
                    {
                        if (transform.localPosition.y <= from.y)
                            BlockChild(true, animationDelay);
                        else
                            BlockChild(true, 0);
                    }
                }
                else
                {
                    if (!Physics.Raycast(transform.position, -_direction, out hit, 0.5f))
                    {
                        transform.localPosition = Vector3.MoveTowards(transform.localPosition, from, movementSpeed * Time.deltaTime);

                        if (childToAnimate.Length > 0)
                        {
                            if (transform.localPosition.y <= from.y)
                                BlockChild(true, animationDelay);
                            else
                                BlockChild(true, 0);
                        }
                    }
                }

                yield return null;
            }

            transform.localPosition = Utility.Round(from, 0);

            if (_playerController) /* At the end of the Animation, if there is a Character on it*/
                _playerController.IsTimeToBlock = false; /* it can move */

            _buttonController.ResetWalkableTile(true);
        }
        else
        {
            _rotationDestination = from;

            while (transform.localRotation != Quaternion.Euler(from))
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(from), movementSpeed * Time.deltaTime);
                yield return null;

                if (childToAnimate.Length > 0)
                    BlockChild(false, animationDelay);
            }

            transform.localEulerAngles = from;

            if (_playerController) /* At the end of the Animation, if there is a Character on it*/
                _playerController.IsTimeToBlock = false; /* it can move */

            _buttonController.ResetWalkableTiles(true);
        }

        EventManager.EnableUndo(true);
    }

    #region<Color>

    public void TurnOnColor()
    {
        meshToColor.material.SetColor("ColorRef", _startColor);
    }

    public void TurnOffColor()
    {
        meshToColor.material.SetColor("ColorRef", offColor);
    }

    #endregion

    #region<Animation>

    private void BlockChild(bool position, float animationDelay)
    {
        childToAnimate[0].GetComponent<BlockTransform>().BlockPosition();
        childToAnimate[1].GetComponent<BlockTransform>().BlockPosition();

        if (position)
        {
            Invoke("ButtonAnimationPosition", animationDelay);
            Invoke("ButtonAnimationPosition2", animationDelay * 2);
        }
        else
        {
            Invoke("ButtonAnimationRotation", animationDelay);
            Invoke("ButtonAnimationRotation2", animationDelay * 2);
        }

    }

    private void ButtonAnimationPosition()
    {
        childToAnimate[0].GetComponent<BlockTransform>().StartAnimation(true, Vector3.zero);
    }

    private void ButtonAnimationPosition2()
    {
        childToAnimate[1].GetComponent<BlockTransform>().StartAnimation(true, Vector3.zero);
    }

    private void ButtonAnimationRotation()
    {
        childToAnimate[0].GetComponent<BlockTransform>().StartAnimation(false, _rotationDestination);
    }

    private void ButtonAnimationRotation2()
    {
        childToAnimate[1].GetComponent<BlockTransform>().StartAnimation(false, _rotationDestination);
    }

    #endregion
}
