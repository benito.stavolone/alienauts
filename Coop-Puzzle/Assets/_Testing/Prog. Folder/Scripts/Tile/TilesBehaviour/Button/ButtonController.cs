﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : CheckPlayer
{ 
    [SerializeField] private ButtonMovement objectToMove;
    [SerializeField] private int weightRequired = 2;

    [Header("Animation")]

    [SerializeField] private GameObject objectToPush;
    private float yObjectToPushStartPosition;

    /* Tile to Move if ButtonMovement has not child */
    private TileController _tileToMove;

    /* Tile to Move if ButtonMovement has child */
    private List<TileController> _tilesToMove = new List<TileController>();
    private TileController _myTile;

    private Coroutine _coroutineMovement;
    private Coroutine _coroutineRevert;

    protected override void Start()
    {
        base.Start();

        GetTileToMoveReference();

        _myTile = GetComponent<TileController>();
        objectToMove.ButtonController = this;

        StartCoroutine(CheckConditionsTile());
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        if (_playerController)
        {
            if (_playerController.Weight == weightRequired)
            {
                if (_coroutineRevert != null)
                    StopCoroutine(_coroutineRevert);

                PushButton();

                /* Check if the Character on Button is Selected */
                if(objectToMove.movementType == ButtonMovement.MovementType.Position)
                    StartCoroutine(CheckPlayerControllerSelected());

                Invoke("StartCoroutine",0.3f);
            }
        }
    }

    private void StartCoroutine()
    {
        if (_playerController)
        {
            _coroutineMovement = StartCoroutine(objectToMove.Move());
        }            
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (_playerController.Weight == weightRequired)
            {
                if (_coroutineMovement!=null)
                    StopCoroutine(_coroutineMovement);

                _playerController.IsTimeToBlock = false;

                LeaveButton();

                Invoke("RevertMovementCoroutine", 0.3f);
            }
        }

        _playerController = null;
    }

    private void RevertMovementCoroutine()
    {
        _coroutineRevert = StartCoroutine(objectToMove.RevertMove());
    }

    /* Check if Position or Rotation and take Tile of object or objects to move */
    private void GetTileToMoveReference()
    {
        if (objectToMove.movementType == ButtonMovement.MovementType.Position)
            _tileToMove = objectToMove.GetComponent<TileController>();
        else
        {
            for (int i = 0; i < objectToMove.transform.childCount; i++)
            {
                if (objectToMove.transform.GetChild(i).GetComponent<TileController>())
                    _tilesToMove.Add(objectToMove.transform.GetChild(i).GetComponent<TileController>());
            }
        }
    }

    private IEnumerator CheckConditionsTile()
    {
        /* If MovementType of ButtonMovement is Position */

        if(objectToMove.GetComponent<ButtonMovement>().movementType == ButtonMovement.MovementType.Position)
        {
            while (_tileToMove)
            {
                /* Check if ButtonController is in Path => player cannot select buttonMovement and tile after it */
                CheckButtonControllerIsInPath();

                /* Check if ButtonMovement is in Path => player cannot select ButtonController*/
                CheckButtonMovementIsInPath();

                yield return null;
            }
        }

        /* If MovementType of ButtonMovement is Rotation */

        else
        {
            while (_tilesToMove.Count > 0)
            {
                /* Check if ButtonController is in Path => player cannot select buttonMovement and tile after it */
                CheckButtonControllerIsInPathRotation();

                /* Check if ButtonMovement is in Path => player cannot select ButtonController*/
                CheckButtonMovementIsInPathRotation();

                yield return null;
            }
        }
    }

    #region<TileToMove Position>

    private IEnumerator CheckPlayerControllerSelected()
    {
        while (_playerController)
        {
            if (_playerController._selectionState == PathfindingController.SelectionState.Selectable)
                objectToMove.GetComponent<TileController>()._walkable = false;
            else
                objectToMove.GetComponent<TileController>()._walkable = true;

            yield return null;
        }
    }

    private void CheckButtonControllerIsInPath()
    {
        if (_myTile.isInPath) /* If ButtonController isInPath => ButtonMovement is not Walkable */
            ResetWalkableTile(false);
        else
        {
            if (_playerController == null) /* If ButtonController is Not InPath => check if there is a character on it */
                ResetWalkableTile(true);
        }       
    }

    private void CheckButtonMovementIsInPath()
    {
        if (!_playerController) /* if There is No-one on ButtonController it cannot be Selectable */
        {
            if (_tileToMove.isInPath) /* if ButtonMovement isInPath => ButtonController is Not Selectable */
                _myTile.Selectable = false;
            else
            {
                if(objectToMove.PlayerToMove) /* if Tile To Move is Not In Path => Check if there is a character on it */
                {
                    if (objectToMove.PlayerToMove.MyPersonalTile && objectToMove.PlayerToMove.MyPersonalTile.isInPath) /* If the Character's on ButtonMovement tile is in Path */
                        _myTile.Selectable = false;
                    else
                        _myTile.Selectable = true;
                }
            }
        }
        else
        {
            if (_tileToMove.isInPath) /* if ButtonMovement isInPath => PlayerController is Blocked */
                _playerController.IsTimeToBlock = true;
            else
            {
                if (objectToMove.PlayerToMove) /* if Tile To Move is Not In Path => Check if there is a character on it */
                {
                    if (objectToMove.PlayerToMove.MyPersonalTile && objectToMove.PlayerToMove.MyPersonalTile.isInPath) /* If the Character's on ButtonMovement tile is in Path */
                        _playerController.IsTimeToBlock = true;
                    else
                        _playerController.IsTimeToBlock = false;
                }
            }
        }
    }

    public void ResetWalkableTile(bool value)
    {
        _tileToMove._walkable = value;
    }

    #endregion

    #region<TileToMove Rotation>

    private void CheckButtonControllerIsInPathRotation()
    {
        if (_myTile.isInPath) /* If ButtonController isInPath => ButtonMovement is not Walkable */
            ResetWalkableTiles(false);
        else
        {
            if (_playerController == null) /* If ButtonController is Not InPath => check if there is a character on it */
                ResetWalkableTiles(true);
        }
    }

    private void CheckButtonMovementIsInPathRotation()
    {
        if (!_playerController) /* if There is No-one on ButtonController it cannot be Selectable */
        {
            for(int i = 0; i < _tilesToMove.Count; i++)
            {
                if (_tilesToMove[i].isInPath) /* if ButtonMovement isInPath => ButtonController is Not Selectable */
                {
                    _myTile.Selectable = false;
                    break;
                }
                else
                    _myTile.Selectable = true;
            }
        }
        else
        {
            for (int i = 0; i < _tilesToMove.Count; i++)
            {
                if (_tilesToMove[i].isInPath) /* if ButtonMovement isInPath => PlayerController is Blocked */
                {
                    _playerController.IsTimeToBlock = true;
                    break;
                }
                else
                    _playerController.IsTimeToBlock = false;
            }
        }
    }

    public void ResetWalkableTiles(bool value)
    {
        for(int i = 0; i < _tilesToMove.Count; i++)
        {
            _tilesToMove[i]._walkable = value;
        }
    }

    #endregion

    #region<Animation>

    private void PushButton()
    {
        objectToMove.TurnOnColor();
        yObjectToPushStartPosition = objectToPush.transform.localPosition.y;
        StartCoroutine(LerpButton(new Vector3(0, yObjectToPushStartPosition - 0.03f, 0)));
    }

    private IEnumerator LerpButton(Vector3 destination)
    {
        while(objectToPush.transform.localPosition != destination)
        {
            objectToPush.transform.localPosition = Vector3.MoveTowards(objectToPush.transform.localPosition, destination, Time.deltaTime * 2);
            yield return null;
        }
    }

    private void LeaveButton()
    {
        objectToMove.TurnOffColor();
        StartCoroutine(LerpButton(new Vector3(0, yObjectToPushStartPosition, 0)));
    }

    #endregion
}
