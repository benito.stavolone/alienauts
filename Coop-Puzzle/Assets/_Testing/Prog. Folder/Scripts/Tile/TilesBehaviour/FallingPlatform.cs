﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : CheckPlayer
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private GameObject smokeParticle;

    [SerializeField] private BoxCollider _boxColliderWeight3;

    private Vector3 _destination = Vector3.zero;
    private Vector3 _initialPosition = Vector3.zero;

    private TileController _myTile;

    private ChangeDimension _changeDimension;

    protected override void Start()
    {
        base.Start();

        if(smokeParticle)
            _changeDimension = smokeParticle.GetComponent<ChangeDimension>();

        _myTile = GetComponent<TileController>();

        EventManager.saveTilePosition += UpdatePositionList;
        EventManager.undo += Undo;
    }

    protected override void PerformAction()
    {
        base.PerformAction();

        //if(GetComponent<BlockCharacters>())
            _playerController.BlockCharacter();

        _destination = new Vector3(transform.position.x, transform.position.y - _playerController.Weight, transform.position.z);
        _initialPosition = transform.position;

        if (_playerController.Weight == 3 && _boxColliderWeight3)
            _boxColliderWeight3.enabled = true;

        Invoke("StartCoroutine", 0.25f);
    }

    private void StartCoroutine()
    {
         StartCoroutine(DownMovement());
    }

    private IEnumerator DownMovement()
    {
        EventManager.enableUndo(false);

        _playerController.IsTimeToBlock = true;

        _playerController.EnableMyTile(false);


        if (_changeDimension)
            _changeDimension.WeightReference = _playerController.Weight;

        while (Mathf.Abs(transform.position.y - _destination.y) != 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, _destination, movementSpeed * Time.deltaTime);
            yield return null;
        }

        EventManager.enableUndo(true);
        _playerController.IsTimeToBlock = false;
        _playerController.EnableMyTile(true);
    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            StopCoroutine(DownMovement());

            if (_playerController.Weight == 3 && _boxColliderWeight3)
                _boxColliderWeight3.enabled = false;

            Invoke("StartUpCoroutine", 0.25f);

            Invoke("ResetUndoMode", 0.2f);
        }

        _playerController = null;
    }

    private void StartUpCoroutine()
    {
        StartCoroutine(UpMovement());
    }

    private IEnumerator UpMovement()
    {
        if(_changeDimension)
            _changeDimension.WeightReference = 0;

        _myTile._walkable = false;

        while (Mathf.Abs(transform.position.y -_initialPosition.y) != 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, _initialPosition, movementSpeed * Time.deltaTime);

            yield return null;
        }

        _myTile._walkable = true;
    }

    #region<Undo>

    private void UpdatePositionList()
    {
        tilePosition.Add(transform.localPosition);
    }

    private void Undo()
    {
        if (tilePosition.Count > 0)
        {
            transform.localPosition = tilePosition[tilePosition.Count - 1];
            tilePosition.RemoveAt(tilePosition.Count - 1);

            StopAllCoroutines();

            isUndoMode = true;
            StartCoroutine(RayCastLauncher());

            if (transform.position != _destination)
                Invoke("ResetUndoMode", 0.2f);
        }     
    }

    private void ResetUndoMode()
    {
        if (isUndoMode)
            isUndoMode = false;
    }

    #endregion

    private void OnDisable()
    {
        EventManager.saveTilePosition -= UpdatePositionList;
        EventManager.undo -= Undo;
    }
}
