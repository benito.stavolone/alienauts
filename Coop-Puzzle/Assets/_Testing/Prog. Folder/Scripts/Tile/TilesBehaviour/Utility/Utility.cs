﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

public static class Utility
{
    public static Vector3 Round(Vector3 vector3, int decimalPlaces)
    {
        float multiplier = 1;
        for (int i = 0; i < decimalPlaces; i++)
        {
            multiplier *= 10f;
        }

        return new Vector3
            (
                Mathf.Round(vector3.x * multiplier) / multiplier,
                Mathf.Round(vector3.y * multiplier) / multiplier,
                Mathf.Round(vector3.z * multiplier) / multiplier
            );
    }

    // public static IEnumerator FadeGameobject(CanvasGroup canvasGroup, float start, float end, float fadeDuration)
    // {
    //     var counter = 0f;
    //     while (counter < fadeDuration)
    //     {
    //         Debug.Log("Start");
    //         counter += Time.deltaTime;
    //         canvasGroup.alpha = Mathf.Lerp(start, end, counter / fadeDuration);
    //         yield return null;
    //     }
    //     
    // }
    public static async UniTask DoFade(CanvasGroup canvasGroup, float start, float end, float fadeDuration)
    {
        var counter = 0f;
        while (counter < fadeDuration)
        {
            counter += Time.deltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, counter / fadeDuration);
            await UniTask.Yield();
        }
    }

    public static IEnumerator TypingtextCurutine(string MyText, TextMeshProUGUI CurrentText, float Delay)
    {
        for (int i = 0; i < MyText.Length +1; i++)
        {
            CurrentText.text = MyText.Substring(0, i);
            yield return new WaitForSeconds(Delay);
        }
    }

}
