﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTransform : MonoBehaviour
{
    private Vector3 blockPosition;
    private Quaternion blockRotation;

    private bool _startAnimation = false;
    private bool _positionAnimation = true;
    private Vector3 _rotationDestination;
    private bool _block = true;

    private float movementSpeed = 2;

    private void Start()
    {
        blockPosition = transform.position;
        blockRotation = transform.localRotation;
    }

    public void BlockPosition()
    {
        if (_block)
        {
            transform.position = blockPosition;
            transform.localRotation = blockRotation;
        }

    }

    public void StartAnimation(bool position, Vector3 destination)
    {
        _block = false;
        _startAnimation = true;
        _positionAnimation = position;

        _rotationDestination = destination;
    }

    private void Update()
    {
        if (_startAnimation)
        {
            if (_positionAnimation)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, movementSpeed * Time.deltaTime);

                if (Vector3.Distance(transform.localPosition, Vector3.zero) <= 0.05f)
                    ResetValue();
            }
            else
            {

                if (transform.localRotation != Quaternion.Euler(_rotationDestination))
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(_rotationDestination), movementSpeed * Time.deltaTime);
                else
                    ResetValue();
            }
        }
    }

    private void ResetValue()
    {
        _startAnimation = false;
        _block = true;

        blockPosition = transform.position;
        blockRotation = transform.localRotation;
    }
}
