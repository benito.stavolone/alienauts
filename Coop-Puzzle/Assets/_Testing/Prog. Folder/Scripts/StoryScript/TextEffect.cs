﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextEffect : MonoBehaviour
{
    [SerializeField] private string text;
    [SerializeField] private TextMeshProUGUI textComponent;
    [SerializeField] private float delay;

    void OnEnable()
    {
        StartCoroutine(Utility.TypingtextCurutine(text, textComponent, delay));
    }
}
