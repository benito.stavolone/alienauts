﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPanController : MonoBehaviour
{
    [SerializeField] private TwoPanPlatform panPlatform1;
    [SerializeField] private TwoPanPlatform panPlatform2;

    private int _weightDifference=0;
    private int _lastWeightDifference;

    private bool _isAnimation = false;

    public bool plus = false;
    public bool initial = false;

    public bool IsAnimation
    {
        get { return _isAnimation; }
        set 
        {
            _isAnimation = value; 

            if(value)
            {
                BlockCharacters(true);
                EventManager.EnableUndo(false);
                panPlatform1.MyTileController._walkable = false;
                panPlatform2.MyTileController._walkable = false;

                if(panPlatform1.WeightOnPlatform == 0 && panPlatform2.WeightOnPlatform == 0)
                    BlockCharacters(false);
            }
            else
            {
                BlockCharacters(false);
                EventManager.EnableUndo(true);
                panPlatform1.MyTileController._walkable = true;
                panPlatform2.MyTileController._walkable = true;
            }
        }
    }

    private void Start()
    {
        EventManager.playerOnScale += CheckPanWeight;

        SetReferenceToPans();

        StartCoroutine(CheckIsInPath());
    }

    private void CheckPanWeight(TwoPanController twoPanController)
    {
        if (twoPanController != this)
            return;

        switch (panPlatform1.WeightOnPlatform)
        {
            case 0:

                switch (panPlatform2.WeightOnPlatform)
                {
                    case 0:
                        /* Pan1 = 0 && Pan2 = 0 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition));

                        break;

                    case 1:
                        /* Pan1 = 0 && Pan2 = 1 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0,1,0)));

                        break;

                    case 2:
                        /* Pan1 = 0 && Pan2 = 2 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -2, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, 2, 0)));

                        break;

                    case 3:
                        /* Pan1 = 0 && Pan2 = 3 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -3, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, 3, 0)));

                        break;
                }

                break;

            case 1:

                switch (panPlatform2.WeightOnPlatform)
                {
                    case 0:
                        /* Pan1 = 1 && Pan2 = 0 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -1, 0)));

                        break;

                    case 2:
                        /* Pan1 = 1 && Pan2 = 2 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, 1, 0)));

                        break;

                    case 3:
                        /* Pan1 = 1 && Pan2 = 3 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -2, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, 2, 0)));

                        break;
                }

                break;

            case 2:

                switch (panPlatform2.WeightOnPlatform)
                {
                    case 0:
                        /* Pan1 = 2 && Pan2 = 0 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 2, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -2, 0)));

                        break;

                    case 1:
                        /* Pan1 = 2 && Pan2 = 1 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -1, 0)));

                        break;

                    case 3:
                        /* Pan1 = 2 && Pan2 = 3 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, -1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, 1, 0)));

                        break;
                }

                break;

            case 3:

                switch (panPlatform2.WeightOnPlatform)
                {
                    case 0:
                        /* Pan1 = 3 && Pan2 = 0 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 3, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -3, 0)));

                        break;

                    case 1:
                        /* Pan1 = 3 && Pan2 = 1 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 2, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -2, 0)));

                        break;

                    case 2:
                        /* Pan1 = 3 && Pan2 = 2 */

                        StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition - new Vector3(0, 1, 0)));
                        StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition - new Vector3(0, -1, 0)));

                        break;
                }

                break;
        }

        ///* If Pan2 has more weight than Pan1 */
        //if (panPlatform1.WeightOnPlatform < panPlatform2.WeightOnPlatform)
        //{
        //    _weightDifference = panPlatform2.WeightOnPlatform - panPlatform1.WeightOnPlatform;

        //    /*If there is a Character on Other Pan*/
        //    if (panPlatform1.WeightOnPlatform != 0)
        //    {
        //        if (!plus)
        //        {
        //            StartCoroutine(panPlatform2.Movement(panPlatform2.transform.localPosition + new Vector3(0, panPlatform2.WeightOnPlatform - panPlatform1.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform1.Movement(panPlatform1.transform.localPosition - new Vector3(0, panPlatform2.WeightOnPlatform - panPlatform1.WeightOnPlatform, 0)));
        //        }
        //        else
        //        {
        //            StartCoroutine(panPlatform2.Movement(panPlatform2.transform.localPosition - new Vector3(0, panPlatform2.WeightOnPlatform - panPlatform1.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform1.Movement(panPlatform1.transform.localPosition + new Vector3(0, panPlatform2.WeightOnPlatform - panPlatform1.WeightOnPlatform, 0)));
        //        }

        //    }
        //    else
        //    {
        //        if(_lastWeightDifference == 0)
        //        {
        //            StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.transform.localPosition.y - panPlatform2.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.transform.localPosition.y + panPlatform2.WeightOnPlatform, 0)));
        //        }
        //        else
        //        {
        //            if (initial)
        //            {
        //                StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.InitialPosition.y - _lastWeightDifference, 0)));
        //                StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.InitialPosition.y + _lastWeightDifference, 0)));
        //            }
        //            else
        //            {
        //                StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.transform.localPosition.y - _lastWeightDifference, 0)));
        //                StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.transform.localPosition.y + _lastWeightDifference, 0)));
        //            }
        //        }

        //    }

        //    _lastWeightDifference = _weightDifference;
        //}

        ///* If Pan1 has more weight than Pan2 */
        //if (panPlatform2.WeightOnPlatform < panPlatform1.WeightOnPlatform)
        //{
        //    _weightDifference = panPlatform1.WeightOnPlatform - panPlatform2.WeightOnPlatform;

        //    /*If there is a Character on Other Pan*/
        //    if (panPlatform2.WeightOnPlatform != 0)
        //    {
        //        if (!plus)
        //        {
        //            StartCoroutine(panPlatform1.Movement(panPlatform1.transform.localPosition + new Vector3(0, panPlatform1.WeightOnPlatform - panPlatform2.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform2.Movement(panPlatform2.transform.localPosition - new Vector3(0, panPlatform1.WeightOnPlatform - panPlatform2.WeightOnPlatform, 0)));
        //        }
        //        else
        //        {
        //            StartCoroutine(panPlatform1.Movement(panPlatform1.transform.localPosition - new Vector3(0, panPlatform1.WeightOnPlatform - panPlatform2.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform2.Movement(panPlatform2.transform.localPosition + new Vector3(0, panPlatform1.WeightOnPlatform - panPlatform2.WeightOnPlatform, 0)));
        //        }
        //    }
        //    else
        //    {
        //        if(_lastWeightDifference == 0)
        //        {
        //            StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.transform.localPosition.y - panPlatform1.WeightOnPlatform, 0)));
        //            StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.transform.localPosition.y + panPlatform1.WeightOnPlatform, 0)));
        //        }
        //        else
        //        {
        //            if (initial)
        //            {
        //                StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.InitialPosition.y - _lastWeightDifference, 0)));
        //                StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.InitialPosition.y + _lastWeightDifference, 0)));
        //            }
        //            else
        //            {
        //                StartCoroutine(panPlatform1.Movement(new Vector3(0, panPlatform1.transform.localPosition.y - _lastWeightDifference, 0)));
        //                StartCoroutine(panPlatform2.Movement(new Vector3(0, panPlatform2.transform.localPosition.y + _lastWeightDifference, 0)));
        //            }
        //        }
        //    }

        //    _lastWeightDifference = _weightDifference;
        //}

        ///* If the pans have the same Weight */
        //if (panPlatform1.WeightOnPlatform == panPlatform2.WeightOnPlatform)
        //{
        //    _weightDifference = 0;
        //    _lastWeightDifference = 0;
        //    StartCoroutine(panPlatform1.Movement(panPlatform1.InitialPosition));
        //    StartCoroutine(panPlatform2.Movement(panPlatform2.InitialPosition));
        //}
    }

    private IEnumerator CheckIsInPath()
    {
        while (true)
        {
            try
            {
                /* If pan1 isInPath we cannot select the other pan */
                if (panPlatform1.MyTileController.isInPath)
                    panPlatform2.MyTileController._walkable = false;
                else
                    panPlatform2.MyTileController._walkable = true;
            }
            catch(Exception e)
            {

            }

            try
            {
                /* We do the same thing with Pan2 */
                if (panPlatform2.MyTileController.isInPath)
                    panPlatform1.MyTileController._walkable = false;
                else
                    panPlatform1.MyTileController._walkable = true;
            }
            catch (Exception e)
            {

            }

            yield return null;
        }
    }

    private void SetReferenceToPans()
    {
        panPlatform1.GetTwoPanController(this);
        panPlatform2.GetTwoPanController(this);
    }

    private void BlockCharacters(bool value)
    {
        if (panPlatform1.PlayerController)
        {
            panPlatform1.PlayerController.IsTimeToBlock = value;
        }

        if (panPlatform2.PlayerController)
        {
            panPlatform2.PlayerController.IsTimeToBlock = value;
        }

    }

    public void SavePosition()
    {
    }

    private void OnDisable()
    {
        EventManager.playerOnScale -= CheckPanWeight;
    }
}
