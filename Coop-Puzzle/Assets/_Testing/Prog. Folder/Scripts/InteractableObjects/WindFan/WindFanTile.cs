﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindFanTile : CheckPlayer
{

    [SerializeField] private int ID;
    [SerializeField] private WindFanController myWindFan;

    public bool notCheck = false;

    protected override void PerformAction()
    {
        base.PerformAction();

        myWindFan.ChangeWindID(_playerController, ID);

        myWindFan.CheckWindDestination();

        if (_playerController.Weight == 1 && ID != 4)
        {
            _playerController.CanSave = false;
            myWindFan.WindPush(_playerController);
        }

    }

    protected override void DesformAction()
    {
        base.DesformAction();

        if (_playerController)
        {
            if (_playerController.Weight == 3 || _playerController.Weight == 2)
            {
                myWindFan.ChangeWindID(_playerController, 0);

                myWindFan.CheckWindDestination();

                myWindFan.WindPush(_playerController);

                _playerController = null;
            }
            else
            {
                _playerController.CanSave = true;
                
                myWindFan.CheckDistance();

                _playerController = null;
            }
        }
    }
}
