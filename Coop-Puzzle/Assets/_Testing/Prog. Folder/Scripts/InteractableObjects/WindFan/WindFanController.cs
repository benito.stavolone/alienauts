﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindFanController : MonoBehaviour
{
    [SerializeField] private float rayDistance;

    [SerializeField] private WindFanController windFanChain;

    [SerializeField] private TileController[] myTiles;
    private List<WindFanTile> myWindFanTile = new List<WindFanTile>();

    [SerializeField] private TileController newTarget;

    [SerializeField] private LayerMask _player;

    [Header("Particle System")]

    [SerializeField] private Transform rays;
    [SerializeField] private ParticleSystem smoke;

    private PlayerController chWeight1;
    private PlayerController cHWeight3;

    private int _weight1ID, _weight2ID, _weight3ID;

    public int Weight3ID
    {
        get { return _weight3ID; }
    }


    private Coroutine _pushPlayerCoroutine;
    RaycastHit[] hits;

    RaycastHit[] checkHits;
    private int chCounter;

    private bool canPush = false;

    private void Start()
    {
        StartCoroutine(RayCastLauncher());

        WindFanTileFill();
    }

    private IEnumerator RayCastLauncher()
    {
        while (true)
        {
            hits = Physics.RaycastAll(transform.position, transform.forward, rayDistance, _player);

            yield return null;
        }
    }

    public void CheckWindDestination()
    {
        if (_weight3ID == 0)
        {
            if (_weight2ID == 0)
                newTarget = myTiles[myTiles.Length - 1];
            else
            {
                if (_weight2ID > _weight1ID)
                    CheckDestination();
                else
                {
                    newTarget = myTiles[myTiles.Length - 1];
                }

            }
        }
        else
        {
            if(_weight3ID > _weight1ID)
            {
                CheckDestination();
            }
            else
            {
                newTarget = myTiles[myTiles.Length - 1];
            }
        }
    }

    private void CheckDestination()
    {
        for (int i = 0; i < myTiles.Length; i++)
        {
            if (myTiles[i].GetComponentInChildren<PlayerController>() == null)
            {
                newTarget = myTiles[i];
            }
            else
            {
                if (myTiles[i].GetComponentInChildren<PlayerController>().Weight != 1)
                {
                    break;
                }
                else
                {
                    continue;
                }
            }
        }
    }

    private void WindFanTileFill()
    {
        for(int i = 0; i < myTiles.Length; i++)
        {
            myWindFanTile.Add(myTiles[i].GetComponent<WindFanTile>());
        }
    }

    public void WindPush(PlayerController playerController)
    {
        if (playerController.Weight == 1)
            chWeight1 = playerController;

        if (hits.Length > 0)
        {
            if(hits[0].collider.GetComponent<PlayerController>().Weight == 3)
                return;
        }

        DisableWindTile(playerController);

        if (chWeight1 && _pushPlayerCoroutine == null && !playerController.isPushing)
        {
            if (_weight3ID != 0 && _weight3ID < _weight1ID)
                return;

            if (_weight1ID > 0 && _weight2ID == 0 && _weight3ID == 0)
                _pushPlayerCoroutine = StartCoroutine(Push(chWeight1));


            if (_weight1ID < _weight2ID || _weight1ID < _weight3ID)
                _pushPlayerCoroutine = StartCoroutine(Push(chWeight1));

            if (_weight2ID != 0 && _weight2ID < _weight1ID)
                _pushPlayerCoroutine = StartCoroutine(Push(chWeight1));

        }

    }

    public IEnumerator Push(PlayerController playerController)
    {
        DisableWindTile(false);

        playerController.BlockCharacter();
        EventManager.EnableUndo(false);
        playerController.isPushing = true;

        while (playerController && playerController.transform.position != newTarget.transform.position + new Vector3(0, 0.9f, 0))
        {
            playerController.transform.position = Vector3.MoveTowards(playerController.transform.position, newTarget.transform.position + new Vector3(0, 0.9f, 0), 2 * Time.deltaTime);

            if (playerController.isPushing == false)
                break;

            yield return null;
        }

        DisableWindTile(true);
        playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);
        playerController.isPushing = false;
        _pushPlayerCoroutine = null;
        EventManager.EnableUndo(true);

        if (newTarget == myTiles[myTiles.Length - 1] && newTarget.GetComponent<WindFanTile>() && windFanChain)
            windFanChain.WindPush(playerController);
    }

    public void ChangeWindID(PlayerController playerController, int id)
    {
        switch (playerController.Weight)
        {
            case 1:
                _weight1ID = id;
                break;
            case 2:
                _weight2ID = id;
                break;
            case 3:
                _weight3ID = id;

#pragma warning disable CS0612 // Il tipo o il membro è obsoleto
                ChangeParticleDimension(id);
#pragma warning restore CS0612 // Il tipo o il membro è obsoleto
                break;
        }
    }

    [System.Obsolete]
    private void ChangeParticleDimension(int id)
    {
        switch (id)
        {
            case 0:
                Invoke("CheckWeigth3", 1);
                break;

            case 1:
                rays.localScale = new Vector3(rays.localScale.x, rays.localScale.y, 0.15f);
                smoke.startLifetime = 0.4f;
                break;

            case 2:
                rays.localScale = new Vector3(rays.localScale.x, rays.localScale.y, 0.5f);
                smoke.startLifetime = 0.8f;
                break;

            case 3:
                rays.localScale = new Vector3(rays.localScale.x, rays.localScale.y, 1);
                smoke.startLifetime = 1.5f;
                break;

            default:
                rays.localScale = new Vector3(rays.localScale.x, rays.localScale.y, 2);
                smoke.startLifetime = 2.2f;
                break;
        }
    }

    [System.Obsolete]
    private void CheckWeigth3()
    {
        if (_weight3ID == 0)
        {
            rays.localScale = new Vector3(rays.localScale.x, rays.localScale.y, 2);
            smoke.startLifetime = 2.2f;
        }
    }

    private void DisableWindTile(PlayerController playerController)
    {
        if(_weight3ID != 0)
        {
            for (int i = _weight3ID - 1; i < myWindFanTile.Count; i++)
            {
                myWindFanTile[i].notCheck = true;
            }
        }
        else
        {
            for (int i = 0; i < myWindFanTile.Count; i++)
            {
                myWindFanTile[i].notCheck = false;
            }
        }
    }

    private void DisableWindTile(bool value)
    {
        for (int i = 0; i < myWindFanTile.Count; i++)
        {
            myWindFanTile[i].GetComponent<TileController>()._walkable = value;
        }
    }

    public void CheckDistance()
    {
        if (chWeight1 != null)
        {

            if (chWeight1.GetComponent<WindFanTile>() == null)
                _weight1ID = 0;
        }

        if(chWeight1)
            chWeight1 = null;
    }

    #region<Old>

    //private IEnumerator CheckDestination()
    //{
    //    while (true)
    //    {
    //        for (int i = 0; i < myTiles.Length; i++)
    //        {
    //            if (myTiles[i].GetComponentInChildren<PlayerController>() == null)
    //            {
    //                newTarget = myTiles[i];
    //            }
    //            else
    //            {
    //                if (myTiles[i].GetComponentInChildren<PlayerController>().Weight != 1)
    //                {
    //                    canPush = true;
    //                    break;
    //                }
    //                else
    //                {
    //                    newTarget = myTiles[i];
    //                }
    //            }
    //        }

    //        yield return null;
    //    }

    //}

    //private IEnumerator CheckMyTiles()
    //{
    //    while (true)
    //    {
    //        hits = Physics.RaycastAll(transform.position, transform.forward, rayDistance, _player);
    //        checkHits = Physics.RaycastAll(transform.position, transform.forward, startRayDistance +1, _player);

    //        if (hits.Length == 1 && checkHits.Length == 1)
    //        {
    //            canPush = true;
    //            newTarget = myTiles[myTiles.Length - 1];
    //        }

    //        for (int i = 0; i < hits.Length; i++)
    //        {
    //            if (hits[i].collider.GetComponent<PlayerController>())
    //            {
    //                PlayerController _playerSupport = hits[i].collider.GetComponent<PlayerController>();

    //                if (_playerSupport.Weight == 3)
    //                {
    //                    _playerSupport.CanSave = false;
    //                    break;
    //                }

    //                if (_playerSupport.Weight == 2)
    //                {
    //                    _playerSupport.CanSave = false;
    //                    continue;
    //                }

    //                if (_playerSupport.Weight == 1)
    //                {
    //                    _playerSupport.CanSave = false;
    //                    _playerController = _playerSupport;

    //                    //if (!canPush)
    //                    //    CheckNewDestination();

    //                    if (canPush && _pushPlayerCoroutine == null)
    //                    {
    //                        _pushPlayerCoroutine = StartCoroutine(PushPlayer());
    //                    }
    //                }
    //            }
    //        }

    //        yield return null;
    //    }

    //}

    //private void CheckNewDestination()
    //{
    //    for(int i = 0;i< myTiles.Length; i++)
    //    {
    //        if(myTiles[i].GetComponentInChildren<PlayerController>() == null)
    //        {
    //            newTarget = myTiles[i];
    //        }
    //        else
    //        {
    //            canPush = true;
    //            break;
    //        }
    //    }
    //}

    //private IEnumerator PushPlayer()
    //{
    //    _playerController.BlockCharacter();
    //    EventManager.EnableUndo(false);

    //    while (_playerController && canPush && _playerController.transform.position != newTarget.transform.position + new Vector3(0, 0.9f, 0))
    //    {
    //        _playerController.transform.position = Vector3.MoveTowards(_playerController.transform.position, newTarget.transform.position + new Vector3(0, 0.9f, 0), 2 * Time.deltaTime);

    //        yield return null;
    //    }

    //    //rayDistance = Mathf.RoundToInt(Vector3.Distance(transform.position, newTarget.transform.position) - 1);

    //    _playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);

    //    Utility.Round(_playerController.transform.position, 0);
    //    Utility.Round(_playerController.transform.localEulerAngles, 0);

    //    _playerController = null;
    //    _pushPlayerCoroutine = null;
    //    canPush = false;

    //    EventManager.EnableUndo(true);
    //}


    //private IEnumerator RayCastLauncher()
    //{
    //    while (true)
    //    {
    //        hits = Physics.RaycastAll(transform.position, transform.forward, startRayDistance, _player);

    //        Debug.DrawRay(transform.position, transform.forward * startRayDistance, Color.green);
    //        Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.red);

    //        for (int i = 0; i < hits.Length; i++)
    //        {
    //            if (hits[i].collider.GetComponent<PlayerController>())
    //            {
    //                PlayerController _playerSupport = hits[i].collider.GetComponent<PlayerController>();


    //                if (_playerSupport.Weight == 3)
    //                {
    //                    break;
    //                }

    //                if (_playerSupport.Weight == 2)
    //                {
    //                    continue;
    //                }

    //                if (_playerSupport.Weight == 1)
    //                {
    //                    _playerController = _playerSupport;

    //                    if (_pushPlayerCoroutine == null && _playerController)
    //                    {
    //                        if ((Mathf.RoundToInt(Vector3.Distance(transform.position, _playerController.transform.position) - 1)) < rayDistance)
    //                        {
    //                            _pushPlayerCoroutine = StartCoroutine(PushPlayer());
    //                            StartCoroutine(CheckBlock());
    //                        }

    //                        //if ((Mathf.RoundToInt(Vector3.Distance(transform.position, _playerController.transform.position))) > rayDistance
    //                        //    && Mathf.RoundToInt(Vector3.Distance(transform.position, _playerController.transform.position)) < startRayDistance)
    //                        //{
    //                        //    Invoke("StartCheckBlock", 1f);
    //                        //}
    //                    }

    //                }
    //            }
    //        }

    //        yield return null;
    //    }
    //}

    //private void StartCheckBlock()
    //{
    //    StartCoroutine(CheckBlock());
    //}

    //private IEnumerator PushPlayer()
    //{
    //    _playerController.BlockCharacter();
    //    EventManager.EnableUndo(false);

    //    while (_playerController && _playerController.transform.position != newTarget.transform.position + new Vector3(0, 0.9f, 0))
    //    {
    //        _playerController.transform.position = Vector3.MoveTowards(_playerController.transform.position, newTarget.transform.position + new Vector3(0, 0.9f, 0), 2 * Time.deltaTime);

    //        if (isBlocked)
    //        {
    //            StopCoroutine("CheckBlock");
    //            break;
    //        }

    //        yield return null;
    //    }

    //    rayDistance = Mathf.RoundToInt(Vector3.Distance(transform.position, _playerController.transform.position) -1);

    //    _playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);

    //    Utility.Round(_playerController.transform.position, 0);
    //    Utility.Round(_playerController.transform.localEulerAngles, 0);

    //    isBlocked = false;

    //    _playerController = null;
    //    _pushPlayerCoroutine = null;

    //    EventManager.EnableUndo(true);
    //}

    //private IEnumerator CheckBlock()
    //{
    //    while(_playerController != null)
    //    {
    //        if (Physics.Raycast(_playerController.transform.position, transform.forward, 1f, _player))
    //        {
    //            Debug.DrawRay(_playerController.transform.position, transform.forward * 1f, Color.green);

    //            isBlocked = true;

    //            _playerController.ChangePlayerState(PathfindingController.PlayerState.Idle);

    //            StopCoroutine("PushPlayer");
    //        }
    //        else
    //        {
    //            Debug.DrawRay(_playerController.transform.position, transform.forward * 1f, Color.red);

    //            if (isBlocked)
    //            {
    //                isBlocked = false;
    //                StartCoroutine(PushPlayer());
    //            }
    //        }

    //            yield return null;
    //    }
    //}

    #endregion
}
