﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class ThreePanController : MonoBehaviour
{
    [SerializeField] private bool _hasJumpController = false;

    private List<ThreePanPlatform> _pans = new List<ThreePanPlatform>();
    private List<TileController> _tilesPans = new List<TileController>();

    private List<ThreePanPlatform> _pansClone = new List<ThreePanPlatform>();
    private List<ThreePanPlatform> _pansPosition = new List<ThreePanPlatform>();

    private List<ThreePanPlatform.PanState> _lastState = new List<ThreePanPlatform.PanState>();

    private ThreePanPlatform _weighterPlatform;

    private Vector3 destinationDown, destinationLeft, destinationRight;

    private bool _animation = false;
    private ThreePanPlatform.PanState _panWithJumpController;

    private bool update = true;

    public bool Animation
    {
        get { return _animation; }
        set { _animation = value; }
    }

    private void Start()
    {
        /* Find and Get the Platforms of the ThreePans Scale */
        GetPansReference();

        /* Initializing the Clone List */
        InitializeList();

        CheckPanStateToUndo();

        EventManager.undo += Undo;
        EventManager.saveTilePosition += SaveMyState;
    }

    private void GetPansReference()
    {
        /* Loop all childs and we add them into our Pans list */
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<ThreePanPlatform>())
                _pans.Add(transform.GetChild(i).GetComponent<ThreePanPlatform>());
        }

        /* Then we loop the Pans list and we add all element in our List of TileController */
        for (int i = 0; i < _pans.Count; i++)
            _tilesPans.Add(_pans[i].GetComponent<TileController>());

    }

    private void InitializeList()
    {
        /* Cloning */
        _pansClone = _pans;
        _pansPosition = _pansClone;
    }

    /* This method is called in PerformAction and DesformAction of Three Pans Platform */
    public bool CheckPansWeight()
    {
        /* We order our list in order to find what pans has the weighter character */
        _pans = _pans.OrderBy(e => e.GetComponent<ThreePanPlatform>().WeightOnPlatform).ToList();
        _weighterPlatform = _pans[_pans.Count - 1];

        /* If the weighter platform is not down yet */
        if (_weighterPlatform._panState != ThreePanPlatform.PanState.Down)
        {
            /* We Start animation */
            EventManager.EnableUndo(false);

            _animation = true;
            update = true;
            Invoke("ThreePanAnimation", 0.25f);
            return true;
        }
        else
            return false;
    }

    private void ThreePanAnimation()
    {
        /* Check if the movement is called by PerformAction or Desform Action */
        if (_weighterPlatform.transform.localPosition.x > 0)
            CheckMovement(false);
        else
            CheckMovement(true);
    }

    private void CheckMovement(bool reverse)
    {
        /* Find Destination of single Platform */
        FindMyDestination(reverse);

        /* After find the destination of our pan we start the Movement and then we change the PanState */
        if (!reverse)
        {
            for (int i = 0; i < _pansClone.Count; i++)
            {
                if (_pansClone[i]._panState == ThreePanPlatform.PanState.SideLeft)
                {
                    StartCoroutine(_pansClone[i].Movement(destinationLeft));
                    _pansClone[i].ChangePanState(ThreePanPlatform.PanState.Down);
                }
                else
                {
                    if (_pansClone[i]._panState == ThreePanPlatform.PanState.Down)
                    {
                        StartCoroutine(_pansClone[i].Movement(destinationDown));
                        _pansClone[i].ChangePanState(ThreePanPlatform.PanState.SideRight);
                    }
                    else
                    {
                        if (_pansClone[i]._panState == ThreePanPlatform.PanState.SideRight)
                        {
                            StartCoroutine(_pansClone[i].Movement(destinationRight));
                            _pansClone[i].ChangePanState(ThreePanPlatform.PanState.SideLeft);
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < _pansClone.Count; i++)
            {
                if (_pansClone[i]._panState == ThreePanPlatform.PanState.SideLeft)
                {
                    StartCoroutine(_pansClone[i].Movement(destinationLeft));
                    _pansClone[i].ChangePanState(ThreePanPlatform.PanState.SideRight);
                }
                else
                {
                    if (_pansClone[i]._panState == ThreePanPlatform.PanState.Down)
                    {
                        StartCoroutine(_pansClone[i].Movement(destinationDown));
                        _pansClone[i].ChangePanState(ThreePanPlatform.PanState.SideLeft);
                    }
                    else
                    {
                        if (_pansClone[i]._panState == ThreePanPlatform.PanState.SideRight)
                        {
                            StartCoroutine(_pansClone[i].Movement(destinationRight));
                            _pansClone[i].ChangePanState(ThreePanPlatform.PanState.Down);
                        }
                    }
                }
            }
        }
    }

    private void FindMyDestination(bool reverse)
    {
        /* Looping our list of PositionPlatform */
        for(int i = 0; i < _pansPosition.Count; i++)
        {
            if (!reverse)
            {
                /* We assign destination depending of our state
                 * => if down go to left
                 * => if left go to right
                 * => if right go to down
                 */
                switch (_pansPosition[i]._panState)
                {
                    case ThreePanPlatform.PanState.Down:
                        destinationLeft = _pansPosition[i].transform.localPosition;
                        break;

                    case ThreePanPlatform.PanState.SideLeft:
                        destinationRight = _pansPosition[i].transform.localPosition;
                        break;

                    case ThreePanPlatform.PanState.SideRight:
                        destinationDown = _pansPosition[i].transform.localPosition;
                        break;
                }
            }
            else
            {
                /* We assign destination depending of our state
                 * => if down go to right
                 * => if left go to down
                 * => if right go to left
                 */
                switch (_pansPosition[i]._panState)
                {
                    case ThreePanPlatform.PanState.Down:
                        destinationRight = _pansPosition[i].transform.localPosition;
                        break;

                    case ThreePanPlatform.PanState.SideLeft:
                        destinationDown = _pansPosition[i].transform.localPosition;
                        break;

                    case ThreePanPlatform.PanState.SideRight:
                        destinationLeft = _pansPosition[i].transform.localPosition;
                        break;
                }
            }
        }
    }

    private void Update()
    {
        if (update)
        {
            /* During Animation the player cannot Select pans and the character cannot move */
            if (_animation)
            {
                PansSelectable(false);
                BlockAllCharacters(true);
            }
            else
            {
                update = false;
                Invoke("DisableElements", 0.5f);
            }
        }   
    }

    private void DisableElements()
    {
        PansSelectable(true);
        BlockAllCharacters(false);
    }

    public void PansSelectable(bool value)
    {
        /* The pans cannot selectable */
        for(int i = 0; i < _pans.Count; i++)
        {
            _tilesPans[i].Selectable = value;
        }
    }

    public void BlockAllCharacters(bool value)
    {
        /* Lock or Unlock all characters that are on Pans */
        for(int i = 0;i< _pans.Count; i++)
        {
            if (_pans[i].PlayerController)
            {
                _pans[i].PlayerController.IsTimeToBlock = value;
            }
        }
    }

    #region<Undo>

    private void SaveMyState()
    {
        if (_hasJumpController)
        {
            CheckPanStateToUndo();

            _lastState.Add(_panWithJumpController);
        }
    }

    private void CheckPanStateToUndo()
    {
        for(int i = 0; i < _pans.Count; i++)
        {
            if (_pans[i].GetComponent<JumpControllerPlatform>())
                _panWithJumpController = _pans[i]._panState;
        }
    }

    private void Undo()
    {
        if (_hasJumpController)
        {
            DestroyActualJumpController(); /* => DestroyActualJumpScript */
            InstantiateNewJumpController(); /* => Add Component JumpScript */

            for(int i = 0; i < _pans.Count; i++)
            {
                _pans[i].ActiveUndoMode();
            }

            _lastState.RemoveAt(_lastState.Count - 1);
        }
    }

    private void DestroyActualJumpController()
    {
        for(int i = 0; i < _pans.Count; i++)
        {
            if (_pans[i].GetComponent<JumpControllerPlatform>())
            {
                _pans[i].MyJumpFeedback.SetActive(false);

                Destroy(_pans[i].gameObject.GetComponent<JumpControllerPlatform>());
            }
        }
    }

    private void InstantiateNewJumpController()
    {
        for (int i = 0; i < _pans.Count; i++)
        {
            if (_pans[i]._panState == _lastState[_lastState.Count-1])
            {
                _pans[i].gameObject.AddComponent<JumpControllerPlatform>();
                _pans[i].MyJumpFeedback.SetActive(true);
            }
        }
    }

    public void BlockPlatform(bool value)
    {
        for(int i = 0; i < _tilesPans.Count; i++)
        {
            _tilesPans[i]._walkable = value;
        }
    }

    #endregion

    private void OnDisable()
    {
        EventManager.undo -= Undo;
        EventManager.saveTilePosition -= SaveMyState;
    }
}
