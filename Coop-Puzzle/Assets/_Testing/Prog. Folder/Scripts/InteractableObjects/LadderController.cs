﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    [SerializeField] private TileController destination;

    public TileController GetTileController()
    {
        return destination;
    }
}
