﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public static Action playerArrived;
    public static Action playerLeft;

    public static Action levelCompleted;
    public static Action levelStarted;

    public static Action<TwoPanController> playerOnScale;

    public static Action movesIncrement;

    public static Action undo;
    public static Action<Vector3, TileController> savePosition;
    public static Action<Quaternion> saveRotation;
    public static Action<GameObject> savePlayer;
    public static Action<bool> enableUndo;

    public static Action saveTilePosition;

    public static Action<int> changeLevel;

    public static Action<PlayerController> selectedPlayer;

    public static Action restartLevel;
    public static Action<bool> pause;

    public static Action EnterTutorial;
    public static Action ExitTutorial;

    public static Action<AudioClip> vfxClip;
    public static Action<AudioClip> backgroundClip;
    public static Action<AudioClip> ambientClip;
    public static Action fadeIn;
    public static Action fadeOut;

    public static Action endLevel;

    public static Action checkWindDestination;
    public static Action<PlayerController> windPush;
    public static Action<PlayerController,int> changeID;

    #region<Arrive Platform Events>

    /* A Character is Arrived on own ArrivePlatform */
    public static void OnPlayerArrived()
    {
        playerArrived?.Invoke();
    }

    /* A Character Left own ArrivePlatform */
    public static void OnPlayerLeft()
    {
        playerLeft?.Invoke();
    }

    #endregion

    #region<Levels Events>

    /* All Characters are Arrived on Own ArrivedPlatform */
    public static void OnLevelCompleted()
    {
        levelCompleted?.Invoke();
    }

    public static void OnLevelStarted()
    {
        levelStarted?.Invoke();
    }

    #endregion

    #region<TwoPan Scale Event>

    /* A Character is on Two Pan Scale Platform */
    public static void OnPlayerScales(TwoPanController twoPanController)
    {
        playerOnScale?.Invoke(twoPanController);
    }

    #endregion

    #region<Moves Counter Events>

    public static void MovesIncrement()
    {
        movesIncrement?.Invoke();
    }

    #endregion

    #region<Undo Event>

    public static void SavePosition(Vector3 position, TileController tile)
    {
        savePosition?.Invoke(position, tile);
    }

    public static void SaveRotation(Quaternion rotation)
    {
        saveRotation?.Invoke(rotation);
    }

    public static void SavePlayer(GameObject player)
    {
        savePlayer?.Invoke(player);
    }

    public static void Undo()
    {
        undo?.Invoke();
    }

    public static void SaveTilePosition()
    {
        saveTilePosition?.Invoke();
    }

    public static void EnableUndo(bool value)
    {
        enableUndo?.Invoke(value);
    }

    #endregion

    #region<ChangeLevel>

    public static void ChangeLevel(int index)
    {
        changeLevel?.Invoke(index);
    }

    #endregion

    #region<Selection Events>

    public static void SelectedPlayer(PlayerController player)
    {
        selectedPlayer?.Invoke(player);
    }

    #endregion

    #region<PauseEvents>

    public static void Pause(bool enablePause)
    {
        pause?.Invoke(enablePause);
    }

    public static void RestartLevel()
    {
        restartLevel?.Invoke();
    }

    #endregion

    #region Tutorial
    
    public static void EnterTileTutorial()
    {
        EnterTutorial?.Invoke();
    }

    public static void ExitTileTutorial()
    {
        ExitTutorial?.Invoke();
    }

    #endregion

    #region<Audio>

    public static void ChangeVFXClip(AudioClip newClip)
    {
        vfxClip?.Invoke(newClip);
    }

    public static void ChangeBackGroundClip(AudioClip newClip)
    {
        backgroundClip?.Invoke(newClip);
    }


    public static void ChangeAmbientClip(AudioClip newClip)
    {
        ambientClip?.Invoke(newClip);
    }

    public static void FadeOut()
    {
        fadeOut?.Invoke();
    }

    public static void FadeIn()
    {
        fadeIn?.Invoke();
    }

    #endregion

    #region<End>

    public static void Endlevel()
    {
        endLevel?.Invoke();
    }

    #endregion

    public static void CheckWindDestination()
    {
        checkWindDestination?.Invoke();
    }

    public static void WindPush(PlayerController player)
    {
        windPush?.Invoke(player);
    }

    public static void ChangeWindID(PlayerController player, int ID)
    {
        changeID?.Invoke(player, ID);
    }
}
