﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SelectionController : MonoBehaviour
{
    [SerializeField] private PlayerController playerReference;

    [SerializeField] private Texture standardCursorImage;
    [SerializeField] private Texture yellowCursorImage;
    [SerializeField] private Texture redCursorImage;
    [SerializeField] private Texture blueCursorImage;
    GamePadCursor gamePadCursor;

    private GameManager _gameManager;

    private void Start()
    {
        gamePadCursor = FindObjectOfType<GamePadCursor>();
        _gameManager = GetComponent<GameManager>();

        Cursor.SetCursor((Texture2D)standardCursorImage, Vector2.zero, CursorMode.Auto);
        gamePadCursor.cursorImage.color = Color.red;

        playerReference = _gameManager.Characters[1];

        ChangeCursor(playerReference);
    }

    private void OnEnable()
    {
        EventManager.pause += ResetCursorColor;
    }

    private void Update()
    {
        CheckMouseEventAction();

        if (Gamepad.current.buttonWest.IsPressed())
            SelectionCheck(_gameManager.Characters[2]);


        if (Gamepad.current.buttonNorth.IsPressed())
            SelectionCheck(_gameManager.Characters[0]);


        if (Gamepad.current.buttonEast.IsPressed())
            SelectionCheck(_gameManager.Characters[1]);

    }

    private void CheckMouseEventAction()
    {
        if (GameManager.currentState == GameManager.GameState.GamePlay)
        {
            if (Gamepad.current.aButton.IsPressed() || Mouse.current.leftButton.IsPressed())
            {
                RaycastHit hit;
                Ray ray;
                if(gamePadCursor.playerInput.currentControlScheme == gamePadCursor.gamepadScheme)
                    ray = Camera.main.ScreenPointToRay(gamePadCursor.cursorTransform.transform.position);
                else
                    ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.collider.gameObject.GetComponent<PlayerController>())
                    {
                        PlayerController newSelected = hit.collider.gameObject.GetComponent<PlayerController>();
                        SelectionCheck(newSelected);
                    }
                }
            }
        }
    }

    private void SelectionCheck(PlayerController playerSelected)
    {
        /* If doesn't Exist a Selected Player */
        if (playerReference == null)
        {
            PlayerSelection(playerSelected);
        }
        else /* If Exist a Selected Player but the new one is different */
        {
            if (playerReference != playerSelected)
            {
                if (playerSelected.MyPersonalTile)
                {
                    if (playerSelected.MyPersonalTile.isInPath == false)
                    {
                        playerReference.ChangeSelectionState(PathfindingController.SelectionState.NotSelectable);
                        PlayerSelection(playerSelected);
                    }
                }
                else
                {
                    playerReference.ChangeSelectionState(PathfindingController.SelectionState.NotSelectable);
                    PlayerSelection(playerSelected);
                }

            }
        }
    }
       

    private void PlayerSelection(PlayerController player)
    {
        ChangeCursor(player);
        playerReference = player;
        playerReference.ChangeSelectionState(PathfindingController.SelectionState.Selectable);
        EventManager.SelectedPlayer(playerReference);
    }

    private void ChangeCursor(PlayerController player)
    {
        switch (player.Weight)
        {
            case 1:
                Cursor.SetCursor((Texture2D)yellowCursorImage, Vector2.zero, CursorMode.Auto);
                gamePadCursor.cursorImage.color = Color.yellow;
                break;
            case 2:
                Cursor.SetCursor((Texture2D)redCursorImage, Vector2.zero, CursorMode.Auto);
                gamePadCursor.cursorImage.color = Color.red;
                break;
            case 3:
                Cursor.SetCursor((Texture2D)blueCursorImage, Vector2.zero, CursorMode.Auto);
                gamePadCursor.cursorImage.color = Color.blue;
                break;
        }
    }

    private void ResetCursorColor(bool value)
    {
        if(value)
        {
            Cursor.SetCursor((Texture2D)standardCursorImage, Vector2.zero, CursorMode.Auto);
            gamePadCursor.cursorImage.color = Color.white;
        }
        else
            ChangeCursor(playerReference);
    }

    private void OnDisable()
    {
        Cursor.SetCursor((Texture2D)standardCursorImage, Vector2.zero, CursorMode.Auto);
        gamePadCursor.cursorImage.color = Color.white;
    }


}
