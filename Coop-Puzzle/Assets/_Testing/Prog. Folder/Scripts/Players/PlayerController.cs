﻿using System.Collections;
using UnityEngine;
using Cysharp.Threading.Tasks;
using UnityEngine.InputSystem;

public class PlayerController : PathfindingController
{
    private TileController _myPersonalTile;

    GamePadCursor gamePadCursor;

    [SerializeField] private bool _selectionStart;

    [SerializeField] private Material myMaterial;
    private SkinnedMeshRenderer _myMeshRenderer;

    public TileController MyPersonalTile
    {
        get { return _myPersonalTile; }
        set { _myPersonalTile = value; }
    }

    private bool _isTimeToBlock = false;
    protected bool _canClick = true;
    private bool _canSave = true;

    private float _shaderValue = 1;
    private bool _canIncrease = false, _shaderMaterial = false;

    public bool isPushing = false;

    public bool CanSave
    {
        get { return _canSave; }
        set { _canSave = value; }
    }

    public bool IsTimeToBlock
    {
        get { return _isTimeToBlock; }
        set 
        {
            _isTimeToBlock = value;

            if (value)
                RemoveSelectableTiles().Forget();
        }
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        gamePadCursor = FindObjectOfType<GamePadCursor>();

        GettingComponents();

        Initialize();
        ChangePlayerState(PlayerState.Idle);

        //SaveMyMove();

        GetCurrentTile();

        if (_selectionStart)
        {
            ChangeSelectionState(SelectionState.Selectable);
            EventManager.SelectedPlayer(this);
        }
        else
            ChangeSelectionState(SelectionState.NotSelectable);


        EventManager.levelCompleted += ResetStates;
        EventManager.undo += DisableParticle;
        EventManager.undo += Undo;
        EventManager.changeLevel += ChangeLevel;
    }

    public override void ChangePlayerState(PlayerState newState)
    {
        _playerState = newState;

        switch (_playerState)
        {
            case PlayerState.Idle:

                _climbState = ClimbState.None;
                _jumpState = JumpState.None;

                if (animationController)
                {
                    if(_selectionState == SelectionState.NotSelectable)
                    {
                        animationController.SetWalkingAnimation(false);
                        animationController.SetIdleAnimation(true);
                        animationController.SetSitDownAnimation(true);

                    }

                    if (_selectionState == SelectionState.Selectable)
                    {
                        animationController.SetWalkingAnimation(false);
                        animationController.SetIdleAnimation(true);
                    }
                }

                StopAllCoroutines();

                GetCurrentTile();
                transform.SetParent(_currentTile.transform);

                if (_selectionState == SelectionState.Selectable)
                    StartCoroutine(CheckSelectableTiles());

                break;

            case PlayerState.Moving:
                    if(animationController)
                        animationController.SetWalkingAnimation(true);

                    transform.SetParent(null);
                    StartCoroutine(CharacterMovement());
                break;
        }
    }

    public void ChangeSelectionState(SelectionState newSelectionState)
    {
        _selectionState = newSelectionState;

        switch (_selectionState)
        {
            case SelectionState.Selectable:
                _canClick = false;
                Invoke("ResetCanClick", 0.3f);

                if (animationController)
                {
                    animationController.SetSitDownAnimation(false);
                    animationController.SetIdleAnimation(true);
                }

                myMaterial.SetColor("ColorRef", Color.white);
                if(GameManager.currentState == GameManager.GameState.GamePlay)
                    SetNormalMaterial();

                StartCoroutine(CheckSelectableTiles());
                EnableMyTile(false);
                break;

            case SelectionState.NotSelectable:

                if (animationController)
                {
                    animationController.SetSitDownAnimation(true);
                }

                myMaterial.SetColor("ColorRef", _notSelectSolor);
                if (GameManager.currentState == GameManager.GameState.GamePlay)
                    SetNormalMaterial();

                Invoke("ResetCanClick", 1f);
                EnableMyTile(true);
                break;
        }
    }

    private void Update()
    {
        if(_playerState == PlayerState.Idle && _selectionState == SelectionState.Selectable)
        {
            CheckMouseEventAction();
        }

        IncreaseShaderTime();
    }

    private IEnumerator CheckSelectableTiles()
    {
        while (_playerState == PlayerState.Idle && _selectionState == SelectionState.Selectable)
        {
            if(!IsTimeToBlock)
                FindSelectableTiles().Forget();
            yield return new WaitForSeconds(1);
        }
    }

    private void CheckMouseEventAction()
    {
        if ((Gamepad.current.aButton.IsActuated() || Input.GetMouseButtonUp(0)) && !_isTimeToBlock && _canClick)
        {
            if (GameManager.currentState == GameManager.GameState.GamePlay)
                MouseInputLogic();
        }

        if (!_canClick)
            Invoke("ResetCanClick", 1f);
    }

    private void MouseInputLogic()
    {
        Ray ray;

        if (gamePadCursor.playerInput.currentControlScheme == gamePadCursor.gamepadScheme)
            ray = Camera.main.ScreenPointToRay(gamePadCursor.cursorTransform.transform.position);
        else
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.GetComponent<TileController>())
            {
                TileController t = hit.collider.GetComponent<TileController>();

                if (t.GetComponent<CatapultController>())
                {
                    if (!t.GetComponent<CatapultController>().CheckCatapultDestination())
                    {
                        t.Selectable = false;

                        animationController.SetDenyAnimation(true);
                        Invoke("ResetDeny", 1);
                    }
                    else
                        t.Selectable = true;
                }

                if (t.GetComponent<TeleportButton>())
                {
                    if (!t.GetComponent<TeleportButton>().CheckDestination())
                        t.Selectable = false;
                    else
                        t.Selectable = true;
                }

                /* Conditions for Mouse Action */
                if (t.TileState == TileController.TileStates.Selectable && t._walkable && t.gameObject.GetComponent<LadderController>() == null && t.Selectable && !t.isInPath)
                {
                    if (t.GetComponentInChildren<PlayerController>() == null)
                    {
                        if(t.GetComponentInParent<PlayerController>() == null)
                        {
                            SaveMyMove();
                            TargetTileSelected(t);
                        }
                    }
                }
                else 
                {
                    animationController.SetDenyAnimation(true);
                    Invoke("ResetDeny", 1);
                }
            }
        }
    }

    public void TargetTileSelected(TileController t)
    {
        ///* Spawn PS MovementFeedback */
        //Component particle = PoolManager.instance.ReuseObject<Component>(ps_MovementFeedback, t.transform.position + new Vector3(0, t.GetComponent<Collider>().bounds.extents.y + 0.1f, 0), Quaternion.identity);
        //_myPersonalMovementFeedBack = particle.gameObject;
        if(animationController)
            animationController.SetIdleAnimation(false);

        MoveToTile(t);
        EventManager.MovesIncrement();
        _canClick = false;
    }

    private void GettingComponents()
    {
        /* Get My Tile */
        if (gameObject.transform.childCount > 0)
            _myPersonalTile = GetComponentInChildren<TileController>();

        if(modelReference.GetComponent<SkinnedMeshRenderer>())
            _myMeshRenderer = modelReference.GetComponent<SkinnedMeshRenderer>();
    }

    private void ResetStates()
    {
        FindTileController().Forget();

        SaveMyMove();
        ChangePlayerState(PlayerState.Idle);
    }

    private void ChangeLevel(int index)
    {
        Invoke("ResetStates", 1);
    }

    public void EnableMyTile(bool value)
    {
        if (_myPersonalTile)
        {
            _myPersonalTile._walkable = value;
        }        
    }

    public void BlockCharacter()
    {
        Invoke("Block", 0.035f);
    }

    private void Block()
    {
        TileReached();
        ResetPath();
    }

    protected override void ResetCanClick()
    {
        _canClick = true;
    }

    private void ResetDeny()
    {
        animationController.SetDenyAnimation(false);
    }

    #region<Undo>

    private void SaveMyMove()
    {
        if (_canSave)
        {
            EventManager.SavePosition(transform.position, _currentTile);
            EventManager.SaveRotation(transform.rotation);
            EventManager.SavePlayer(gameObject);
            EventManager.SaveTilePosition();
        }
    }

    private void Undo()
    {
        _canClick = true;

        _isTimeToBlock = false;

        ResetPath();

        if (animationController)
        {
            animationController.SetEndClimbing(false);
            animationController.SetClimbingAnimation(false);
            animationController.SetWalkingAnimation(false);
            animationController.SetJumpingAnimation(false);
        }

        if (_movementFeedback && _movementFeedback.activeInHierarchy)
            DisableParticle();
    }

    #endregion

    #region<Shader>

    public void SetDissolveMaterial(int startTime, bool increase)
    {
        sh_Dissolve.SetInt("TimeRef", startTime);
        _shaderValue = startTime;

        if(_myMeshRenderer)
            _myMeshRenderer.material = sh_Dissolve;
        else
        {
            for (int i = 0; i < modelReference.childCount; i++)
            {
                if (modelReference.GetChild(i).GetComponent<MeshRenderer>())
                {
                    modelReference.GetChild(i).GetComponent<MeshRenderer>().material = sh_Dissolve;
                }
            }
        }

        _shaderMaterial = true;
        _canIncrease = increase;
    }

    private void IncreaseShaderTime()
    {
        if (_shaderMaterial)
        {
            if (_canIncrease)
            {
                _shaderValue -= Time.deltaTime;
                SetShaderValue(_shaderValue);

                if (_shaderValue < -1)
                    _shaderMaterial = false;
            }
            else
            {
                _shaderValue += Time.deltaTime;
                SetShaderValue(_shaderValue);

                if (_shaderValue > 1)
                    _shaderMaterial = false;
            }
        }

    }

    public void SetShaderValue(float value)
    {
        if (_myMeshRenderer)
        {
            _myMeshRenderer.material.SetFloat("TimeRef", value);
        }
        else
        {
            for (int i = 0; i < modelReference.childCount; i++)
            {
                if (modelReference.GetChild(i).GetComponent<MeshRenderer>())
                {
                    modelReference.GetChild(i).GetComponent<MeshRenderer>().material.SetFloat("TimeRef", value);
                }
            }
        }
    }

    public void SetNormalMaterial()
    {
        if (_myMeshRenderer)
        {
            _myMeshRenderer.material = myMaterial;
        }
        else
        {
            for (int i = 0; i < modelReference.childCount; i++)
            {
                if (modelReference.GetChild(i).GetComponent<MeshRenderer>())
                {
                    modelReference.GetChild(i).GetComponent<MeshRenderer>().material = myMaterial;
                }
            }
        }
    }

    #endregion

    private void OnDisable()
    {
        EventManager.levelCompleted -= ResetStates;
        EventManager.undo -= DisableParticle;
        EventManager.undo += Undo;
        EventManager.changeLevel -= ChangeLevel;
    }

    private void OnDestroy()
    {
        myMaterial.SetColor("ColorRef", Color.white);
    }
}
