﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class PathfindingController : MonoBehaviour
{
    #region<Pathfinding Settings>

    public float distanceBetweenTiles = 0.1f;
    public float _distanceFromLadderforClimb = 0.7f;
    public float _distanceFromEdgeForClimb = 0.15f;
    public float _distanceFromCharacterForJump = 0.55f;
    public float _distanceFromEdgeForJump = 0.25f;

    #endregion

    [Header("Animation")]
    [SerializeField] protected AnimationController animationController;

    #region<PlayerState>

    public enum PlayerState
    {
        Idle,
        Moving,
        Climbing,
        Jumping,
    }

    public PlayerState _playerState;

    public enum SelectionState
    {
        Selectable,
        NotSelectable
    }

    public SelectionState _selectionState = SelectionState.NotSelectable;

    public enum ClimbState
    {
        None,
        ClimbDown,
        ClimbUp,
        MovingEdge
    }

    protected ClimbState _climbState = ClimbState.None;

    public enum JumpState
    {
        None,
        JumpDown,
        JumpUp,
        MovingEdge
    }

    protected JumpState _jumpState = JumpState.None;

    #endregion

    #region<PlayerSettings>

    [SerializeField] private PlayerScriptable playerSettings;
    [SerializeField] private GameObject characterModelToRotate;

    private float _climbHeight;
    private float _moveSpeed;
    private float _climbVelocity;
    private int _weight;
    private float _jumpVelocity;

    protected Color _notSelectSolor;

    public int Weight
    {
        get { return _weight; }
    }

    private Vector3 _velocity = new Vector3();
    private Vector3 _heading = new Vector3();

    private float _halfheight = 0;

    #region<Particle System>

    [Header("ParticleSystem")]

    [SerializeField] protected GameObject ps_MovementFeedback;

    protected GameObject _movementFeedback;

    #endregion

    #region<Shader>

    [Header("ShaderMaterial")]

    [SerializeField] protected Transform modelReference;
    [SerializeField] protected Material sh_Dissolve;

    public Material Dissolve
    {
        get { return sh_Dissolve; }
    }

    #endregion

    #endregion

    #region<PathfindingSettings>

    /* List that Contains the Tiles where the Selected Player can Walk */
    protected List<TileController> _selectableTiles = new List<TileController>();
    
    /* Array that contains all tiles */
    private TileController[] tiles;

    /* Represents a Collection of Variable-size last-in-first-out (LIFO) Instances of the same Specified Type. */
    private Stack<TileController> _path = new Stack<TileController>();

    protected TileController _currentTile;

    #endregion

    #region<Jump/Climb Settings>

    [SerializeField] private GameObject PS_smokeJumping;
    private Component _jumpParticle;

    private Vector3 _climbTarget;
    private bool climbing = false;
    private bool jumping = false;

    #endregion

    protected void Initialize()
    {
        GetSettingsFromScriptable();

        FindTileController().Forget();

        _halfheight = GetComponent<Collider>().bounds.extents.y;
    }

    protected async UniTask FindTileController()
    {
        await UniTask.Yield();

        tiles = GameObject.FindObjectsOfType<TileController>();
    }

    #region<Pathfinding Section>

    protected void GetCurrentTile()
    {
        _currentTile = GetTargetTile(gameObject);
        _currentTile.ChangeTileState(TileController.TileStates.Current);
    }

    private TileController GetTargetTile(GameObject target)
    {
        RaycastHit hit;
        TileController tile = null;

        if(Physics.Raycast(target.transform.position, -target.transform.up, out hit, 1f))
            tile = hit.collider.GetComponent<TileController>();

        return tile;
    }

    private void ComputeAdjacencyList()
    {
        foreach (TileController tile in tiles)
            tile.FindNeighboursTile(_climbHeight);
    }

    protected async UniTask FindSelectableTiles()
    {
        await UniTask.Yield();

        ComputeAdjacencyList();

        Queue<TileController> process = new Queue<TileController>();

        process.Enqueue(_currentTile);

        _currentTile.IsVisited = true;

        while (process.Count > 0)
        {
            TileController t = process.Dequeue();

            _selectableTiles.Add(t);
            t.ChangeTileState(TileController.TileStates.Selectable);

            foreach (TileController tile in t.AdjacencyList)
            {
                if (!tile.IsVisited)
                {
                    if (!tile.parentSelf)
                        tile.Parent = t;
                    else
                        tile.Parent = tile.parentSelf;

                    tile.IsVisited = true;
                    process.Enqueue(tile);
                }
            }
        }
    }

    protected async UniTask RemoveSelectableTiles()
    {
        await UniTask.Yield();

        for (int i = 0; i < _selectableTiles.Count; i++)
            _selectableTiles[i].ResetValue();

        _selectableTiles.Clear();
    }

    #endregion

    #region<Characters Movement

    protected void MoveToTile(TileController tile)
    {
        ResetPath();

        TileController next = tile;

        while (next != null)
        {
            /* If the Tile is in the Path of others character */
            if (next.isInPath)
            {
                ResetPath(); /* reset the path */
                DisableParticle();
                return;
            }

            next.isInPath = true;
            _path.Push(next);
            next = next.Parent;
        }

        if(next == null)
        {
            tile.ChangeTileState(TileController.TileStates.Target);
            ChangePlayerState(PlayerState.Moving);

            /* Spawn PS MovementFeedback */
            Component particle = PoolManager.instance.ReuseObject<Component>(ps_MovementFeedback, tile.transform.position + new Vector3(0, tile.GetComponent<Collider>().bounds.extents.y + 0.01f, 0), Quaternion.identity);
            _movementFeedback = particle.gameObject;
        }
    }

    protected void ResetPath()
    {
        while (_path.Count > 0)
        {
            TileController t = _path.Peek();

            t.isInPath = false;

            _path.Pop();
        }

        _path.Clear();
    }
    
    protected IEnumerator CharacterMovement()
    {
        while (_path.Count > 0)
        {
            TileController t = _path.Peek();
            Vector3 target = t.transform.position;

            /* Calculating the Character Position */
            target.y += _halfheight + t.GetComponent<Collider>().bounds.extents.y;

            if(Vector3.Distance(transform.position, target) >= distanceBetweenTiles)
            {
                bool jump = transform.position.y != target.y;

                if (jump)
                {
                    if (t.GetComponentInParent<PlayerController>())
                    {
                        if (Mathf.Abs(transform.position.y - target.y) > 0.1f)
                        {
                            //Debug.Log("here");
                            Jump(target, _distanceFromCharacterForJump, 1);
                        }

                    }
                    else
                    {
                        if (t.HasDirectionLine)
                        {
                            if (Mathf.Abs(transform.position.y - target.y) < 1f && !climbing && jumping)
                                Jump(target, 1.5f, 2);
                            else
                                Climb(target);
                        }
                        else
                        {
                            Climb(target);
                        }

                    }
                }
                else
                {
                    CalculateHeading(target);
                    SetHorizontalVelocity();
                }

                transform.forward = _heading;

                transform.position += _velocity * Time.deltaTime;
            }
            else
            {
                /* Tile Center Reached */
                transform.position = target;
                t.isInPath = false;
                _path.Pop();
            }

            yield return null;
        }

        
        TileReached();      
    }

    protected void TileReached()
    {
        if (characterModelToRotate)
            characterModelToRotate.transform.localPosition = new Vector3(0, characterModelToRotate.transform.localPosition.y, 0);

        Invoke("ResetCanClick", 0.3f);
        RemoveSelectableTiles().Forget();
        ChangePlayerState(PlayerState.Idle);
        DisableParticle();
    }

    public void DisableParticle()
    {
        if(_movementFeedback)
            _movementFeedback.SetActive(false);
    }

    protected virtual void ResetCanClick()
    {
        
    }

    private void CalculateHeading(Vector3 target)
    {
        _heading = target - transform.position;
        _heading.Normalize();
    }

    private void SetHorizontalVelocity()
    {
        _velocity = _heading * _moveSpeed;
    }

    #region<ClimbSection>

    private async void Climb(Vector3 target)
    {
        await Task.Yield();

        switch (_climbState)
        {
            case ClimbState.ClimbDown:
                ClimbDown(target);
                break;

            case ClimbState.ClimbUp:
                ClimbUp(target);
                break;

            case ClimbState.MovingEdge:
                MoveToEdgeClimbing();
                break;

            case ClimbState.None:
                PrepareToClimb(target);
                break;
        }
    }

    private void PrepareToClimb(Vector3 target)
    {
        float targetY = target.y;

        target.y = transform.position.y;

        CalculateHeading(target);

        /* Up or Down? */
        if(transform.position.y > targetY)
        {
            _climbState = ClimbState.MovingEdge;

            _climbTarget = transform.position + (target - transform.position) / 2;
        }
        else
        {
            /* Distance from Ladder before Climb */
            if (Vector3.Distance(transform.position, target) <= _distanceFromLadderforClimb)
            {
                _climbState = ClimbState.ClimbUp;

                /* No Horizontal Velocity */
                _velocity = Vector3.zero;

                float difference = targetY - transform.position.y;

                _velocity.y = _climbVelocity * (difference / 2);
            }
            else
                SetHorizontalVelocity();
        }
    }

    private void ClimbDown(Vector3 target)
    {
        if (!jumping)
        {
            climbing = true;

            if (animationController)
            {
                if (Mathf.Abs(transform.position.y - target.y) > 0.2f)
                {
                    characterModelToRotate.transform.localEulerAngles = new Vector3(0, 180, 0);
                    characterModelToRotate.transform.localPosition = new Vector3(0, characterModelToRotate.transform.localPosition.y, 0.5f);
                    animationController.SetClimbingAnimation(true);
                    animationController.SetEndClimbing(false);
                }
            }


            transform.position += Vector3.down * _climbVelocity * 2 * Time.deltaTime;
        }
        else
        {
            _velocity += Physics.gravity * Time.deltaTime;
            _velocity += transform.forward * 3 * Time.deltaTime;
        }

        if (transform.position.y <= target.y)
        {
            _climbState = ClimbState.None;

            Vector3 p = transform.position;
            p.y = target.y;
            transform.position = p;

            _velocity = new Vector3();

            if (animationController)
            {
                characterModelToRotate.transform.localEulerAngles = new Vector3(0, 0, 0);
                animationController.SetClimbingAnimation(false);
                climbing = false;
            }
        }
    }

    private void ClimbUp(Vector3 target)
    {
        if (animationController)
        {
            if (Mathf.Abs(transform.position.y - target.y) > 0.2f)
            {
                if (Mathf.Abs(transform.position.y - target.y) > 0.5f)
                {
                    animationController.SetClimbingAnimation(true);
                    climbing = true;
                }   
            }
        }

 
        if (climbing)
        {
            if (animationController)
            {
                if (Mathf.Abs(transform.position.y - target.y) < 1.3f)
                {
                    animationController.SetEndClimbing(true);
                }
                else
                    animationController.SetEndClimbing(false);
            }
        }

        transform.position += Vector3.up * _climbVelocity * Time.deltaTime;

        if (transform.position.y > target.y)
        {
            if (animationController)
            {
                animationController.SetClimbingAnimation(false);
                animationController.SetEndClimbing(false);
                climbing = false;
            }

            _climbState = ClimbState.None;
        }
    }

    private void MoveToEdgeClimbing()
    {

        if (Vector3.Distance(transform.position, _climbTarget) >= _distanceFromEdgeForClimb)
        {
            SetHorizontalVelocity();
        }
        else
        {
            _velocity += transform.forward;
            _climbState = ClimbState.ClimbDown;

            /* No Horizontal Velocity */
            _velocity = Vector3.zero;
        }
    }

    #endregion

    #region<JumpSection>

    protected bool jumpUp, jumpDown;

    private async void Jump(Vector3 target, float distance, float velocityMultiplier)
    {
        await Task.Yield();

        switch (_jumpState)
        {
            case JumpState.JumpDown:



                JumpDown(target);
                break;

            case JumpState.JumpUp:

                if (_jumpParticle == null && PS_smokeJumping)
                    _jumpParticle = PoolManager.instance.ReuseObject<Component>(PS_smokeJumping, transform.position - new Vector3(0,0.5f,0), Quaternion.identity);

                JumpUp(target);
                break;

            case JumpState.MovingEdge:
                MoveToEdgeJumping();
                break;

            case JumpState.None:
                PrepareToJump(target, distance, velocityMultiplier);
                break;
        }
    }

    private void JumpDown(Vector3 target)
    {
        jumping = true;

        if (animationController)
            animationController.SetJumpingAnimation(true);

        _velocity += Physics.gravity * Time.deltaTime;

        if (transform.position.y <= target.y)
        {
            _jumpState = JumpState.None;

            if (!jumpUp)
                Invoke("StopAnimation", 1f);
            else
                _jumpParticle = null;

            Vector3 p = transform.position;
            p.y = target.y;
            transform.position = p;

            _velocity = new Vector3();
        }
    }

    private void JumpUp(Vector3 target)
    {
        jumping = true;

        if (animationController)
            animationController.SetJumpingAnimation(true);

        _velocity += Physics.gravity * Time.deltaTime;
        _velocity += transform.forward * 3 * Time.deltaTime;

        if (transform.position.y > target.y)
        {
            if (!jumpDown)
            {
                Invoke("ResetParticle", 0.5f);

                Invoke("StopAnimation", 0.9f);
            }


            _jumpState = JumpState.None;
        }
    }

    private void ResetParticle()
    {
        PoolManager.instance.PutObjectInPool(_jumpParticle.gameObject);
        _jumpParticle = null;
    }

    private void StopAnimation()
    {
        jumping = false;

        //if(PS_smokeJumping)
        //    PoolManager.instance.PutObjectInPool(PS_smokeJumping);

        if (animationController)
        {
            animationController.SetJumpingAnimation(false);
        }
    }

    private void MoveToEdgeJumping()
    {
        if (Vector3.Distance(transform.position, _climbTarget) >= _distanceFromEdgeForJump)
        {
            SetHorizontalVelocity();
        }
        else
        {
            _jumpState = JumpState.JumpDown;

            _velocity /= 6;
            _velocity.y = 1.5f;
        }
    }

    private void PrepareToJump(Vector3 target, float distancefromCharacter, float multiplier)
    {
        float targetY = target.y;

        target.y = transform.position.y;

        CalculateHeading(target);

        /* Up or Down? */
        if (transform.position.y > targetY)
        {
            _jumpState = JumpState.MovingEdge;

            jumpDown = true;
            jumpUp = false;

            _climbTarget = transform.position + (target - transform.position) / 2;
        }
        else
        {
            /* Distance from Companions before Jump */
            if (Vector3.Distance(transform.position, target) <= distancefromCharacter)
            {
                jumpDown = false;
                jumpUp = true;

                _jumpState = JumpState.JumpUp;

                _velocity = _heading * _moveSpeed / 5f;

                float difference = targetY - transform.position.y;

                _velocity.y = _jumpVelocity * multiplier * (difference / 2);
            }
            else
                SetHorizontalVelocity();
        }
    }

    #endregion

    #endregion

    private void GetSettingsFromScriptable()
    {
        _climbHeight = playerSettings.ClimbHeight;
        _moveSpeed = playerSettings.MoveSpeed;
        _climbVelocity = playerSettings.ClimbVelocity;
        _weight = playerSettings.Weight;
        _jumpVelocity = playerSettings.JumpVelocity;
        _notSelectSolor = playerSettings.NotSelectColor;
    }

    #region <VirtualMethods>

    public virtual void ChangePlayerState(PlayerState newState)
    {

    }

    #endregion
}
