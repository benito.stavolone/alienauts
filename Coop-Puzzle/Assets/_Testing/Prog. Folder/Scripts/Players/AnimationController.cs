﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator myAnimator;

    public Animator GetAnimator()
    {
        return myAnimator;
    }

    public void SetWalkingAnimation(bool value)
    {
        myAnimator.SetBool("isWalking", value);
    } 

    public void SetClimbingAnimation(bool value)
    {
        myAnimator.SetBool("isClimbing", value);
    }

    public void SetEndClimbing(bool value)
    {
        myAnimator.SetBool("isEndClimb", value);
    }

    public void SetIsUndo(bool value)
    {
        myAnimator.SetBool("isUndo", value);
    }

    public void SetIdleAnimation(bool value)
    {
        myAnimator.SetBool("isIdle", value);
    }

    public void SetSitDownAnimation(bool value)
    {
        myAnimator.SetBool("isNotSelect", value);
    }

    public void SetJumpingAnimation(bool value)
    {
        myAnimator.SetBool("isJumping", value);
    }

    public void SetDenyAnimation(bool value)
    {
        myAnimator.SetBool("isDeny", value);
    }
}
