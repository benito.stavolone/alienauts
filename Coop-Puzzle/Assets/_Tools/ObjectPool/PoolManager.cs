﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
	Dictionary<int, Queue<GameObject>> poolDictionary = new Dictionary<int, Queue<GameObject>>();
	List<GameObject> poolHolders = new List<GameObject>();

	static PoolManager _instance;

	public static PoolManager instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<PoolManager>();
			}
			return _instance;
		}
	}

	public void CreatePool(GameObject prefab, int poolSize)
	{
		int poolKey = prefab.GetInstanceID();

		if (!poolDictionary.ContainsKey(poolKey))
		{
			poolDictionary.Add(poolKey, new Queue<GameObject>());

			GameObject poolHolder = new GameObject(prefab.name + " pool");
			poolHolder.transform.parent = transform;
			poolHolders.Add(poolHolder);

			for (int i = 0; i < poolSize; i++)
			{
				GameObject newObject = Instantiate(prefab) as GameObject;
				poolDictionary[poolKey].Enqueue(newObject);
				newObject.SetActive(false);
				newObject.transform.SetParent(poolHolder.transform);
			}
		}
	}

	public T ReuseObject<T>(GameObject prefab, Vector3 position, Quaternion rotation) where T : Component
	{
		int poolKey = prefab.GetInstanceID();
		GameObject objectToReuse = null;
		
		if (poolDictionary.ContainsKey(poolKey))
		{
			
			objectToReuse = poolDictionary[poolKey].Dequeue();
			poolDictionary[poolKey].Enqueue(objectToReuse );

			if(objectToReuse.activeInHierarchy == true)
			{
				GameObject newObject = Instantiate(prefab) as GameObject;
				poolDictionary[poolKey].Enqueue(newObject);
				objectToReuse = newObject;
				for(int i = 0; i<poolHolders.Count; i++)
				{
					if (poolHolders[i].name == prefab.name + " pool")
					{
						objectToReuse.transform.SetParent(poolHolders[i].transform);
					}
				}
			}

			objectToReuse.gameObject.SetActive(true);
			objectToReuse.transform.position = position;
			objectToReuse.transform.rotation = rotation;
		}

		return objectToReuse.GetComponent(typeof(T)) as T;
	}

	public void PutObjectInPool(GameObject prefab)
    {
		IPoolable poolObjectScript;

		if (prefab.GetComponent<IPoolable>() != null)
        {
			poolObjectScript = prefab.GetComponent<IPoolable>();
			poolObjectScript.OnObjectReused();
		}

		prefab.SetActive(false);
	}
}