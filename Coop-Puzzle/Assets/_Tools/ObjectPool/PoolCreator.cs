﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PoolCreator : MonoBehaviour
{
    [SerializeField] private Pools[] pools;

    private void Start()
    {
        for (int i = 0; i < pools.Length;i++)
            PoolManager.instance.CreatePool(pools[i].prefab, pools[i].size);
    }
}

[System.Serializable]
public class Pools
{
    public GameObject prefab;
    public int size;
}

