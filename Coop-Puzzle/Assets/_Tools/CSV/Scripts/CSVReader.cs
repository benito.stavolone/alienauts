﻿using UnityEngine;
using System.IO;

public class CSVReader : MonoBehaviour
{
   private const char lineSeparator = '\n';
   private const char fieldSeparator = ',';

   private void Start()
   {
      RenderStream();
   }

   private void RenderStream()
   {
        try
        {
            StreamReader sr = File.OpenText(getPath());

            var line = "";

            while ((line = sr.ReadLine()) != null)
            {
                string[] fields = line.Split(fieldSeparator);
                for (int j = 0; j < fields.Length; j++)
                {
                    Debug.Log(fields[j]);
                }
            }

            sr.Close();
        }
        catch(FileNotFoundException e)
        {
            Debug.LogError("File Not Found");
        }
      
   }

    private string getPath()
    {
        return Application.dataPath + "DataSaved.csv";
    }
}
