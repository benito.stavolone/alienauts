﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class CSVWriterCustom : MonoBehaviour
{
    [SerializeField] private bool enableWriting = false;

    public enum LabelType
    {
        Chapter,
        Level,
        NMoves,
        NUndo
    }

    private Dictionary<LabelType, string> _dataDictionary = new Dictionary<LabelType, string>();

    private string _filePath;
    private StreamWriter _sw;

    private int _chapter;
    private int _level;
    private int _moves;
    private int _undo;

    private int _id;

    private void Start()
    {
        if (enableWriting)
        {
            SetIDFile();

            _filePath = getPath();

            SaveLabel();

            EventManager.undo += UpdateUndo;
            EventManager.levelCompleted += SaveData;
        }
    }

    #region<Update Methods>

    public void UpdateChapter(int currentChapter)
    {
        _chapter = currentChapter;
    }

    public void UpdateLevel(int currentLevel)
    {
        _level = currentLevel;
    }

    public void UpdateMoves(int currentMoves)
    {
        _moves = currentMoves;
    }

    public void UpdateUndo()
    {
        _undo++;
    }

    #endregion

    private void SaveLabel()
    {
        _dataDictionary.Add(LabelType.Chapter, "Chapter");
        _dataDictionary.Add(LabelType.Level, "Level");
        _dataDictionary.Add(LabelType.NMoves, "N.Moves");
        _dataDictionary.Add(LabelType.NUndo, "N.Undo");

        CreateText();
        Write();
    }


    private void CreateText()
    {
        _sw = File.CreateText(_filePath);
        _sw.Close();
    }

    private void Write()
    {
        string delimiter = ",";
        StringBuilder sb = new StringBuilder();

        sb.AppendLine(string.Join(delimiter, _dataDictionary.Values));
        _sw = File.AppendText(_filePath);

        _sw.Write(sb);
        _sw.Close();
    }

    private void SaveData()
    {
        Invoke("Saving", 1);
    }

    private void Saving()
    {
        _dataDictionary.Clear();

        _dataDictionary.Add(LabelType.Chapter, _chapter.ToString());
        _dataDictionary.Add(LabelType.Level, _level.ToString());
        _dataDictionary.Add(LabelType.NMoves, _moves.ToString());
        _dataDictionary.Add(LabelType.NUndo, _undo.ToString());

        Write();
        ResetData();
    }

    private string getPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/CSV/" + "DataSaved" + _id + ".csv";
#endif

        return Application.dataPath + "DataSaved" + _id + ".csv";
    }

    private void ResetData()
    {
        _undo = 0;
    }

    private void SetIDFile()
    {
        _id = PlayerPrefs.GetInt("IDFile");

        _id++;

        PlayerPrefs.SetInt("IDFile", _id);
    }

    private void OnDisable()
    {
        EventManager.undo -= UpdateUndo;
        EventManager.levelCompleted -= SaveData;
    }
}

