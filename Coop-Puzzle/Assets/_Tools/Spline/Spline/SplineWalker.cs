﻿using System;
using UnityEngine;

public class SplineWalker : MonoBehaviour
{

	[SerializeField] private BezierSpline[] Splines;

	private int index;

	public float duration;

	public bool lookForward;

	public SplineWalkerMode mode;

	private float progress;
	private bool goingForward = true;
	

	private void Awake()
	{
		index = 0;
	}

	private void Update () 
	{
		if (goingForward)
		{
			progress += Time.deltaTime / duration;
			if (progress > 1f) 
			{
				if (mode == SplineWalkerMode.Once) 
				{
					progress = 1f;
				}
				else if (mode == SplineWalkerMode.Loop) 
				{
					progress -= 1f;
				}
				else
				{
					progress = 2f - progress;
					goingForward = false;
				}
			}
		}
		
		
		Vector3 position = Splines[index].GetPoint(progress);
		transform.localPosition = position;
		if (lookForward) 
		{
			transform.LookAt(position + Splines[index].GetDirection(progress));
		}
	}

	
}