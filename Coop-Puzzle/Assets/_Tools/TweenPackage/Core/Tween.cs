﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("")]
public class Tween : MonoBehaviour
{
	[Header("Tween Settings")]

	[SerializeField] protected PlayType playType = PlayType.One;

	public enum PlayType
	{
		One,
		Loop,
		PingPong
	}

	[SerializeField] protected StartType startType = StartType.Start;

	public enum StartType
	{
		Start,
		OnEnable,
		Code
	}

	public enum AnimationType
    {
		Ping, 
		Pong
    }

	private AnimationType animationType = AnimationType.Pong;

	[SerializeField] private bool ignoreTimeScale = true;

	[SerializeField] private bool randomTween = false;

	private float playTimeNote;

	protected bool isRewind = false;
	protected bool isPlaying = false;
	protected bool isEnding = false;

	protected float animationTime = 1;

	private void Start()
	{
		if (startType == StartType.Start)
        {
			Initialize();
			Play();
		}
			
	}

	private void OnEnable()
	{
		if (startType == StartType.OnEnable)
		{
			Initialize();
			Play();
		}
	}

	

	protected virtual void Initialize()
	{

	}

	private void Play()
	{
		PlayAnimation();
		playTimeNote -= animationTime;
	}

	protected virtual void PlayAnimation()
	{
		CheckTimeScaleSetting();
		isPlaying = true;
		StartCoroutine(PlayAtTime());
	}

	protected float GetCurrentTimeValue()
	{
		float curTimeLenght = ignoreTimeScale ? (Time.time - playTimeNote) : (Time.unscaledTime - playTimeNote);
		float curValue = curTimeLenght / animationTime;

		if (animationType != AnimationType.Ping)
		{
			curValue = 1 - curValue;
			if (curValue <= 0)
			{
				if (isRewind)
					isPlaying = false;
				StopPlay();
			}
		}
		else
		{
			if (curValue >= 1)
			{
				isPlaying = false;
				StopPlay();

			}
		}

		curValue = curValue < 0 ? 0 : curValue;
		curValue = curValue > 1 ? 1 : curValue;

		return curValue;
	}

	protected virtual IEnumerator PlayAtTime()
	{
		yield return null;
	}

	private void CheckTimeScaleSetting()
	{
		if (!ignoreTimeScale)
			playTimeNote = Time.unscaledTime;
		else
			playTimeNote = Time.time;
	}

	protected void StopPlay()
	{
		switch (playType)
		{
			case PlayType.One:
				CheckTimeScaleSetting();
				animationType = AnimationType.Ping;
				break;

			case PlayType.Loop:
				CheckTimeScaleSetting();
				animationType = AnimationType.Ping;
				break;

			case PlayType.PingPong:
				CheckTimeScaleSetting();
				if(isRewind == false)
					animationType = AnimationType.Ping;
				break;
		}
	}

	protected int CheckEndAlltransitions(int index)
    {
		if (playType == PlayType.Loop)
			return 0;

		if (playType == PlayType.PingPong)
        {
            if (!isRewind)
            {
				animationType = AnimationType.Pong;
				isRewind = true;
				return index;
			}
            else
            {
				animationType = AnimationType.Ping;
				isRewind = false;
				return index;
			}
        }


		return index++;
	}

	//Utils Method

	public void StopTween()
    {
		isEnding = true;

		StopAllCoroutines();
    }

	public void PlayTween()
    {
		if (isEnding)
			isEnding = false;

		Initialize();

		Invoke("Play", 0.5f);
    }

	public void PauseTween(float amountOfSeconds)
    {
		StopTween();
		Invoke("PlayTween", amountOfSeconds);
    }
}

