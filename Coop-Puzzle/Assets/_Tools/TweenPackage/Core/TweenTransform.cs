﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("")]
public class TweenTransform : Tween
{
    [Header("Transform Settings")]

    public List<TransformData> data;

    protected Transform tweenObject;
    protected float thisValue;
    protected int index = 0;

    protected override void Initialize()
    {
        base.Initialize();

        tweenObject = transform;

        if(data.Count!=0)
            animationTime = data[index].animationTime;
    }

    protected override IEnumerator PlayAtTime()
    {
        if (tweenObject == null)
            yield return null;

        if (GetCurrentTimeValue() == 1)
            yield return null;

        if (data.Count > 0)
        {
            while (isPlaying)
            {
                thisValue = data[index].curve.Evaluate(GetCurrentTimeValue());

                ChangeValue();

                yield return null;
            }

            data[index].onFinish.Invoke();

            CheckEndTransition();
        }     
    }

    private void CheckEndTransition()
    {
        StopAllCoroutines();

        if (!isRewind)
        {
            if (index < data.Count - 1)
                index++;
            else
            {
                if(playType == PlayType.One)
                    isEnding = true;
                else
                    index = CheckEndAlltransitions(index);
            }
                
        }
        else
        {
            if (index > 0)
                index--;
            else
                index = CheckEndAlltransitions(index);
        }

        if (!isEnding)
        {
            PlayAnimation();
            animationTime = data[index].animationTime;
        }
        else
        {
            isPlaying = false;
        }

    }

    public virtual void ChangeValue()
    {

    }
}

[System.Serializable]
public class TransformData
{
    [Min(1)]
    public float animationTime = 1.0f;

    [Header("Position")]

    [SerializeField] Vector3 _fromVector = Vector3.one;
    [SerializeField] Vector3 _toVector = Vector3.one;

    public Vector3 fromVector { get { return _fromVector; } set { _fromVector = value; } }
    public Vector3 toVector { get { return _toVector; } set { _toVector = value; } }

    [Header("Animation Curve")]

    public AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    [Space(10)]
    public bool isLocal = true;

    public OnFinish onFinish;
}
