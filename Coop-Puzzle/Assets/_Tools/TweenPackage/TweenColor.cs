﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TweenColor : Tween
{
    [Header("Colors Settings")]

    public List<ColorData> colorData;

    protected MeshRenderer tweenObject3D;
    protected Image tweenObject2D;
    protected float thisValue;
    protected int index = 0;

    protected override void Initialize()
    {
        base.Initialize();

        if (transform.GetComponent<MeshRenderer>())
            tweenObject3D = transform.GetComponent<MeshRenderer>();
        else
            tweenObject2D = transform.GetComponent<Image>();

        if (colorData.Count != 0)
        {
            animationTime = colorData[index].animationTime;
        }


    }

    protected override IEnumerator PlayAtTime()
    {
        if (tweenObject3D == null || tweenObject2D == null)
            yield return null;

        if (colorData.Count > 0)
        {
            while (isPlaying)
            {
                thisValue = colorData[index].curve.Evaluate(GetCurrentTimeValue());

                if(tweenObject3D)
                    tweenObject3D.material.color = Color.Lerp(colorData[index].fromColor, colorData[index].toColor, thisValue);
                else
                    tweenObject2D.color = Color.Lerp(colorData[index].fromColor, colorData[index].toColor, thisValue);

                yield return null;
            }

            colorData[index].onFinish.Invoke();

            CheckEndTransition();
        }
    }

    private void CheckEndTransition()
    {
        StopAllCoroutines();

        if (!isRewind)
        {
            if (index < colorData.Count - 1)
                index++;
            else
            {
                if (playType == PlayType.One)
                    isEnding = true;
                else
                    index = CheckEndAlltransitions(index);
            }

        }
        else
        {
            if (index > 0)
                index--;
            else
                index = CheckEndAlltransitions(index);
        }

        if (!isEnding)
        {
            PlayAnimation();
            animationTime = colorData[index].animationTime;
        }
        else
        {
            isPlaying = false;
            this.enabled = false;
        }

    }
}

[System.Serializable]
public class ColorData
{
    [Min(1)]
    public float animationTime = 1.0f;

    [Header("Colors")]

    [SerializeField] Color _fromColor = Color.white;
    [SerializeField] Color _toColor = Color.black;

    public Color fromColor { get { return _fromColor; } set { _toColor = value; } }
    public Color toColor { get { return _toColor; } set { _toColor = value; } }

    [Header("Animation Curve")]

    public AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    public OnFinish onFinish;
}
