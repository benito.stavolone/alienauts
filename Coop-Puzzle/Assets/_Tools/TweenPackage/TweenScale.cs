﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenScale : TweenTransform
{
    public override void ChangeValue()
    {
        if (data[index].isLocal)
            tweenObject.localScale = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
        else
            tweenObject.localScale = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
    }
}
