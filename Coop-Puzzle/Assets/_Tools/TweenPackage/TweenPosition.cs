﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenPosition : TweenTransform
{
    public override void ChangeValue()
    {
        if (tweenObject.GetComponent<RectTransform>() != null)
            tweenObject.localPosition = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
        else
        {
            if (data[index].isLocal)
                tweenObject.localPosition = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
            else
                tweenObject.position = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
        }


    }
}
