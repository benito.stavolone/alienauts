﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenRotation : TweenTransform
{
    public override void ChangeValue()
    {
        if (data[index].isLocal)
            tweenObject.localEulerAngles = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
        else
            tweenObject.eulerAngles = Vector3.Lerp(data[index].fromVector, data[index].toVector, thisValue);
    }
}
