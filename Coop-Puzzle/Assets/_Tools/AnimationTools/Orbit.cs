﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
    [Tooltip("The Distance we Want to Move on X-Axis")]
    [SerializeField] private float xSpread;

    [Tooltip("The Distance we Want to Move on Z-Axis")]
    [SerializeField] private float zSpread;

    [Tooltip("Above or Below from CenterPoint")]
    [SerializeField] private float yOffset;

    [SerializeField] private Transform centerPoint;

    [SerializeField] private float rotateSpeed;

    [Tooltip("Type of Rotation")]
    [SerializeField] private bool rotationClockwise;

    private float _timer = 0;

    private void Update()
    {
        _timer += Time.deltaTime * rotateSpeed;
        Rotate();
    }

    private void Rotate()
    {
        if (rotationClockwise)
        {
            float x = -Mathf.Cos(_timer) * xSpread;
            float z = Mathf.Sin(_timer) * zSpread;
            Vector3 pos = new Vector3(x, yOffset, z);
            transform.position = pos + centerPoint.position;
        }
        else
        {
            float x = Mathf.Cos(_timer) * xSpread;
            float z = Mathf.Sin(_timer) * zSpread;
            Vector3 pos = new Vector3(x, yOffset, z);
            transform.position = pos + centerPoint.position;
        }
    }


}
