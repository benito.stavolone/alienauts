﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField] private float orbitSpeed;
    [SerializeField] private GameObject parent;
    [SerializeField] private bool rotationClockwise;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 rotationDirection;

    private void Update()
    {
        if (rotationClockwise)
            transform.RotateAround(parent.transform.position, parent.transform.up, orbitSpeed * Time.deltaTime);
        else
            transform.RotateAround(parent.transform.position, -parent.transform.up, orbitSpeed * Time.deltaTime);

        transform.Rotate(rotationDirection, rotationSpeed * Time.deltaTime);
    }

    public void SetUpbeltObject(float speed, float rotation, GameObject parentObject, bool Clockwise)
    {
        orbitSpeed = speed;
        rotationSpeed = rotation;
        parent = parentObject;
        rotationClockwise = Clockwise;
        rotationDirection = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
    }
}
