﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsBelt : MonoBehaviour
{
    [Header("Spawner Settings")]

    [SerializeField] private GameObject asteroidPrefab;
    [SerializeField] private int asteroidDensity;
    [SerializeField] private int scaleAdditive;
    [SerializeField] private int seed;
    [SerializeField] private float innerRadius;
    [SerializeField] private float outerRadius;
    [SerializeField] private float height;
    [SerializeField] private bool clockwiseRotation;

    [SerializeField] private LayerMask asteroidLayer;

    [Header("Asteroid Setting")]

    [SerializeField] private float minRotationSpeed;
    [SerializeField] private float maxRotationSpeed;

    [SerializeField] private float minOrbitSpeed;
    [SerializeField] private float maxorbitSpeed;

    [SerializeField] private Material asteroidMaterial;

    public Material AsteroidMaterial
    {
        get { return asteroidMaterial; }
    }

    [SerializeField] private Material lockMaterial;

    private Material _myMaterial;
    private GameObject _actualAsteroid;

    [SerializeField] private PlanetData myPlanet;

    private Vector3 _localPosition;
    private Vector3 _worldOffset;
    private Vector3 _worldPosition;

    private float _randomRadius;
    private float _randomRadian;

    private float _x, _y, _z;

    private void Awake()
    {
        Random.InitState(seed);

        for(int i = 0; i < asteroidDensity; i++)
        {
            do
            {
                _randomRadius = Random.Range(innerRadius, outerRadius);
                _randomRadian = Random.Range(0, 2 * Mathf.PI);

                _y = Random.Range(-(height / 2), (height / 2));
                _x = _randomRadius * Mathf.Cos(_randomRadian);
                _z = _randomRadius * Mathf.Sin(_randomRadian);
            }
            while (float.IsNaN(_z) && float.IsNaN(_z));

            _localPosition = new Vector3(_x, _y, _z);
            _worldOffset = transform.rotation * _localPosition;
            _worldPosition = transform.position + _worldOffset;

            GameObject _asteroid = Instantiate(asteroidPrefab, _worldPosition, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            _actualAsteroid = _asteroid;

            _asteroid.transform.localScale = new Vector3(_asteroid.transform.localScale.x * scaleAdditive, _asteroid.transform.localScale.y * scaleAdditive, _asteroid.transform.localScale.z * scaleAdditive);

            if (myPlanet)
            {
                if (myPlanet.PlanetStatus == PlanetStatus.LOCK)
                    ChangeMaterial(false);
                else
                    ChangeMaterial(true);
            }
            else
                ChangeMaterial(true);

            _asteroid.layer = Mathf.RoundToInt(Mathf.Log(asteroidLayer.value, 2));
            _asteroid.AddComponent<Asteroid>().SetUpbeltObject(Random.Range(minOrbitSpeed, maxorbitSpeed), Random.Range(minRotationSpeed, maxRotationSpeed), gameObject, clockwiseRotation);
            _asteroid.transform.SetParent(transform);
        }
    }

    public void ChangeMaterial(bool unlock)
    {
        if(unlock)
            _actualAsteroid.GetComponent<MeshRenderer>().material = asteroidMaterial;
        else
            _actualAsteroid.GetComponent<MeshRenderer>().material = lockMaterial;
    }
}
